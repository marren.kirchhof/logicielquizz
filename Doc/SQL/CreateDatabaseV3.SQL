﻿/*
Created: 17.08.2020
Modified: 17.08.2020
Model: MCD
Database: MySQL 8.0
*/


-- Table Quizz

CREATE TABLE `Quizz`
(
  `ID_Quizz` Int NOT NULL AUTO_INCREMENT,
  `Titre` Text NOT NULL,
  `Date` Date NOT NULL,
  `Version` Int NOT NULL,
  `ID_Utilisateur` Int NOT NULL,
  PRIMARY KEY (`ID_Quizz`, `ID_Utilisateur`)
)
;

-- Table Utilisateur

CREATE TABLE `Utilisateur`
(
  `ID_Utilisateur` Int NOT NULL AUTO_INCREMENT,
  `Nom` Text NOT NULL,
  `Prenom` Text NOT NULL,
  `Pseudo` Text NOT NULL,
  PRIMARY KEY (`ID_Utilisateur`)
)
;

-- Table Question

CREATE TABLE `Question`
(
  `ID_Question` Int NOT NULL AUTO_INCREMENT,
  `Contenu` Text NOT NULL,
  `Type` Int NOT NULL,
  `ID_Type` Int NOT NULL,
  PRIMARY KEY (`ID_Question`, `ID_Type`)
)
;

-- Table VraiFaux

CREATE TABLE `VraiFaux`
(
  `ID_VraixFaux` Int NOT NULL AUTO_INCREMENT,
  `Nom` Text NOT NULL,
  `RepUser` Bool NOT NULL,
  `Correction` Bool NOT NULL,
  `ID_Question` Int,
  `ID_Type` Int,
  PRIMARY KEY (`ID_VraixFaux`)
)
;

CREATE INDEX `IX_aVraiFaux` ON `VraiFaux` (`ID_Question`, `ID_Type`)
;

-- Table DragAndDrop

CREATE TABLE `DragAndDrop`
(
  `ID_DragDrop` Bigint NOT NULL AUTO_INCREMENT,
  `Nom` Text NOT NULL,
  `RepUser` Int NOT NULL,
  `Correction` Int NOT NULL,
  `ID_Question` Int,
  `ID_Type` Int,
  PRIMARY KEY (`ID_DragDrop`)
)
;

CREATE INDEX `IX_aDragAndDrop` ON `DragAndDrop` (`ID_Question`, `ID_Type`)
;

-- Table QCM

CREATE TABLE `QCM`
(
  `ID_QCM` Int NOT NULL AUTO_INCREMENT,
  `Nom` Text NOT NULL,
  `RepUser` Bool NOT NULL,
  `Correction` Bool NOT NULL,
  PRIMARY KEY (`ID_QCM`)
)
;

-- Table Type

CREATE TABLE `Type`
(
  `ID_Type` Int NOT NULL AUTO_INCREMENT,
  `Nom` Int NOT NULL,
  PRIMARY KEY (`ID_Type`)
)
;

-- Table Quizz_Question

CREATE TABLE `Quizz_Question`
(
  `ID_Quizz` Int NOT NULL,
  `ID_Question` Int NOT NULL,
  `ID_Type` Int NOT NULL,
  `ID_Utilisateur` Int NOT NULL
)
;

-- Table Quizz_Utilisateur

CREATE TABLE `Quizz_Utilisateur`
(
  `ID_Quizz` Int NOT NULL,
  `ID_Utilisateur` Int NOT NULL
)
;

-- Table Question_QCM

CREATE TABLE `Question_QCM`
(
  `ID_Question` Int NOT NULL,
  `ID_QCM` Int NOT NULL,
  `ID_Type` Int NOT NULL
)
;

-- Create foreign keys (relationships) section -------------------------------------------------

ALTER TABLE `VraiFaux` ADD CONSTRAINT `aVraiFaux` FOREIGN KEY (`ID_Question`, `ID_Type`) REFERENCES `Question` (`ID_Question`, `ID_Type`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `DragAndDrop` ADD CONSTRAINT `aDragAndDrop` FOREIGN KEY (`ID_Question`, `ID_Type`) REFERENCES `Question` (`ID_Question`, `ID_Type`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Question` ADD CONSTRAINT `QisType` FOREIGN KEY (`ID_Type`) REFERENCES `Type` (`ID_Type`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Quizz` ADD CONSTRAINT `QuizzOwnerUtilisateur` FOREIGN KEY (`ID_Utilisateur`) REFERENCES `Utilisateur` (`ID_Utilisateur`) ON DELETE RESTRICT ON UPDATE RESTRICT
;


