INSERT INTO utilisateur (Nom, Password) VALUES ("Kirchhof","e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a" );
INSERT INTO utilisateur (Nom, Password) VALUES ("Blomaers", "e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a" );
INSERT INTO utilisateur (Nom, Password) VALUES ("Paris", "e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a" );
INSERT INTO utilisateur (Nom, Password) VALUES ("Provin", "e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a" );
INSERT INTO utilisateur (Nom, Password) VALUES ("Biedermann", "e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a" );


INSERT INTO quizz (Titre,quizz.date,quizz.Version,ID_Utilisateur) 
	SELECT "Quizz N°1", '01.01.2020' , 1, utilisateur.ID_Utilisateur
	FROM utilisateur
	WHERE utilisateur.Nom = 'Kirchhof'; 	
	
INSERT INTO quizz_utilisateur(ID_Quizz,ID_Utilisateur)
	SELECT quizz.ID_Quizz , utilisateur.ID_Utilisateur FROM utilisateur INNER JOIN quizz ON quizz.Titre = "Quizz N°1"  WHERE utilisateur.Nom LIKE '%a%' ;
	


INSERT INTO `type` (`Nom`) VALUES ("VraiFaux"),("DragDrop"),("QCM"); 
INSERT INTO `quizz`.`question` (`Contenu`, `ID_Type`) VALUES ('Quel est le poinds moyen d\'un canard ?', '3');
INSERT INTO `quizz`.`question` (`Contenu`, `ID_Type`) VALUES ('Quels sont les 7 couches OSI ', '2');
INSERT INTO `quizz`.`question` (`Contenu`, `ID_Type`) VALUES ('Skype est-t-il le meilleur outil de communication ?', '1');
INSERT INTO `quizz`.`vraifaux` (`Nom`, `RepUser`, `Correction`, `ID_Question`) VALUES ('Oui / Non', '1', '1', '3');
INSERT INTO `quizz`.`qcm` (`Nom`) VALUES ('Poisson');
INSERT INTO `quizz`.`qcm` (`Nom`) VALUES ('Desert');
INSERT INTO `quizz`.`qcm` (`Nom`) VALUES ('Cancer');
INSERT INTO `quizz`.`question_qcm` (`ID_Question`, `ID_QCM`) VALUES ('1', '1');
INSERT INTO `quizz`.`question_qcm` (`ID_Question`, `ID_QCM`) VALUES ('1', '2');
INSERT INTO `quizz`.`question_qcm` (`ID_Question`, `ID_QCM`) VALUES ('1', '3');
INSERT INTO `quizz`.`draganddrop` (`Nom`, `RepUser`, `Correction`, `ID_Question`) VALUES ('"Voici les valeurs à trier :","physique","liaison de données","réseau","transport","session","présentation","application"', '1234657', '1234567', '2');

INSERT INTO `quizz`.`quizz_question` (`ID_Quizz`, `ID_Question`, `ID_Type`, `ID_Utilisateur`) VALUES ('1', '2', '3', '2');
