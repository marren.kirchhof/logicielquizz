﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzClient {
    public static class Corrige {

        // Contain the question list and the response list of the quizz
        public static List<Question> LQ;
        public static List<Response> LR;

        
        public static int PtsQcm { get; private set; } = 0; //Get the total points of the qcms
        public static int PtsUserQcm { get; private set; } = 0; //Get the total points of the qcms obtained by the user
        public static int PtsVraiFaux { get; private set; } = 0;//Get the total points of the TrueFalse
        public static int PtsUserVraiFaux { get; private set; } = 0;//Get the total points of the TrueFalse obtained by the user
        public static int PtsDragdrop { get; private set; } = 0;//Get the total points of the DragDrops
        public static int PtsUserDragdrop { get; private set; } = 0;//Get the total points of the DragDrops obtained by the user


        /// <summary>
        /// calculates and set the values in the accessor
        /// </summary>
        /// <returns></returns>
        public static async Task<int> CalcPourcentage() {
            double FinalResultPercent = 0;
            PtsQcm = 0;
            PtsVraiFaux = 0;
            PtsDragdrop = 0;
            PtsUserQcm = 0;
            PtsUserVraiFaux = 0;
            PtsUserDragdrop = 0;
            try {

                if (LR != null && LR != null) {

                    double TotalPoint = 0;
                    double EarnedPoint = 0;

                    foreach (Question Q in LQ) {
                        string JsonRepUser = Q.correction;
                        Response CurrentResponse = LR.Find(Response => Response.idQuestion == Q.idQuestion);

                        if (CurrentResponse != null) {
                            Task<QuestionType> QType = HttpRequest.TypeQuestionRequest.GetOne(Q.idType);
                            QuestionType TypeCurrentQuestion = await QType;

                            int currentQuestionTotalPoint = 0;
                            int currentQuestionEarnedPoint = 0;

                            switch (TypeCurrentQuestion.name) {
                                case Constantes.QuestionsType.Qcm:

                                    Qcm QcmQuestionCorrection = JsonConvert.DeserializeObject<Qcm>(Q.correction);
                                    Qcm QcmQuestionUserResponse = JsonConvert.DeserializeObject<Qcm>(CurrentResponse.respUser);
                                    
                                    currentQuestionTotalPoint += CalcTotQcmPoints(QcmQuestionCorrection);
                                    currentQuestionEarnedPoint += CalcearnedQcmPoints(QcmQuestionCorrection, QcmQuestionUserResponse);

                                    PtsQcm += currentQuestionTotalPoint;
                                    PtsUserQcm += currentQuestionEarnedPoint;
                                    
                                    break;
                                case Constantes.QuestionsType.Vraifaux:
                                    
                                    VraiFaux VraifauxQuestionCorrection = JsonConvert.DeserializeObject<VraiFaux>(Q.correction);
                                    VraiFaux VraifauxQuestionUserResponse = JsonConvert.DeserializeObject<VraiFaux>(CurrentResponse.respUser);

                                    currentQuestionTotalPoint += CalcTotVraiFauxPoints(VraifauxQuestionCorrection);
                                    currentQuestionEarnedPoint += CalcearnedVraiFauxPoints(VraifauxQuestionCorrection, VraifauxQuestionUserResponse);

                                    PtsVraiFaux += currentQuestionTotalPoint;
                                    PtsUserVraiFaux += currentQuestionEarnedPoint;

                                    break;
                                case Constantes.QuestionsType.DragDrop:

                                    DragAndDrop DragAndDropQuestionCorrection = JsonConvert.DeserializeObject<DragAndDrop>(Q.correction);
                                    DragAndDrop DragAndDropQuestionUserResponse = JsonConvert.DeserializeObject<DragAndDrop>(CurrentResponse.respUser);

                                    currentQuestionTotalPoint = CalcTotDragAndDropPoints(DragAndDropQuestionCorrection);
                                    currentQuestionEarnedPoint = CalcearnedDragAndDropPoints(DragAndDropQuestionCorrection, DragAndDropQuestionUserResponse);

                                    PtsDragdrop += currentQuestionTotalPoint;
                                    PtsUserDragdrop += currentQuestionEarnedPoint;

                                    break;
                            }
                            TotalPoint += currentQuestionTotalPoint;
                            EarnedPoint += currentQuestionEarnedPoint;
                        }

                        if (TotalPoint > 0) {
                            FinalResultPercent = EarnedPoint / TotalPoint * 100;
                        } else { FinalResultPercent = 0; }

                    }
                }
            } catch 
            { }

            
            return Convert.ToInt32(FinalResultPercent);
        }


        



        // If you want to change the barem, modify the code below this line

        /// <summary>
        /// calc the totals point of the Qcm
        /// </summary>
        /// <param name="Q">Qcm ref</param>
        /// <returns></returns>
        #region Qcm
        private static int CalcTotQcmPoints(Qcm Q) {
            int CorrectResponse = 0;
            foreach (KeyValuePair<string, bool> entry in Q.ListQcm) {
                if (Q.ListQcm[entry.Key]) {
                    CorrectResponse++;
                }
            }
            return CorrectResponse;
        }


        /// <summary>
        /// Return the différence between the Qcm of the user and the Qcm corrected by the professor
        /// </summary>
        /// <param name="QCorrected"></param>
        /// <param name="QUser"></param>
        /// <returns></returns>
        private static int CalcearnedQcmPoints(Qcm QCorrected,Qcm QUser) {
            int point = 0;
            foreach (KeyValuePair<string, bool> entry in QUser.ListQcm) {
                if (QCorrected.ListQcm[entry.Key] == true && QUser.ListQcm[entry.Key] == QCorrected.ListQcm[entry.Key]) {
                    point++;
                }
                if (QCorrected.ListQcm[entry.Key] == true && QUser.ListQcm[entry.Key] != QCorrected.ListQcm[entry.Key]) {
                    point--;
                }
            }
            if (point < 0) { point = 0; }

            return point;
        }
        #endregion


        /// <summary>
        /// Return the total point of a question true false
        /// </summary>
        /// <param name="Q"></param>
        /// <returns></returns>
        #region VraiFaux
        private static int CalcTotVraiFauxPoints(VraiFaux Q) {
            return Constantes.Correction.PointsTotVraiFaux;
        }

        /// <summary>
        ///  Return the différence between the trueFlase of the user and the trueFalse corrected
        /// </summary>
        /// <param name="QCorrected">Corrected Question true false  </param>
        /// <param name="QUser">Question true false of the user</param>
        /// <returns></returns>
        private static int CalcearnedVraiFauxPoints(VraiFaux QCorrected, VraiFaux QUser) {
            int point = Constantes.Correction.PointsWrongAnswerVraiFaux;
            if (QCorrected.ResponseCorrection == QUser.ResponseCorrection) {
                point = Constantes.Correction.PointsTotVraiFaux;
            }
            return point;
        }
        #endregion

        /// <summary>
        /// calc the total point of a question drag drop
        /// </summary>
        /// <param name="Q">question drag drop</param>
        /// <returns>total points of the question</returns>
        #region DragAndDrop
        private static int CalcTotDragAndDropPoints(DragAndDrop Q) {
            int point = Q.WrongQuestion.Count;
            foreach (ResponseDragDrop r in Q.Response) {
                point += r.ListQuestion.Count;
            }
            return point;
        }

        /// <summary>
        ///  Return the différence between the dragdrop of the user and the dragdrop corrected
        /// </summary>
        /// <param name="QCorrected">Corrected Question dragdrop  </param>
        /// <param name="QUser">Question dragdrop of the user</param>
        /// <returns>the total points</returns>
        private static int CalcearnedDragAndDropPoints(DragAndDrop QCorrected, DragAndDrop QUser) {
            int point = 0;

            point = PointsDiffList(QCorrected.WrongQuestion, QUser.WrongQuestion);

            foreach (ResponseDragDrop r in QCorrected.Response) {
                ResponseDragDrop rUser = QUser.Response.Find(x => x.Title == r.Title);
                point += PointsDiffList(r.ListQuestion, rUser.ListQuestion);
            }

            
            
            return point;
        }

        /// <summary>
        /// This function calculates the difference between two string list
        /// </summary>
        /// <param name="LCorrection">The correct list of the dragdrop</param>
        /// <param name="LResponseUser">The user list of the dragdrop</param>
        /// <returns></returns>
        private static int PointsDiffList(List<string> LCorrection,List<string>LResponseUser) {
            int ptsQuestionTot = 0;
            List<string> AlreadyCheckedvalue = new List<string>();
            foreach (string strQuestionUnused in LCorrection) {
                int ptsQuestion = 0;
                int OccuQuestionRight = 0;
                int OccuQuestionUser = 0;

                if (!AlreadyCheckedvalue.Contains(strQuestionUnused)) {
                    
                    OccuQuestionRight = LCorrection.Where(s => s.Contains(strQuestionUnused)).Count();
                    OccuQuestionUser = LResponseUser.Where(s => s.Contains(strQuestionUnused)).Count();
                    ptsQuestion = OccuQuestionRight;
                    int PtsDeduce = Math.Abs(OccuQuestionRight - OccuQuestionUser);
                    ptsQuestion -= PtsDeduce;
                    if (ptsQuestion < 0) { ptsQuestion = 0; }

                    ptsQuestionTot += ptsQuestion;
                    AlreadyCheckedvalue.Add(strQuestionUnused);


                }
            }
            return ptsQuestionTot;
        }

        #endregion






    }
}
