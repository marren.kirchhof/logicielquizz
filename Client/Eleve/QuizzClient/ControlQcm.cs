﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace QuizzClient {
    public partial class ControlQcm : UserControl {
        public ControlQcm() {
            InitializeComponent();
        }
        private Question Q;

        private List<CxFlatUI.CxFlatCheckBox> LcheckBox;
        public Question question {
            get { return Q; }
            set {

                Q = value;
                lbQuestionText.Text = value.title;
                Qcm Qcms = Newtonsoft.Json.JsonConvert.DeserializeObject<Qcm>(value.correction);
                flpResponse.Controls.Clear();
                LcheckBox = new List<CxFlatUI.CxFlatCheckBox>();
                int i = 0;
                foreach (KeyValuePair<string, bool> item in Qcms.ListQcm) {
                    CxFlatUI.CxFlatCheckBox C = new CxFlatUI.CxFlatCheckBox();
                    C.Text = item.Key;
                    C.Checked = false;
                    LcheckBox.Add(C);
                    C.CheckedChanged += ControlQcm_Click;
                    flpResponse.Controls.Add(LcheckBox[i]);
                    i++;
                }


            }
        }

        public Question CorrectedQuestion{
            get { return Q; }
            set {

                Q = value;
                lbQuestionText.Text = value.title;
                Qcm Qcms = Newtonsoft.Json.JsonConvert.DeserializeObject<Qcm>(value.correction);
                flpResponse.Controls.Clear();
                LcheckBox = new List<CxFlatUI.CxFlatCheckBox>();
                int i = 0;
                foreach (KeyValuePair<string, bool> item in Qcms.ListQcm) {
                    CxFlatUI.CxFlatCheckBox C = new CxFlatUI.CxFlatCheckBox();
                    C.Text = item.Key;
                    C.Checked = item.Value;
                    C.Enabled = false;
                    LcheckBox.Add(C);
                    C.CheckedChanged += ControlQcm_Click;
                    flpResponse.Controls.Add(LcheckBox[i]);
                    i++;
                }


            }
        }


        public void SetColorCorrection() {
            lbInfoQcm.ButtonType = CxFlatUI.ButtonType.Success;
           
        }


        private void ControlQcm_Click(object sender, EventArgs e) {
           
        }


        public string ResponseJsonString { get { return GetJsonStringResponse(); } }

        private string GetJsonStringResponse() {
            Dictionary<string, bool> ListQuestion = new Dictionary<string, bool>();
            for (int i = 0; i < LcheckBox.Count; i++) {
                ListQuestion.Add(LcheckBox[i].Text, (LcheckBox[i].Checked));
            }

            Qcm QcmJson = new Qcm(ListQuestion);
            return Newtonsoft.Json.JsonConvert.SerializeObject(QcmJson);
        }

    }
}