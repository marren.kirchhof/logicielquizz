﻿namespace QuizzClient {
    partial class ControlVraiFaux {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.gbxReponse = new CxFlatUI.CxFlatGroupBox();
            this.tbxFalse = new CxFlatUI.CxFlatTextBox();
            this.tbxTrue = new CxFlatUI.CxFlatTextBox();
            this.rbFalse = new CxFlatUI.CxFlatRadioButton();
            this.rbTrue = new CxFlatUI.CxFlatRadioButton();
            this.lbQuestionText = new CxFlatUI.CxFlatTextBox();
            this.lbInfoVraiFaux = new CxFlatUI.Controls.CxFlatButton();
            this.gbxReponse.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxReponse
            // 
            this.gbxReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxReponse.Controls.Add(this.tbxFalse);
            this.gbxReponse.Controls.Add(this.tbxTrue);
            this.gbxReponse.Controls.Add(this.rbFalse);
            this.gbxReponse.Controls.Add(this.rbTrue);
            this.gbxReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxReponse.Location = new System.Drawing.Point(209, 36);
            this.gbxReponse.MinimumSize = new System.Drawing.Size(203, 39);
            this.gbxReponse.Name = "gbxReponse";
            this.gbxReponse.ShowText = false;
            this.gbxReponse.Size = new System.Drawing.Size(291, 103);
            this.gbxReponse.TabIndex = 14;
            this.gbxReponse.TabStop = false;
            this.gbxReponse.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // tbxFalse
            // 
            this.tbxFalse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxFalse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxFalse.Hint = "Faux";
            this.tbxFalse.Location = new System.Drawing.Point(37, 58);
            this.tbxFalse.MaxLength = 32767;
            this.tbxFalse.Multiline = false;
            this.tbxFalse.Name = "tbxFalse";
            this.tbxFalse.PasswordChar = '\0';
            this.tbxFalse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxFalse.SelectedText = "";
            this.tbxFalse.SelectionLength = 0;
            this.tbxFalse.SelectionStart = 0;
            this.tbxFalse.Size = new System.Drawing.Size(248, 38);
            this.tbxFalse.TabIndex = 3;
            this.tbxFalse.TabStop = false;
            this.tbxFalse.UseSystemPasswordChar = false;
            // 
            // tbxTrue
            // 
            this.tbxTrue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxTrue.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxTrue.Hint = "Vrai";
            this.tbxTrue.Location = new System.Drawing.Point(37, 14);
            this.tbxTrue.MaxLength = 32767;
            this.tbxTrue.Multiline = false;
            this.tbxTrue.Name = "tbxTrue";
            this.tbxTrue.PasswordChar = '\0';
            this.tbxTrue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxTrue.SelectedText = "";
            this.tbxTrue.SelectionLength = 0;
            this.tbxTrue.SelectionStart = 0;
            this.tbxTrue.Size = new System.Drawing.Size(248, 38);
            this.tbxTrue.TabIndex = 2;
            this.tbxTrue.TabStop = false;
            this.tbxTrue.UseSystemPasswordChar = false;
            // 
            // rbFalse
            // 
            this.rbFalse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbFalse.AutoSize = true;
            this.rbFalse.CheckedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            this.rbFalse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.rbFalse.Location = new System.Drawing.Point(8, 64);
            this.rbFalse.Name = "rbFalse";
            this.rbFalse.Size = new System.Drawing.Size(25, 20);
            this.rbFalse.TabIndex = 1;
            this.rbFalse.TabStop = true;
            this.rbFalse.UseVisualStyleBackColor = true;
            // 
            // rbTrue
            // 
            this.rbTrue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbTrue.AutoSize = true;
            this.rbTrue.CheckedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            this.rbTrue.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.rbTrue.Location = new System.Drawing.Point(8, 20);
            this.rbTrue.Name = "rbTrue";
            this.rbTrue.Size = new System.Drawing.Size(25, 20);
            this.rbTrue.TabIndex = 0;
            this.rbTrue.TabStop = true;
            this.rbTrue.UseVisualStyleBackColor = true;
            // 
            // lbQuestionText
            // 
            this.lbQuestionText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbQuestionText.Enabled = false;
            this.lbQuestionText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbQuestionText.Hint = "";
            this.lbQuestionText.Location = new System.Drawing.Point(4, 36);
            this.lbQuestionText.MaxLength = 32767;
            this.lbQuestionText.MinimumSize = new System.Drawing.Size(206, 78);
            this.lbQuestionText.Multiline = true;
            this.lbQuestionText.Name = "lbQuestionText";
            this.lbQuestionText.PasswordChar = '\0';
            this.lbQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbQuestionText.SelectedText = "";
            this.lbQuestionText.SelectionLength = 0;
            this.lbQuestionText.SelectionStart = 0;
            this.lbQuestionText.Size = new System.Drawing.Size(206, 103);
            this.lbQuestionText.TabIndex = 13;
            this.lbQuestionText.TabStop = false;
            this.lbQuestionText.UseSystemPasswordChar = false;
            // 
            // lbInfoVraiFaux
            // 
            this.lbInfoVraiFaux.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoVraiFaux.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoVraiFaux.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoVraiFaux.Location = new System.Drawing.Point(0, 0);
            this.lbInfoVraiFaux.Name = "lbInfoVraiFaux";
            this.lbInfoVraiFaux.Size = new System.Drawing.Size(500, 28);
            this.lbInfoVraiFaux.TabIndex = 15;
            this.lbInfoVraiFaux.Text = "Vrai / Faux";
            this.lbInfoVraiFaux.TextColor = System.Drawing.Color.White;
            // 
            // ControlVraiFaux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbInfoVraiFaux);
            this.Controls.Add(this.gbxReponse);
            this.Controls.Add(this.lbQuestionText);
            this.Name = "ControlVraiFaux";
            this.Size = new System.Drawing.Size(500, 142);
            this.gbxReponse.ResumeLayout(false);
            this.gbxReponse.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatGroupBox gbxReponse;
        private CxFlatUI.CxFlatTextBox tbxFalse;
        private CxFlatUI.CxFlatTextBox tbxTrue;
        private CxFlatUI.CxFlatTextBox lbQuestionText;
        private CxFlatUI.Controls.CxFlatButton lbInfoVraiFaux;
        public CxFlatUI.CxFlatRadioButton rbFalse;
        public CxFlatUI.CxFlatRadioButton rbTrue;
    }
}
