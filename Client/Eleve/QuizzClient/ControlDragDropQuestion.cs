﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzClient {
    public partial class ControlDragDropQuestion : UserControl {
        public ControlDragDropQuestion() {
            InitializeComponent();
        }

        public ControlDragDropQuestion(string Value) {
            InitializeComponent();
            tbxQuestion.Text = Value;
        }

        public string ValueText { get { return tbxQuestion.Text; } set { tbxQuestion.Text = value; } }
    }
}

