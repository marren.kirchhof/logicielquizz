﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QuizzClient.Constantes.UI;

namespace QuizzClient {
    public partial class FillQuizzView : Form {

        private User _CurrentUser;
        private Quizz _CurrentQuizz;

        private Dictionary<Question, bool> _QuestionAnswered = new Dictionary<Question, bool>();

        public FillQuizzView(User ConnectedUser,Quizz CurrentQuizz) {
            InitializeComponent();
            SetUI();
            _CurrentQuizz = CurrentQuizz;
            _CurrentUser = ConnectedUser;
            load();
        }

        #region UI
        private void SetUI() {
            this.DoubleBuffered = true;                          //  Necessary for resize when you don't have border --> Border resize
            this.SetStyle(ControlStyles.ResizeRedraw, true);     //  Border resize
            sbFillView.Text = Constantes.TitleForm.ViewMain;
        }

        #region Resize form

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }

        #endregion
        #endregion

        /// <summary>
        /// Load all question of the quizz in the interface
        /// </summary>
        private void load() {
            if (_CurrentQuizz != null && _CurrentQuizz.ListQuestion != null) {
                lbInfoQuizz.Text = _CurrentQuizz.name;
                foreach (Question q in _CurrentQuizz.ListQuestion) {
                    switch (q.idType) {
                        case 1:
                            flpQuestion.Controls.Add(NewQuestionDragDrop(q));
                            break;
                        case 2:
                            flpQuestion.Controls.Add(NewQuestionVraiFaux(q));
                            break;
                        case 3:
                            flpQuestion.Controls.Add(NewQuestionQCM(q));
                            break;
                    }
                    _QuestionAnswered.Add(q, false);
                }
            }
        }

        /// <summary>
        /// Create a new question Qcm control without user answer
        /// </summary>
        /// <param name="Q">Question to ask to the user</param>
        /// <returns>Control created</returns>
        #region Create Question
        private ControlQcm NewQuestionQCM(Question Q) {
            ControlQcm cQcm = new ControlQcm();
            cQcm.question = Q;

            Control FlpQcm = cQcm.Controls.Find("flpResponse", false)[0];
            try {
                for (int i = 0; i < FlpQcm.Controls.Count; i++) {
                    FlpQcm.Controls[i].Tag = Q;
                    FlpQcm.Controls[i].Click += Question_Click;
                }
            } catch { }
            return cQcm;
        }
        /// <summary>
        /// Create a new question DragDrop control without user answer
        /// </summary>
        /// <param name="Q">Question to ask to the user</param>
        /// <returns>Control created</returns>
        private ControlDragDrop NewQuestionDragDrop(Question Q) {
            ControlDragDrop cDragDrop = new ControlDragDrop();
            cDragDrop.question = Q;
            Control FlpQuestionDragDrop = cDragDrop.Controls.Find("flpQuestion", false)[0];
            try {
                for (int i = 0 ; i < FlpQuestionDragDrop.Controls.Count; i++) {
                    FlpQuestionDragDrop.Controls[i].Tag = Q;
                    FlpQuestionDragDrop.Controls[i].Click += Question_Click;
                }
            } 
            catch { }
            return cDragDrop;
        }
        /// <summary>
        /// Create a new question Truefalse control without user answer
        /// </summary>
        /// <param name="Q">Question to ask to the user</param>
        /// <returns>Control created</returns>
        private ControlVraiFaux NewQuestionVraiFaux(Question Q) {
            ControlVraiFaux cVraiFaux = new ControlVraiFaux();
            cVraiFaux.Width = flpQuestion.Width;
            cVraiFaux.question = Q;
            try {
                //Control rbTrue = cVraiFaux.Controls.Find("rbTrue", false)[0];
                Control rbTrue = cVraiFaux.Controls.Find("gbxReponse", false)[0].Controls[3];
                rbTrue.Tag = Q;
                ((CxFlatUI.CxFlatRadioButton)rbTrue).CheckedChanged += Question_Click;
                //Control rbFalse = cVraiFaux.Controls.Find("rbFalse", false)[0];
                Control rbFalse = cVraiFaux.Controls.Find("gbxReponse", false)[0].Controls[2];
                rbFalse.Tag = Q;
                ((CxFlatUI.CxFlatRadioButton)rbFalse).CheckedChanged += Question_Click;
            } catch (Exception  ex )
            { ex.ToString(); }
            return cVraiFaux;
        }
        #endregion

        #region proggress bar
        /// <summary>
        /// Set the progression of the user in the quizz 
        /// </summary>
        private void Question_Click(object sender, EventArgs e) {
            _QuestionAnswered[((Question)((Control)sender).Tag)] = true;

            double sumAll = _QuestionAnswered.Count;
            double sumAllAswered = 0;
            foreach (KeyValuePair<Question, bool> i in _QuestionAnswered) {
                if (i.Value) { sumAllAswered++; }
            }

            pbQuizz.ValueNumber = Convert.ToInt16((sumAllAswered / sumAll) * 100);
            pbRound.ValueNumber = Convert.ToInt16((sumAllAswered / sumAll) * 100);

            if (pbRound.ValueNumber == 100) {
                btnSend.Visible = true;
                pbRound.Visible = false;
            }

        }





        #endregion


        /// <summary>
        /// Create a list of response and upload this data with the fonction UploadResponse
        /// </summary>
        private void btnSend_Click(object sender, EventArgs e) {
            _QuestionAnswered.ToString();

            List<Response> LR = new List<Response>();

            foreach (Control c in flpQuestion.Controls) {
                if (c is ControlVraiFaux) {
                    string JsonString = ((ControlVraiFaux)c).ResponseJsonString;
                    Question QuestionBase = ((ControlVraiFaux)c).question;
                    LR.Add(new Response(_CurrentQuizz.idQuizz, _CurrentUser.idUser, QuestionBase.idQuestion, JsonString));
                }
                if (c is ControlDragDrop) {
                    string JsonString = ((ControlDragDrop)c).ResponseJsonString;
                    Question QuestionBase = ((ControlDragDrop)c).question;
                    LR.Add(new Response(_CurrentQuizz.idQuizz, _CurrentUser.idUser, QuestionBase.idQuestion, JsonString));
                }
                if (c is ControlQcm) {
                    string JsonString = ((ControlQcm)c).ResponseJsonString;
                    Question QuestionBase = ((ControlQcm)c).question;
                    LR.Add(new Response(_CurrentQuizz.idQuizz, _CurrentUser.idUser, QuestionBase.idQuestion, JsonString));
                }
            }
            UploadResponses(LR);
        }

        /// <summary>
        /// Upload all responses set in this quizz
        /// </summary>
        /// <param name="ListResponses">List of responses create by the user</param>
        private async void UploadResponses(List<Response> ListResponses) {
            foreach (Response r in ListResponses) {
                Task<Response> R = HttpRequest.ResponseRequest.Create(r.idQuestion,r.idQuizz,r.idUser,r.respUser);
                Response rResultat = await R;
                this.Close();
            }
        }

    }
}
