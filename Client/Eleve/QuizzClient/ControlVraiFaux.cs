﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzClient {
    public partial class ControlVraiFaux : UserControl {

        private Question Q;


        public ControlVraiFaux() {
            InitializeComponent();
        }

        public Question question {
            get { return Q; }
            set {

                Q = value;
                lbQuestionText.Text = value.title;
                VraiFaux vf = Newtonsoft.Json.JsonConvert.DeserializeObject<VraiFaux>(value.correction);

                tbxFalse.Text = vf.ResponseFalse;
                tbxFalse.Enabled = false;
                tbxTrue.Text = vf.ResponseTrue;
                tbxTrue.Enabled = false;

            }
        }

        public Question CorrectedQuestion {
            get { return Q; }
            set {

                Q = value;
                lbQuestionText.Text = value.title;
                VraiFaux vf = Newtonsoft.Json.JsonConvert.DeserializeObject<VraiFaux>(value.correction);

                tbxFalse.Text = vf.ResponseFalse;
                tbxFalse.Enabled = false;
                tbxTrue.Text = vf.ResponseTrue;
                tbxTrue.Enabled = false;

                rbTrue.Checked = vf.ResponseCorrection;
                rbFalse.Checked = !vf.ResponseCorrection;

                rbFalse.Enabled = rbTrue.Enabled = false;

            }

        }

        public void SetColorCorrection() {
            lbInfoVraiFaux.ButtonType = CxFlatUI.ButtonType.Success;
        }


        public string ResponseJsonString { get { return GetJsonStringResponse(); } }

        private string GetJsonStringResponse() {
            VraiFaux R = new VraiFaux(rbTrue.Checked, tbxTrue.Text, tbxFalse.Text);
            return Newtonsoft.Json.JsonConvert.SerializeObject(R);
        }

    }
}
