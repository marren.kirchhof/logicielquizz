﻿namespace QuizzClient {
    partial class ControlDragDrop {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.lbQuestionText = new CxFlatUI.CxFlatTextBox();
            this.flpReponse = new System.Windows.Forms.FlowLayoutPanel();
            this.flpQuestion = new System.Windows.Forms.FlowLayoutPanel();
            this.lbInfoDragDrop = new CxFlatUI.Controls.CxFlatButton();
            this.lbInfoColoumnQuestion = new CxFlatUI.Controls.CxFlatButton();
            this.lbInfoColumnReponse = new CxFlatUI.Controls.CxFlatButton();
            this.SuspendLayout();
            // 
            // lbQuestionText
            // 
            this.lbQuestionText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbQuestionText.Enabled = false;
            this.lbQuestionText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbQuestionText.Hint = "";
            this.lbQuestionText.Location = new System.Drawing.Point(27, 42);
            this.lbQuestionText.MaxLength = 32767;
            this.lbQuestionText.Multiline = true;
            this.lbQuestionText.Name = "lbQuestionText";
            this.lbQuestionText.PasswordChar = '\0';
            this.lbQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbQuestionText.SelectedText = "";
            this.lbQuestionText.SelectionLength = 0;
            this.lbQuestionText.SelectionStart = 0;
            this.lbQuestionText.Size = new System.Drawing.Size(464, 45);
            this.lbQuestionText.TabIndex = 24;
            this.lbQuestionText.TabStop = false;
            this.lbQuestionText.Text = "Question";
            this.lbQuestionText.UseSystemPasswordChar = false;
            // 
            // flpReponse
            // 
            this.flpReponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpReponse.AutoScroll = true;
            this.flpReponse.BackColor = System.Drawing.Color.White;
            this.flpReponse.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpReponse.Location = new System.Drawing.Point(260, 118);
            this.flpReponse.Name = "flpReponse";
            this.flpReponse.Size = new System.Drawing.Size(231, 197);
            this.flpReponse.TabIndex = 23;
            this.flpReponse.WrapContents = false;
            // 
            // flpQuestion
            // 
            this.flpQuestion.AllowDrop = true;
            this.flpQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flpQuestion.AutoScroll = true;
            this.flpQuestion.BackColor = System.Drawing.Color.White;
            this.flpQuestion.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuestion.Location = new System.Drawing.Point(27, 118);
            this.flpQuestion.Name = "flpQuestion";
            this.flpQuestion.Size = new System.Drawing.Size(227, 197);
            this.flpQuestion.TabIndex = 22;
            this.flpQuestion.WrapContents = false;
            this.flpQuestion.Click += new System.EventHandler(this.flpQuestion_Click);
            // 
            // lbInfoDragDrop
            // 
            this.lbInfoDragDrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoDragDrop.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoDragDrop.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoDragDrop.Location = new System.Drawing.Point(0, 0);
            this.lbInfoDragDrop.Name = "lbInfoDragDrop";
            this.lbInfoDragDrop.Size = new System.Drawing.Size(519, 36);
            this.lbInfoDragDrop.TabIndex = 25;
            this.lbInfoDragDrop.Text = "Drag and drop";
            this.lbInfoDragDrop.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoColoumnQuestion
            // 
            this.lbInfoColoumnQuestion.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoColoumnQuestion.Enabled = false;
            this.lbInfoColoumnQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoColoumnQuestion.Location = new System.Drawing.Point(27, 93);
            this.lbInfoColoumnQuestion.Name = "lbInfoColoumnQuestion";
            this.lbInfoColoumnQuestion.Size = new System.Drawing.Size(227, 19);
            this.lbInfoColoumnQuestion.TabIndex = 26;
            this.lbInfoColoumnQuestion.Text = "Choix";
            this.lbInfoColoumnQuestion.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoColumnReponse
            // 
            this.lbInfoColumnReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoColumnReponse.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoColumnReponse.Enabled = false;
            this.lbInfoColumnReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoColumnReponse.Location = new System.Drawing.Point(260, 93);
            this.lbInfoColumnReponse.Name = "lbInfoColumnReponse";
            this.lbInfoColumnReponse.Size = new System.Drawing.Size(231, 19);
            this.lbInfoColumnReponse.TabIndex = 27;
            this.lbInfoColumnReponse.Text = "Réponses possibles";
            this.lbInfoColumnReponse.TextColor = System.Drawing.Color.White;
            // 
            // ControlDragDrop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbInfoColumnReponse);
            this.Controls.Add(this.lbInfoColoumnQuestion);
            this.Controls.Add(this.flpReponse);
            this.Controls.Add(this.lbInfoDragDrop);
            this.Controls.Add(this.lbQuestionText);
            this.Controls.Add(this.flpQuestion);
            this.Name = "ControlDragDrop";
            this.Size = new System.Drawing.Size(519, 318);
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatTextBox lbQuestionText;
        private System.Windows.Forms.FlowLayoutPanel flpReponse;
        private System.Windows.Forms.FlowLayoutPanel flpQuestion;
        private CxFlatUI.Controls.CxFlatButton lbInfoColoumnQuestion;
        public CxFlatUI.Controls.CxFlatButton lbInfoColumnReponse;
        public CxFlatUI.Controls.CxFlatButton lbInfoDragDrop;
    }
}
