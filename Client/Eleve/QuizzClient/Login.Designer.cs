﻿namespace QuizzClient {
    partial class Login {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.sbLogin = new CxFlatUI.CxFlatStatusBar();
            this.tbxPassword = new CxFlatUI.CxFlatTextBox();
            this.btnLogin = new CxFlatUI.Controls.CxFlatButton();
            this.tbxuserName = new CxFlatUI.CxFlatTextBox();
            this.cxFlatPictureBox1 = new CxFlatUI.CxFlatPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.cxFlatPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // sbLogin
            // 
            this.sbLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbLogin.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbLogin.Location = new System.Drawing.Point(0, 0);
            this.sbLogin.Name = "sbLogin";
            this.sbLogin.Size = new System.Drawing.Size(414, 40);
            this.sbLogin.TabIndex = 1;
            this.sbLogin.Text = "Login";
            this.sbLogin.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // tbxPassword
            // 
            this.tbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxPassword.Hint = "Password";
            this.tbxPassword.Location = new System.Drawing.Point(12, 156);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.PasswordChar = '*';
            this.tbxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPassword.SelectedText = "";
            this.tbxPassword.SelectionLength = 0;
            this.tbxPassword.SelectionStart = 0;
            this.tbxPassword.Size = new System.Drawing.Size(389, 38);
            this.tbxPassword.TabIndex = 5;
            this.tbxPassword.TabStop = false;
            this.tbxPassword.UseSystemPasswordChar = false;
            this.tbxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxPassword_KeyDown);
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogin.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnLogin.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLogin.Location = new System.Drawing.Point(12, 205);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(389, 23);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.TabStop = false;
            this.btnLogin.Text = "Login";
            this.btnLogin.TextColor = System.Drawing.Color.White;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // tbxuserName
            // 
            this.tbxuserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxuserName.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxuserName.Hint = "Nom d\'utilisateur";
            this.tbxuserName.Location = new System.Drawing.Point(12, 112);
            this.tbxuserName.MaxLength = 32767;
            this.tbxuserName.Multiline = false;
            this.tbxuserName.Name = "tbxuserName";
            this.tbxuserName.PasswordChar = '\0';
            this.tbxuserName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxuserName.SelectedText = "";
            this.tbxuserName.SelectionLength = 0;
            this.tbxuserName.SelectionStart = 0;
            this.tbxuserName.Size = new System.Drawing.Size(389, 38);
            this.tbxuserName.TabIndex = 4;
            this.tbxuserName.TabStop = false;
            this.tbxuserName.UseSystemPasswordChar = false;
            // 
            // cxFlatPictureBox1
            // 
            this.cxFlatPictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cxFlatPictureBox1.Image = global::QuizzClient.Properties.Resources.login;
            this.cxFlatPictureBox1.Location = new System.Drawing.Point(159, 46);
            this.cxFlatPictureBox1.Name = "cxFlatPictureBox1";
            this.cxFlatPictureBox1.Size = new System.Drawing.Size(90, 60);
            this.cxFlatPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cxFlatPictureBox1.TabIndex = 7;
            this.cxFlatPictureBox1.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 241);
            this.Controls.Add(this.cxFlatPictureBox1);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.tbxuserName);
            this.Controls.Add(this.sbLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(800, 241);
            this.MinimumSize = new System.Drawing.Size(414, 241);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.cxFlatPictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbLogin;
        private CxFlatUI.CxFlatTextBox tbxPassword;
        private CxFlatUI.Controls.CxFlatButton btnLogin;
        private CxFlatUI.CxFlatTextBox tbxuserName;
        private CxFlatUI.CxFlatPictureBox cxFlatPictureBox1;
    }
}

