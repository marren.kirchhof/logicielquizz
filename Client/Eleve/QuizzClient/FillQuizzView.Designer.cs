﻿namespace QuizzClient {
    partial class FillQuizzView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.sbFillView = new CxFlatUI.CxFlatStatusBar();
            this.gbxMain = new CxFlatUI.CxFlatGroupBox();
            this.flpQuestion = new System.Windows.Forms.FlowLayoutPanel();
            this.pbQuizz = new CxFlatUI.CxFlatProgressBar();
            this.lbInfoQuizz = new CxFlatUI.Controls.CxFlatButton();
            this.btnSend = new CxFlatUI.CxFlatRoundButton();
            this.pbRound = new CxFlatUI.CxFlatRoundProgressBar();
            this.gbxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbFillView
            // 
            this.sbFillView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbFillView.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbFillView.Location = new System.Drawing.Point(0, 0);
            this.sbFillView.Name = "sbFillView";
            this.sbFillView.Size = new System.Drawing.Size(1110, 40);
            this.sbFillView.TabIndex = 2;
            this.sbFillView.Text = "Remplir le quizz";
            this.sbFillView.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // gbxMain
            // 
            this.gbxMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxMain.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxMain.Controls.Add(this.flpQuestion);
            this.gbxMain.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxMain.Location = new System.Drawing.Point(44, 90);
            this.gbxMain.Name = "gbxMain";
            this.gbxMain.ShowText = false;
            this.gbxMain.Size = new System.Drawing.Size(996, 522);
            this.gbxMain.TabIndex = 3;
            this.gbxMain.TabStop = false;
            this.gbxMain.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // flpQuestion
            // 
            this.flpQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpQuestion.AutoScroll = true;
            this.flpQuestion.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuestion.Location = new System.Drawing.Point(201, 28);
            this.flpQuestion.Name = "flpQuestion";
            this.flpQuestion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flpQuestion.Size = new System.Drawing.Size(615, 478);
            this.flpQuestion.TabIndex = 0;
            this.flpQuestion.WrapContents = false;
            // 
            // pbQuizz
            // 
            this.pbQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbQuizz.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pbQuizz.IsError = false;
            this.pbQuizz.Location = new System.Drawing.Point(273, 52);
            this.pbQuizz.Name = "pbQuizz";
            this.pbQuizz.ProgressBarStyle = CxFlatUI.CxFlatProgressBar.Style.ToolTip;
            this.pbQuizz.Size = new System.Drawing.Size(767, 32);
            this.pbQuizz.TabIndex = 4;
            this.pbQuizz.ValueNumber = 0;
            // 
            // lbInfoQuizz
            // 
            this.lbInfoQuizz.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoQuizz.Enabled = false;
            this.lbInfoQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoQuizz.Location = new System.Drawing.Point(44, 52);
            this.lbInfoQuizz.Name = "lbInfoQuizz";
            this.lbInfoQuizz.Size = new System.Drawing.Size(223, 32);
            this.lbInfoQuizz.TabIndex = 5;
            this.lbInfoQuizz.Text = " Quizz";
            this.lbInfoQuizz.TextColor = System.Drawing.Color.White;
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.ButtonType = CxFlatUI.ButtonType.Success;
            this.btnSend.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSend.Location = new System.Drawing.Point(884, 618);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(156, 63);
            this.btnSend.TabIndex = 6;
            this.btnSend.Text = "Valider";
            this.btnSend.TextColor = System.Drawing.Color.White;
            this.btnSend.Visible = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // pbRound
            // 
            this.pbRound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbRound.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.pbRound.IsError = false;
            this.pbRound.Location = new System.Drawing.Point(933, 618);
            this.pbRound.Name = "pbRound";
            this.pbRound.Size = new System.Drawing.Size(63, 63);
            this.pbRound.TabIndex = 7;
            this.pbRound.Text = "pbRound";
            this.pbRound.ValueNumber = 0;
            // 
            // FillQuizzView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1110, 693);
            this.Controls.Add(this.pbRound);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.lbInfoQuizz);
            this.Controls.Add(this.pbQuizz);
            this.Controls.Add(this.gbxMain);
            this.Controls.Add(this.sbFillView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.Name = "FillQuizzView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FillQuizzView";
            this.gbxMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbFillView;
        private CxFlatUI.CxFlatGroupBox gbxMain;
        private System.Windows.Forms.FlowLayoutPanel flpQuestion;
        private CxFlatUI.CxFlatProgressBar pbQuizz;
        private CxFlatUI.Controls.CxFlatButton lbInfoQuizz;
        private CxFlatUI.CxFlatRoundButton btnSend;
        private CxFlatUI.CxFlatRoundProgressBar pbRound;
    }
}