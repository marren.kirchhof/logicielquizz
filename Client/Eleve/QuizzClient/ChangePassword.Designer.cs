﻿namespace QuizzClient {
    partial class ChangePassword {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.sbViewPassword = new CxFlatUI.CxFlatStatusBar();
            this.btnLogOut = new CxFlatUI.CxFlatRoundButton();
            this.btnAction = new CxFlatUI.CxFlatSimpleButton();
            this.tbxConfirmPassword = new CxFlatUI.CxFlatTextBox();
            this.tbxPassword = new CxFlatUI.CxFlatTextBox();
            this.SuspendLayout();
            // 
            // sbViewPassword
            // 
            this.sbViewPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewPassword.Location = new System.Drawing.Point(0, 0);
            this.sbViewPassword.Name = "sbViewPassword";
            this.sbViewPassword.Size = new System.Drawing.Size(469, 40);
            this.sbViewPassword.TabIndex = 1;
            this.sbViewPassword.Text = "changement de mot de passe";
            this.sbViewPassword.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnLogOut
            // 
            this.btnLogOut.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnLogOut.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLogOut.Location = new System.Drawing.Point(12, 46);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(127, 23);
            this.btnLogOut.TabIndex = 3;
            this.btnLogOut.Text = "retour";
            this.btnLogOut.TextColor = System.Drawing.Color.White;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnAction.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAction.Location = new System.Drawing.Point(266, 79);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(191, 83);
            this.btnAction.TabIndex = 7;
            this.btnAction.Text = "Modifier";
            this.btnAction.TextColor = System.Drawing.Color.White;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // tbxConfirmPassword
            // 
            this.tbxConfirmPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxConfirmPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxConfirmPassword.Hint = "Confirmation du mot de passe";
            this.tbxConfirmPassword.Location = new System.Drawing.Point(12, 124);
            this.tbxConfirmPassword.MaxLength = 32767;
            this.tbxConfirmPassword.Multiline = false;
            this.tbxConfirmPassword.Name = "tbxConfirmPassword";
            this.tbxConfirmPassword.PasswordChar = '*';
            this.tbxConfirmPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxConfirmPassword.SelectedText = "";
            this.tbxConfirmPassword.SelectionLength = 0;
            this.tbxConfirmPassword.SelectionStart = 0;
            this.tbxConfirmPassword.Size = new System.Drawing.Size(248, 38);
            this.tbxConfirmPassword.TabIndex = 6;
            this.tbxConfirmPassword.TabStop = false;
            this.tbxConfirmPassword.UseSystemPasswordChar = false;
            // 
            // tbxPassword
            // 
            this.tbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxPassword.Hint = "Mot de passe";
            this.tbxPassword.Location = new System.Drawing.Point(12, 80);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.PasswordChar = '*';
            this.tbxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPassword.SelectedText = "";
            this.tbxPassword.SelectionLength = 0;
            this.tbxPassword.SelectionStart = 0;
            this.tbxPassword.Size = new System.Drawing.Size(248, 38);
            this.tbxPassword.TabIndex = 5;
            this.tbxPassword.TabStop = false;
            this.tbxPassword.UseSystemPasswordChar = false;
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 174);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.tbxConfirmPassword);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.sbViewPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(469, 174);
            this.Name = "ChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChangePassword";
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewPassword;
        private CxFlatUI.CxFlatRoundButton btnLogOut;
        private CxFlatUI.CxFlatSimpleButton btnAction;
        private CxFlatUI.CxFlatTextBox tbxConfirmPassword;
        private CxFlatUI.CxFlatTextBox tbxPassword;
    }
}