﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flurl.Http;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Converters;

namespace QuizzClient {
    public static class HttpRequest {

        public static class RolesRequest {
            public static async Task<List<Role>> GetAll() {
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Roles.GetAll).GetStringAsync();
                List<Role> RetournedTypes = JsonConvert.DeserializeObject<List<Role>>(getResp);
                return RetournedTypes;
            }
        }

        public static class UsersRequest {
            public static async Task<User> Find(string username) {
                List<User> Users;
                User Uresult = null;
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Users.Find, username).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    Users = JsonConvert.DeserializeObject<List<User>>(getResp);
                    Uresult = Users[0];
                }
                return Uresult;
            }

            public static async Task<List<User>> GetAll() {
                List<User> Users;
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Users.GetAll).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    Users = JsonConvert.DeserializeObject<List<User>>(getResp);
                } else {
                    Users = null;
                }
                return Users;
            }

            public static async Task<User> Create(string username, string Sha256Password, int iRole) {
                var getResp = await (string.Format(Constantes.URL.RequestHTTP.Users.Create, username, Sha256Password, iRole)).PostJsonAsync(new { }).ReceiveString();
                User UserCreated = JsonConvert.DeserializeObject<User>(getResp);
                return UserCreated;
            }

            public static async Task<bool> Update(int id, string username, string Sha256Password, int iRole) {
                bool success = true;
                var getResp = await (string.Format(Constantes.URL.RequestHTTP.Users.Update, id, username, Sha256Password, iRole)).PutJsonAsync(new { });

                if (!getResp.IsSuccessStatusCode) {
                    success = false;
                }
                return success;
            }
            public static async Task<bool> Delete(int id) {
                HttpResponseMessage getResp = await string.Format(Constantes.URL.RequestHTTP.Users.Delete, id).DeleteAsync();
                return getResp.IsSuccessStatusCode;
            }

            public static async Task<List<User>> GetAllUserLinkedToQuizz(int idQuizz) {
                List<User> Users;
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Quizzes.UsersQuizz.GetAll, idQuizz).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    Users = JsonConvert.DeserializeObject<List<User>>(getResp);
                } else {
                    Users = null;
                }
                return Users;
            }

            public static async Task<bool> LinkToQuizz(int iduser, int idquizz) {
                HttpResponseMessage getResp = await (string.Format(Constantes.URL.RequestHTTP.Users.UserQuizzes.PutAttach, iduser, idquizz)).PutJsonAsync(new { });
                return (getResp.IsSuccessStatusCode);
            }
            public static async Task<bool> DetachToQuizz(int iduser, int idquizz) {
                HttpResponseMessage getResp = await (string.Format(Constantes.URL.RequestHTTP.Users.UserQuizzes.DeleteDetach, iduser, idquizz)).DeleteAsync();
                return (getResp.IsSuccessStatusCode);
            }




        }
        
        public static class QuizzesRequest {

            //Get the quizzes wich the user must answer
            public static async Task<List<Quizz>> GetQuizzesLinkedToUsers(int idUser) {
                List<Quizz> LQuizzes;
                string s = string.Format(Constantes.URL.RequestHTTP.Users.UserQuizzes.GetAll, idUser);
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Users.UserQuizzes.GetAll, idUser).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    LQuizzes = JsonConvert.DeserializeObject<List<Quizz>>(getResp);
                } else {
                    LQuizzes = null;
                }
                return LQuizzes;
            }

            public static async Task<Quizz> Create(string name, int idOwner, DateTime date) {
                var getResp = await (string.Format(Constantes.URL.RequestHTTP.Quizzes.Create, name, idOwner, date.ToShortDateString())).PostJsonAsync(new { }).ReceiveString();
                Quizz QuizzCreated = JsonConvert.DeserializeObject<Quizz>(getResp,
                new IsoDateTimeConverter {
                    DateTimeFormat = Constantes.Global.DateFormatClient
                });
                return QuizzCreated;
            }
            public static async Task<bool> Update(int idQuizz, string name, int idOwner, DateTime date) {
                HttpResponseMessage getResp = await (string.Format(Constantes.URL.RequestHTTP.Quizzes.Update, idQuizz, name, idOwner, date.ToShortDateString())).PutJsonAsync(new { });
                return (getResp.IsSuccessStatusCode);
            }

            public static async Task<List<Quizz>> GetQuizzesWhoUserIsOwner(int idUser) {
                List<Quizz> LQuizzes;
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Users.UserQuizzesOwned.GetAll, idUser).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    LQuizzes = JsonConvert.DeserializeObject<List<Quizz>>(getResp);
                } else {
                    LQuizzes = null;
                }
                return LQuizzes;
            }


            public static async Task<bool> Delete(int idquizz) {
                HttpResponseMessage getResp = await (string.Format(Constantes.URL.RequestHTTP.Quizzes.Delete, idquizz)).DeleteAsync();
                return getResp.IsSuccessStatusCode;
            }

        }
        
        public static class QuestionRequest {
            public static async Task<List<Question>> GetAll() {
                List<Question> Users;
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Questions.GetAll).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    Users = JsonConvert.DeserializeObject<List<Question>>(getResp);
                } else {
                    Users = null;
                }
                return Users;
            }
            public static async Task<bool> Delete(int id) {
                HttpResponseMessage getResp = await string.Format(Constantes.URL.RequestHTTP.Questions.Delete, id).DeleteAsync();
                return getResp.IsSuccessStatusCode;
            }


            public static async Task<List<Question>> GetAllQuestionsLinkedToQuizz(int idQuizz) {
                List<Question> Users;
                string s = string.Format(Constantes.URL.RequestHTTP.Quizzes.QuizzesQuestions.GetAll, idQuizz);
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Quizzes.QuizzesQuestions.GetAll, idQuizz).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    Users = JsonConvert.DeserializeObject<List<Question>>(getResp);
                } else {
                    Users = null;
                }
                return Users;
            }
            public static async Task<Question> GetOne(int idquestion) {
                Question rQuestion = null;
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Questions.GetOne, idquestion).GetStringAsync();
                if (getResp != Constantes.Global.EmptyJson) {
                    rQuestion = JsonConvert.DeserializeObject<Question>(getResp);
                }
                return rQuestion;
            }

            public static async Task<Question> Create(string title, int idQuizz, int idType, string JSONCorrection) {
                var getResp = await (string.Format(Constantes.URL.RequestHTTP.Questions.Create, title, JSONCorrection, idType, idQuizz)).PostJsonAsync(new { }).ReceiveString();
                Question QuestionCreated = JsonConvert.DeserializeObject<Question>(getResp);
                return QuestionCreated;
            }

            public static async Task<bool> Update(int idQuestion, string title, int idQuizz, int idType, string JSONCorrection) {
                HttpResponseMessage getResp = await (string.Format(Constantes.URL.RequestHTTP.Questions.Update, idQuestion, title, JSONCorrection, idType, idQuizz).PutJsonAsync(new { }));
                return (getResp.IsSuccessStatusCode);
            }
        }
        
        public static class ResponseRequest {
            public static async Task<List<Response>> GetAlllinkedToUserAndQuizz(int idUser,int idquizz) {
                var getResp = await string.Format(Constantes.URL.RequestHTTP.responses.GetAllFromUserAndQuizz, idUser, idquizz).GetStringAsync();
                List<Response> RetournedTypes = JsonConvert.DeserializeObject<List<Response>>(getResp);
                return RetournedTypes;
            }

            public static async Task<Response> Create(int idQuestion, int idQuizz, int idUser, string JSONCorrection) {
                var getResp = await (string.Format(Constantes.URL.RequestHTTP.responses.Create,JSONCorrection,idQuizz,idUser,idQuestion)).PostJsonAsync(new { }).ReceiveString();
                Response ResponseCreated = JsonConvert.DeserializeObject<Response>(getResp);
                return ResponseCreated;
            }

        }

        public static class TypeQuestionRequest {
            public static async Task<List<QuestionType>> GetAll() {
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Types.GetAllType).GetStringAsync();
                List<QuestionType> RetournedTypes = JsonConvert.DeserializeObject<List<QuestionType>>(getResp);
                return RetournedTypes;
            }
            public static async Task<QuestionType> GetOne(int id) {
                string s = string.Format(Constantes.URL.RequestHTTP.Types.GetOne, id);
                var getResp = await string.Format(Constantes.URL.RequestHTTP.Types.GetOne, id).GetStringAsync();
                QuestionType RetournedType = JsonConvert.DeserializeObject<QuestionType>(getResp);
                return RetournedType;
            }

        }




    }
}
