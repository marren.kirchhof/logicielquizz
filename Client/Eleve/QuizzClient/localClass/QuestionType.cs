﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzClient {
    public class QuestionType {
        public QuestionType(int type_id,string nom) {
            idType = type_id;
            name = nom;
        }

        public int idType { get; set; }
        public string name { get; set; }
    }
}
