﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzClient {
    public class Response {

        public Response(int idResponse, int idQuizz,int idUser,int idQuestion, string respUser) {
            this.idResponse = idResponse;
            this.idQuizz = idQuizz;
            this.idUser = idUser;
            this.idQuestion = idQuestion;
            this.respUser = respUser;
        }

        public Response(int idQuizz, int idUser, int idQuestion, string respUser) {
            this.idResponse = idResponse;
            this.idQuizz = idQuizz;
            this.idUser = idUser;
            this.idQuestion = idQuestion;
            this.respUser = respUser;
        }

        public Response() {
        }

        public int idResponse { get; set; }
        public int idQuizz { get; set; }
        public int idUser { get; set; }
        public int idQuestion { get; set; }
        public string respUser { get; set; }


    }
}
