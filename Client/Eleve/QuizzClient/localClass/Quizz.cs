﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzClient {
    public class Quizz {
        private DateTime _Date;
        public Quizz(int id, string title, DateTime date, int UserOwner) {
            idQuizz = id;
            name = title;
            _Date = date;
            idUser = UserOwner;
        }

        public int idQuizz { get; set; }
        public string name { get; set; }

        public DateTime date {
            get { return _Date; }
            set { try { Convert.ToDateTime(value); } catch { } }
        }
        public int idUser { get; set; }

        public List<Question> ListQuestion { get; set; }
        public List<Response> ListResponse { get; set; }


        public int percentQuizz { get {
                int resPourcentage = 0;
                if (ListQuestion != null && ListResponse != null) {
                    resPourcentage = ListResponse.Count * 100 / ListQuestion.Count;
                }
                return resPourcentage;

            } }
        

    }
}
