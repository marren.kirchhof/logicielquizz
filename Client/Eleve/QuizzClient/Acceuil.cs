﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QuizzClient.Constantes.UI;
using System.Windows.Forms;

namespace QuizzClient {
    public partial class AcceuilView : Form {

        User _ConnectedUser;
        Quizz _CurrentQuizz;

        bool bCorrectionNeedRefresh = true;

        //represents the user's chosen action
        string _Action;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ConnectedUser">the connected user</param>
        public AcceuilView(User ConnectedUser) {
            InitializeComponent();
            lbInfo.Text = ConnectedUser.username;
            _ConnectedUser = ConnectedUser;
            SetUI();
            InitComposants();
            RefreshQuizzesAsync();
        }

        /// <summary>
        /// init the timer and the others composants
        /// </summary>
        private void InitComposants() {
            tRefresh.Interval = Constantes.Refresh.Interval;
            tRefresh.Enabled = Constantes.Refresh.Enabled;
        }

        #region UI
        /// <summary>
        /// Set the style of the UI 
        /// </summary>
        private void SetUI() {
            this.DoubleBuffered = true;                          //  Necessary for resize when you don't have border --> Border resize
            this.SetStyle(ControlStyles.ResizeRedraw, true);     //  Border resize
            sbAcceuil.Text = Constantes.TitleForm.ViewMain;
            lbxQuizz.DisplayMember = Constantes.DisplayListbox.ListboxQuizzes;
        }

        #region Resize form

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }

        #endregion
        #endregion

        #region Refresh
        /// <summary>
        /// Refresh the interface 
        /// </summary>
        private void tRefresh_Tick(object sender, EventArgs e) {
            RefreshQuizzesAsync();
            lbxQuizz_SelectedIndexChanged(lbxQuizz, null);
        }
        /// <summary>
        /// Refresh the quizz list without unselect
        /// </summary>
        public async void RefreshQuizzesAsync() {

            try {
                Task<List<Quizz>> R = HttpRequest.QuizzesRequest.GetQuizzesLinkedToUsers(_ConnectedUser.idUser);
                List<Quizz> LResultat = await R;

                if (LResultat != null) {
                    _ConnectedUser.quizzes_owned = LResultat;
                    foreach (Quizz qToAdd in _ConnectedUser.quizzes_owned) {
                        bool AddThisQuizz = true;
                        foreach (Quizz qSources in lbxQuizz.Items) {
                            if (qToAdd.idQuizz == qSources.idQuizz) {
                                AddThisQuizz = false;
                            }
                        }
                        if (AddThisQuizz) {
                            lbxQuizz.Items.Add(qToAdd);
                        }
                    }

                    List<Quizz> ListToDel = new List<Quizz>();
                    foreach (Quizz qtoDel in lbxQuizz.Items) {
                        bool DelThisQuizz = true;
                        foreach (Quizz qSources in _ConnectedUser.quizzes_owned) {
                            if (qtoDel.idQuizz == qSources.idQuizz) {
                                DelThisQuizz = false;
                            }
                        }
                        if (DelThisQuizz) {
                            ListToDel.Add(qtoDel);
                        }
                    }
                    foreach (Quizz delQ in ListToDel) {
                        lbxQuizz.Items.Remove(delQ);
                    }
                }


            } catch (Exception ex)
            {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }


        #endregion

        #region Button 
        private void btnLogOut_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnResetPassword_Click(object sender, EventArgs e) {
            ChangePassword c = new ChangePassword(_ConnectedUser);
            Visible = false;
            c.ShowDialog();
            Visible = true;
        }





        #endregion


        /// <summary>
        /// Set the interface according to the quizz
        /// </summary>
        private async void lbxQuizz_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                if ((Quizz)((ListBox)sender).SelectedItem != null) {

                    if (_CurrentQuizz != null && _CurrentQuizz.idQuizz != ((Quizz)((ListBox)sender).SelectedItem).idQuizz) {
                        bCorrectionNeedRefresh = true;
                    }

                    _CurrentQuizz = ((Quizz)((ListBox)sender).SelectedItem);
                    Task<List<Question>> RlQuestions = HttpRequest.QuestionRequest.GetAllQuestionsLinkedToQuizz(_CurrentQuizz.idQuizz);
                    List<Question> ListQuestions = await RlQuestions;
                    _CurrentQuizz.ListQuestion = ListQuestions;


                    Task<List<Response>> RlReponses = HttpRequest.ResponseRequest.GetAlllinkedToUserAndQuizz(_ConnectedUser.idUser, _CurrentQuizz.idQuizz);
                    List<Response> ListResponses = await RlReponses;
                    _CurrentQuizz.ListResponse = ListResponses;

                    if (_CurrentQuizz.percentQuizz < 100) {
                        SetButton(Constantes.btnQuizzActionType.Fill);
                    } else {
                        SetButton(Constantes.btnQuizzActionType.Consult);
                    }

                    if (_CurrentQuizz != null && bCorrectionNeedRefresh ) {
                        DisplayInfoQuizz();
                        bCorrectionNeedRefresh = false;
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }


        /// <summary>
        /// Display statistics on the interface 
        /// </summary>
        private async void DisplayInfoQuizz() {
            
            Corrige.LQ = _CurrentQuizz.ListQuestion;
            Corrige.LR = _CurrentQuizz.ListResponse;

            int i = await Corrige.CalcPourcentage();
            pbRoundErrorUserInQuizz.ValueNumber = i;

            if (Corrige.LR != null) {
                
                lbInfoQcm.Text = string.Format(Constantes.AccueilView.lbQcmInfo, Corrige.PtsUserQcm, Corrige.PtsQcm);
                if (Corrige.PtsQcm > 0) {
                    double dblPercent = Convert.ToDouble(Corrige.PtsUserQcm) / Convert.ToDouble(Corrige.PtsQcm) * 100;
                    pbQcm.ValueNumber = Convert.ToInt16(dblPercent);
                } else { pbQcm.ValueNumber = 0; }
                lbInfoQcm.Refresh();

                lbInfoVraiFaux.Text = string.Format(Constantes.AccueilView.lbVraiFaux, Corrige.PtsUserVraiFaux, Corrige.PtsVraiFaux);
                if (Corrige.PtsVraiFaux > 0) {
                    double dblPercent = Convert.ToDouble(Corrige.PtsUserVraiFaux) / Convert.ToDouble(Corrige.PtsVraiFaux) * 100;
                    pbVraiFaux.ValueNumber = Convert.ToInt16(dblPercent);
                } else { pbVraiFaux.ValueNumber = 0; }
                lbInfoVraiFaux.Refresh();

                lbInfoDragDrop.Text = string.Format(Constantes.AccueilView.lbDragDropInfo, Corrige.PtsUserDragdrop, Corrige.PtsDragdrop);
                if (Corrige.PtsDragdrop > 0) {
                    double dblPercent = Convert.ToDouble(Corrige.PtsUserDragdrop) / Convert.ToDouble(Corrige.PtsDragdrop) * 100;
                    pbDragDrop.ValueNumber = Convert.ToInt16(dblPercent);
                } else { { pbDragDrop.ValueNumber = 0; } }
                lbInfoDragDrop.Refresh();
            }
            
        }


        /// <summary>
        /// Set the style of the button
        /// </summary>
        /// <param name="Action">Constantes.btnQuizzActionType</param>
        private void SetButton(string Action) {
            btnValide.Text = Action;
            switch (Action) {
                case Constantes.btnQuizzActionType.Fill:
                    btnValide.ButtonType = CxFlatUI.ButtonType.Success;
                    _Action = Action;
                    HideStats(true);
                    break;
                case Constantes.btnQuizzActionType.Consult:
                    _Action = Action;
                    btnValide.ButtonType = CxFlatUI.ButtonType.Primary;
                    HideStats(false);
                    break;
            }
        }

        /// <summary>
        /// Hide or show statistics
        /// </summary>
        /// <param name="b">Hide = true | show = false </param>
        private void HideStats(bool b) {
            b = !b;
            pbRoundErrorUserInQuizz.Visible = b;
            lbInfoDragDrop.Visible = b;
            lbInfoGeneral.Visible = b;
            lbInfoQcm.Visible = b;
            lbInfoVraiFaux.Visible = b;
            pbQcm.Visible = b;
            pbVraiFaux.Visible = b;
            pbDragDrop.Visible = b;

        }

        /// <summary>
        /// Show the next interface 
        /// FillQuizzView 
        /// or
        /// ConsutQuizzView
        /// </summary>
        private void btnValide_Click(object sender, EventArgs e) {
            tRefresh.Enabled = false;
            switch (_Action) {
                case Constantes.btnQuizzActionType.Fill:
                    FillQuizzView FQ = new FillQuizzView(_ConnectedUser,_CurrentQuizz);
                    this.Visible = false;
                    FQ.ShowDialog();
                    this.Visible = true;
                    break;
                case Constantes.btnQuizzActionType.Consult:
                    ConsultQuizzview Cqv = new ConsultQuizzview(_ConnectedUser,_CurrentQuizz);
                    this.Visible = false;
                    Cqv.ShowDialog();
                    this.Visible = true;
                    break;
            }
            tRefresh.Enabled = true;
            bCorrectionNeedRefresh = true;
        }

        private void lbxQuizz_Click(object sender, EventArgs e) {
            bCorrectionNeedRefresh = true;
        }
    }
}
