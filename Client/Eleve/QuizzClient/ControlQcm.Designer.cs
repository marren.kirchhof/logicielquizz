﻿namespace QuizzClient {
    partial class ControlQcm {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.btnInfoReponse = new CxFlatUI.CxFlatRoundButton();
            this.lbQuestionText = new CxFlatUI.CxFlatTextBox();
            this.flpResponse = new System.Windows.Forms.FlowLayoutPanel();
            this.lbInfoQcm = new CxFlatUI.Controls.CxFlatButton();
            this.SuspendLayout();
            // 
            // btnInfoReponse
            // 
            this.btnInfoReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfoReponse.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnInfoReponse.Enabled = false;
            this.btnInfoReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnInfoReponse.Location = new System.Drawing.Point(4, 90);
            this.btnInfoReponse.Name = "btnInfoReponse";
            this.btnInfoReponse.Size = new System.Drawing.Size(556, 23);
            this.btnInfoReponse.TabIndex = 25;
            this.btnInfoReponse.Text = "Liste des réponses";
            this.btnInfoReponse.TextColor = System.Drawing.Color.White;
            // 
            // lbQuestionText
            // 
            this.lbQuestionText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbQuestionText.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbQuestionText.Enabled = false;
            this.lbQuestionText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbQuestionText.Hint = "";
            this.lbQuestionText.Location = new System.Drawing.Point(0, 40);
            this.lbQuestionText.MaxLength = 32767;
            this.lbQuestionText.Multiline = true;
            this.lbQuestionText.Name = "lbQuestionText";
            this.lbQuestionText.PasswordChar = '\0';
            this.lbQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbQuestionText.SelectedText = "";
            this.lbQuestionText.SelectionLength = 0;
            this.lbQuestionText.SelectionStart = 0;
            this.lbQuestionText.Size = new System.Drawing.Size(565, 45);
            this.lbQuestionText.TabIndex = 24;
            this.lbQuestionText.TabStop = false;
            this.lbQuestionText.Text = "Question";
            this.lbQuestionText.UseSystemPasswordChar = false;
            // 
            // flpResponse
            // 
            this.flpResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpResponse.Location = new System.Drawing.Point(4, 120);
            this.flpResponse.Name = "flpResponse";
            this.flpResponse.Size = new System.Drawing.Size(556, 78);
            this.flpResponse.TabIndex = 26;
            // 
            // lbInfoQcm
            // 
            this.lbInfoQcm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoQcm.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoQcm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoQcm.Location = new System.Drawing.Point(-1, -1);
            this.lbInfoQcm.Name = "lbInfoQcm";
            this.lbInfoQcm.Size = new System.Drawing.Size(564, 44);
            this.lbInfoQcm.TabIndex = 27;
            this.lbInfoQcm.Text = "QCM";
            this.lbInfoQcm.TextColor = System.Drawing.Color.White;
            // 
            // ControlQcm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbInfoQcm);
            this.Controls.Add(this.flpResponse);
            this.Controls.Add(this.btnInfoReponse);
            this.Controls.Add(this.lbQuestionText);
            this.Name = "ControlQcm";
            this.Size = new System.Drawing.Size(563, 200);
            this.Click += new System.EventHandler(this.ControlQcm_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatRoundButton btnInfoReponse;
        public CxFlatUI.CxFlatTextBox lbQuestionText;
        private System.Windows.Forms.FlowLayoutPanel flpResponse;
        private CxFlatUI.Controls.CxFlatButton lbInfoQcm;
    }
}
