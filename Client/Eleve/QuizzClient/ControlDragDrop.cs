﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzClient {
    public partial class ControlDragDrop : UserControl {
        public ControlDragDrop() {
            InitializeComponent();
        }

        private Question Q;
        ControlDragDropQuestion CurrentQcontrol;
        ControlDragDropResponse CurrentRcontrol;




        #region DragDrop comportement


        /// <summary>
        /// Set the interface when the user must answer to this drag drop
        /// </summary>
        public Question question {
            get { return Q; }
            set {

                Q = value;
                lbQuestionText.Text = value.title;
                DragAndDrop DragDrop = Newtonsoft.Json.JsonConvert.DeserializeObject<DragAndDrop>(value.correction);

                if (DragDrop != null && DragDrop.Response != null) {

                    foreach (string s in DragDrop.WrongQuestion) {
                        CreateQuestion(s);
                    }
                    foreach (ResponseDragDrop r in DragDrop.Response) {
                        CreateResponse(r.Title);
                        foreach (string s in r.ListQuestion) {
                            CreateQuestion(s);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set the interface when the user must see the correct answer to this drag drop
        /// </summary>
        public Question CorrectedQuestion {
            get { return Q; }
            set {
                Q = value;
                lbQuestionText.Text = value.title;
                DragAndDrop DragDrop = Newtonsoft.Json.JsonConvert.DeserializeObject<DragAndDrop>(value.correction);

                if (DragDrop != null && DragDrop.Response != null) {

                    foreach (string s in DragDrop.WrongQuestion) {
                        CreateQuestion(s);
                    }
                    int i = 0;
                    foreach (ResponseDragDrop r in DragDrop.Response) {
                        CreateResponse(r.Title);
                        foreach (string sQuestion in r.ListQuestion) {
                            ControlDragDropQuestion Q = new ControlDragDropQuestion(sQuestion);
                            ((ControlDragDropResponse)flpReponse.Controls[i]).flpResponse.Controls.Add(Q);
                            Q.Width = Q.Parent.Width - Constantes.UI.iMargeFlp;
                            Q.Height = 50;
                            Q.Enabled = false;
                        }
                        i++;
                    }
                }

            }
        }

        //set the darg drop in green
        public void SetColorCorrection() {
            lbInfoDragDrop.ButtonType = CxFlatUI.ButtonType.Success;
            lbInfoColoumnQuestion.ButtonType = CxFlatUI.ButtonType.Success;
            lbInfoColumnReponse.ButtonType = CxFlatUI.ButtonType.Success;
        }




        /// <summary>
        /// Create a control question inside flpQuestion
        /// </summary>
        /// <param name="Name">Title of the question</param>
        private void CreateQuestion(string Name) {
            ControlDragDropQuestion Q = new ControlDragDropQuestion(Name);
            
            flpQuestion.Controls.Add(Q);
            Q.Width = Q.Parent.Width - 25;
            Q.pbDraggable.Height = 50;
            Q.Click += Q_Click;
            Q.tbxQuestion.Text = Name;
            Q.tbxQuestion.Enabled = false;
        }

        /// <summary>
        /// Create a control response inside flpResponse
        /// </summary>
        /// <param name="Name">Title of the response</param>
        private void CreateResponse(string Name) {
            ControlDragDropResponse R = new ControlDragDropResponse(Name);
            R.flpResponse.Click += FlpReponse_Click;
            R.Width = flpReponse.Width - Constantes.UI.iMargeFlp;
            flpReponse.Controls.Add(R);
        }

        /// <summary>
        /// Clic event when the user click on flpresponse 
        /// </summary>
        private void FlpReponse_Click(object sender, EventArgs e) {
            if (CurrentRcontrol == null && CurrentQcontrol == null) {
                CurrentRcontrol = (ControlDragDropResponse)((FlowLayoutPanel)(sender)).Parent;
            } else if (CurrentRcontrol != (ControlDragDropResponse)((FlowLayoutPanel)(sender)).Parent && CurrentQcontrol == null) {
                CurrentRcontrol = (ControlDragDropResponse)((FlowLayoutPanel)(sender)).Parent;
            } else {
                CurrentRcontrol = (ControlDragDropResponse)((FlowLayoutPanel)(sender)).Parent;
                if (CurrentQcontrol != null) {
                    CurrentQcontrol.Anchor = (AnchorStyles.Left | AnchorStyles.Right);
                    CurrentQcontrol.Width = CurrentRcontrol.Width + Constantes.UI.iMargeFlp;
                    CurrentRcontrol.flpResponse.Controls.Add(CurrentQcontrol);
                    CurrentQcontrol.Visible = true;
                    CurrentQcontrol = null;
                }
            }
        }

        /// <summary>
        /// event clic when the user clic on a question in a dragdrop
        /// </summary>
        private void Q_Click(object sender, EventArgs e) {
            if (CurrentQcontrol != null) {
                CurrentQcontrol.Visible = true;
                CurrentQcontrol.Width = CurrentQcontrol.Parent.Width - Constantes.UI.iMargeFlp;
                CurrentQcontrol = (ControlDragDropQuestion)(sender);
                CurrentQcontrol.Visible = false;

            } else if (CurrentQcontrol == (ControlDragDropQuestion)(sender)) {
                CurrentQcontrol.Width = CurrentQcontrol.Parent.Width - Constantes.UI.iMargeFlp;
                CurrentQcontrol.Visible = true;
            } else {
                CurrentQcontrol = (ControlDragDropQuestion)(sender);
                CurrentQcontrol.Visible = false;
            }
        }

        /// <summary>
        /// event clic when the user clic on flpquestion
        /// </summary>
        private void flpQuestion_Click(object sender, EventArgs e) {
            if (CurrentQcontrol != null) {
                CurrentQcontrol.Width = flpQuestion.Width - Constantes.UI.iMargeFlp;
                flpQuestion.Controls.Add(CurrentQcontrol);
                CurrentQcontrol.Visible = true;
                CurrentQcontrol = null;
            }
        }

        #endregion


        #region serialisation

        /// <summary>
        /// get the json string of fhe current drag drop
        /// </summary>
        public string ResponseJsonString { get { return GetJsonStringResponse(); } }


        /// <summary>
        /// test and return the json string of the current dragdrop
        /// </summary>
        /// <returns>Jsonstring</returns>
        private string GetJsonStringResponse() {
            DragAndDrop R = new DragAndDrop();

            R.WrongQuestion = GetAllWrongAnswer();
            R.Response = GetAllResp();

            string jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(R);

            DragAndDrop Rtest = Newtonsoft.Json.JsonConvert.DeserializeObject<DragAndDrop>(jsonstring);

            return jsonstring;
        }

        /// <summary>
        /// get all the response contained in flpQuestion
        /// </summary>
        /// <returns></returns>
        private List<string> GetAllWrongAnswer() {
            List<string> lr = new List<string>();
            foreach (ControlDragDropQuestion q in flpQuestion.Controls) {
                lr.Add(q.ValueText);
            }
            return lr;
        }

        /// <summary>
        /// Get all the question in flp question and in all response
        /// </summary>
        /// <returns></returns>
        private List<ResponseDragDrop> GetAllResp() {
            List<ResponseDragDrop> Lr = new List<ResponseDragDrop>();
            foreach (ControlDragDropResponse cR in flpReponse.Controls) {
                List<string> LQuestion = new List<string>();
                string titleRep = cR.Title;
                foreach (ControlDragDropQuestion cQ in cR.flpResponse.Controls) {
                    LQuestion.Add(cQ.ValueText);
                }
                Lr.Add(new ResponseDragDrop(titleRep, LQuestion));
            }
            return Lr;
        }

        #endregion



    }
}
