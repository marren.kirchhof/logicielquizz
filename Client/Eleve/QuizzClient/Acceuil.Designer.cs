﻿namespace QuizzClient {
    partial class AcceuilView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AcceuilView));
            this.sbAcceuil = new CxFlatUI.CxFlatStatusBar();
            this.btnLogOut = new CxFlatUI.CxFlatRoundButton();
            this.lbInfo = new CxFlatUI.Controls.CxFlatButton();
            this.tRefresh = new System.Windows.Forms.Timer(this.components);
            this.btnResetPassword = new CxFlatUI.CxFlatRoundButton();
            this.lbxQuizz = new System.Windows.Forms.ListBox();
            this.btnValide = new CxFlatUI.CxFlatSimpleButton();
            this.gbxStatistique = new CxFlatUI.CxFlatGroupBox();
            this.pbRoundErrorUserInQuizz = new CxFlatUI.CxFlatRoundProgressBar();
            this.pbQcm = new CxFlatUI.CxFlatProgressBar();
            this.lbInfoStatistiques = new CxFlatUI.Controls.CxFlatButton();
            this.lbInfoQcm = new CxFlatUI.CxFlatSimpleButton();
            this.lbInfoDragDrop = new CxFlatUI.CxFlatSimpleButton();
            this.pbDragDrop = new CxFlatUI.CxFlatProgressBar();
            this.lbInfoVraiFaux = new CxFlatUI.CxFlatSimpleButton();
            this.pbVraiFaux = new CxFlatUI.CxFlatProgressBar();
            this.lbInfoGeneral = new CxFlatUI.CxFlatSimpleButton();
            this.cxFlatButton1 = new CxFlatUI.Controls.CxFlatButton();
            this.cxFlatGroupBox2 = new CxFlatUI.CxFlatGroupBox();
            this.pbImgDisplay = new CxFlatUI.CxFlatPictureBox();
            this.gbxStatistique.SuspendLayout();
            this.cxFlatGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // sbAcceuil
            // 
            this.sbAcceuil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbAcceuil.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbAcceuil.Location = new System.Drawing.Point(0, 0);
            this.sbAcceuil.Name = "sbAcceuil";
            this.sbAcceuil.Size = new System.Drawing.Size(1071, 40);
            this.sbAcceuil.TabIndex = 1;
            this.sbAcceuil.Text = "Acceuil";
            this.sbAcceuil.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnLogOut
            // 
            this.btnLogOut.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnLogOut.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLogOut.Location = new System.Drawing.Point(12, 46);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(127, 23);
            this.btnLogOut.TabIndex = 2;
            this.btnLogOut.Text = "Déconnexion";
            this.btnLogOut.TextColor = System.Drawing.Color.White;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lbInfo
            // 
            this.lbInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfo.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfo.Enabled = false;
            this.lbInfo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfo.Location = new System.Drawing.Point(6, 68);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(290, 23);
            this.lbInfo.TabIndex = 4;
            this.lbInfo.Text = " - nom -";
            this.lbInfo.TextColor = System.Drawing.Color.White;
            // 
            // tRefresh
            // 
            this.tRefresh.Tick += new System.EventHandler(this.tRefresh_Tick);
            // 
            // btnResetPassword
            // 
            this.btnResetPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetPassword.ButtonType = CxFlatUI.ButtonType.Waring;
            this.btnResetPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnResetPassword.Location = new System.Drawing.Point(6, 97);
            this.btnResetPassword.Name = "btnResetPassword";
            this.btnResetPassword.Size = new System.Drawing.Size(290, 23);
            this.btnResetPassword.TabIndex = 5;
            this.btnResetPassword.Text = "Changer son mot de passe";
            this.btnResetPassword.TextColor = System.Drawing.Color.White;
            this.btnResetPassword.Click += new System.EventHandler(this.btnResetPassword_Click);
            // 
            // lbxQuizz
            // 
            this.lbxQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbxQuizz.BackColor = System.Drawing.Color.White;
            this.lbxQuizz.Font = new System.Drawing.Font("Noto Mono", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxQuizz.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbxQuizz.FormattingEnabled = true;
            this.lbxQuizz.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbxQuizz.IntegralHeight = false;
            this.lbxQuizz.ItemHeight = 19;
            this.lbxQuizz.Location = new System.Drawing.Point(12, 143);
            this.lbxQuizz.Name = "lbxQuizz";
            this.lbxQuizz.Size = new System.Drawing.Size(280, 365);
            this.lbxQuizz.TabIndex = 3;
            this.lbxQuizz.Click += new System.EventHandler(this.lbxQuizz_Click);
            this.lbxQuizz.SelectedIndexChanged += new System.EventHandler(this.lbxQuizz_SelectedIndexChanged);
            // 
            // btnValide
            // 
            this.btnValide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValide.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnValide.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnValide.Location = new System.Drawing.Point(769, 470);
            this.btnValide.Name = "btnValide";
            this.btnValide.Size = new System.Drawing.Size(290, 38);
            this.btnValide.TabIndex = 6;
            this.btnValide.Text = "Consulter / Remplir";
            this.btnValide.TextColor = System.Drawing.Color.White;
            this.btnValide.Click += new System.EventHandler(this.btnValide_Click);
            // 
            // gbxStatistique
            // 
            this.gbxStatistique.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxStatistique.Controls.Add(this.lbInfoGeneral);
            this.gbxStatistique.Controls.Add(this.lbInfoVraiFaux);
            this.gbxStatistique.Controls.Add(this.pbVraiFaux);
            this.gbxStatistique.Controls.Add(this.lbInfoDragDrop);
            this.gbxStatistique.Controls.Add(this.pbDragDrop);
            this.gbxStatistique.Controls.Add(this.lbInfoQcm);
            this.gbxStatistique.Controls.Add(this.lbInfoStatistiques);
            this.gbxStatistique.Controls.Add(this.pbQcm);
            this.gbxStatistique.Controls.Add(this.pbRoundErrorUserInQuizz);
            this.gbxStatistique.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxStatistique.Location = new System.Drawing.Point(298, 114);
            this.gbxStatistique.Name = "gbxStatistique";
            this.gbxStatistique.ShowText = false;
            this.gbxStatistique.Size = new System.Drawing.Size(465, 394);
            this.gbxStatistique.TabIndex = 7;
            this.gbxStatistique.TabStop = false;
            this.gbxStatistique.Text = "gbxStatistique";
            this.gbxStatistique.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // pbRoundErrorUserInQuizz
            // 
            this.pbRoundErrorUserInQuizz.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pbRoundErrorUserInQuizz.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.pbRoundErrorUserInQuizz.IsError = false;
            this.pbRoundErrorUserInQuizz.Location = new System.Drawing.Point(175, 279);
            this.pbRoundErrorUserInQuizz.Name = "pbRoundErrorUserInQuizz";
            this.pbRoundErrorUserInQuizz.Size = new System.Drawing.Size(109, 109);
            this.pbRoundErrorUserInQuizz.TabIndex = 0;
            this.pbRoundErrorUserInQuizz.ValueNumber = 0;
            // 
            // pbQcm
            // 
            this.pbQcm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbQcm.BackColor = System.Drawing.SystemColors.Control;
            this.pbQcm.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pbQcm.IsError = false;
            this.pbQcm.Location = new System.Drawing.Point(6, 58);
            this.pbQcm.Name = "pbQcm";
            this.pbQcm.ProgressBarStyle = CxFlatUI.CxFlatProgressBar.Style.ValueInSide;
            this.pbQcm.Size = new System.Drawing.Size(453, 14);
            this.pbQcm.TabIndex = 1;
            this.pbQcm.Text = "cxFlatProgressBar1";
            this.pbQcm.ValueNumber = 0;
            // 
            // lbInfoStatistiques
            // 
            this.lbInfoStatistiques.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoStatistiques.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoStatistiques.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoStatistiques.Location = new System.Drawing.Point(6, 0);
            this.lbInfoStatistiques.Name = "lbInfoStatistiques";
            this.lbInfoStatistiques.Size = new System.Drawing.Size(453, 23);
            this.lbInfoStatistiques.TabIndex = 2;
            this.lbInfoStatistiques.Text = "Statistiques du quizz";
            this.lbInfoStatistiques.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoQcm
            // 
            this.lbInfoQcm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoQcm.ButtonType = CxFlatUI.ButtonType.Default;
            this.lbInfoQcm.Enabled = false;
            this.lbInfoQcm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoQcm.Location = new System.Drawing.Point(6, 29);
            this.lbInfoQcm.Name = "lbInfoQcm";
            this.lbInfoQcm.Size = new System.Drawing.Size(453, 23);
            this.lbInfoQcm.TabIndex = 3;
            this.lbInfoQcm.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoDragDrop
            // 
            this.lbInfoDragDrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoDragDrop.ButtonType = CxFlatUI.ButtonType.Default;
            this.lbInfoDragDrop.Enabled = false;
            this.lbInfoDragDrop.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoDragDrop.Location = new System.Drawing.Point(6, 96);
            this.lbInfoDragDrop.Name = "lbInfoDragDrop";
            this.lbInfoDragDrop.Size = new System.Drawing.Size(453, 23);
            this.lbInfoDragDrop.TabIndex = 5;
            this.lbInfoDragDrop.TextColor = System.Drawing.Color.White;
            // 
            // pbDragDrop
            // 
            this.pbDragDrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbDragDrop.BackColor = System.Drawing.SystemColors.Control;
            this.pbDragDrop.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pbDragDrop.IsError = false;
            this.pbDragDrop.Location = new System.Drawing.Point(6, 125);
            this.pbDragDrop.Name = "pbDragDrop";
            this.pbDragDrop.ProgressBarStyle = CxFlatUI.CxFlatProgressBar.Style.ValueInSide;
            this.pbDragDrop.Size = new System.Drawing.Size(453, 14);
            this.pbDragDrop.TabIndex = 4;
            this.pbDragDrop.Text = "cxFlatProgressBar1";
            this.pbDragDrop.ValueNumber = 0;
            // 
            // lbInfoVraiFaux
            // 
            this.lbInfoVraiFaux.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoVraiFaux.ButtonType = CxFlatUI.ButtonType.Default;
            this.lbInfoVraiFaux.Enabled = false;
            this.lbInfoVraiFaux.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoVraiFaux.Location = new System.Drawing.Point(6, 161);
            this.lbInfoVraiFaux.Name = "lbInfoVraiFaux";
            this.lbInfoVraiFaux.Size = new System.Drawing.Size(453, 23);
            this.lbInfoVraiFaux.TabIndex = 7;
            this.lbInfoVraiFaux.TextColor = System.Drawing.Color.White;
            // 
            // pbVraiFaux
            // 
            this.pbVraiFaux.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbVraiFaux.BackColor = System.Drawing.SystemColors.Control;
            this.pbVraiFaux.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pbVraiFaux.IsError = false;
            this.pbVraiFaux.Location = new System.Drawing.Point(6, 190);
            this.pbVraiFaux.Name = "pbVraiFaux";
            this.pbVraiFaux.ProgressBarStyle = CxFlatUI.CxFlatProgressBar.Style.ValueInSide;
            this.pbVraiFaux.Size = new System.Drawing.Size(453, 14);
            this.pbVraiFaux.TabIndex = 6;
            this.pbVraiFaux.Text = "cxFlatProgressBar1";
            this.pbVraiFaux.ValueNumber = 0;
            // 
            // lbInfoGeneral
            // 
            this.lbInfoGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoGeneral.ButtonType = CxFlatUI.ButtonType.Default;
            this.lbInfoGeneral.Enabled = false;
            this.lbInfoGeneral.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoGeneral.Location = new System.Drawing.Point(6, 237);
            this.lbInfoGeneral.Name = "lbInfoGeneral";
            this.lbInfoGeneral.Size = new System.Drawing.Size(453, 36);
            this.lbInfoGeneral.TabIndex = 8;
            this.lbInfoGeneral.Text = "Taux de réussite générale";
            this.lbInfoGeneral.TextColor = System.Drawing.Color.White;
            // 
            // cxFlatButton1
            // 
            this.cxFlatButton1.ButtonType = CxFlatUI.ButtonType.Primary;
            this.cxFlatButton1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cxFlatButton1.Location = new System.Drawing.Point(12, 114);
            this.cxFlatButton1.Name = "cxFlatButton1";
            this.cxFlatButton1.Size = new System.Drawing.Size(280, 23);
            this.cxFlatButton1.TabIndex = 8;
            this.cxFlatButton1.Text = "Listes des quizz";
            this.cxFlatButton1.TextColor = System.Drawing.Color.White;
            // 
            // cxFlatGroupBox2
            // 
            this.cxFlatGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cxFlatGroupBox2.Controls.Add(this.pbImgDisplay);
            this.cxFlatGroupBox2.Controls.Add(this.btnResetPassword);
            this.cxFlatGroupBox2.Controls.Add(this.lbInfo);
            this.cxFlatGroupBox2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cxFlatGroupBox2.Location = new System.Drawing.Point(761, 46);
            this.cxFlatGroupBox2.Name = "cxFlatGroupBox2";
            this.cxFlatGroupBox2.ShowText = false;
            this.cxFlatGroupBox2.Size = new System.Drawing.Size(302, 130);
            this.cxFlatGroupBox2.TabIndex = 9;
            this.cxFlatGroupBox2.TabStop = false;
            this.cxFlatGroupBox2.Text = "cxFlatGroupBox2";
            this.cxFlatGroupBox2.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // pbImgDisplay
            // 
            this.pbImgDisplay.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbImgDisplay.Image = global::QuizzClient.Properties.Resources.login;
            this.pbImgDisplay.Location = new System.Drawing.Point(104, 12);
            this.pbImgDisplay.Name = "pbImgDisplay";
            this.pbImgDisplay.Size = new System.Drawing.Size(100, 50);
            this.pbImgDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImgDisplay.TabIndex = 6;
            this.pbImgDisplay.TabStop = false;
            // 
            // AcceuilView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 529);
            this.Controls.Add(this.cxFlatGroupBox2);
            this.Controls.Add(this.cxFlatButton1);
            this.Controls.Add(this.gbxStatistique);
            this.Controls.Add(this.btnValide);
            this.Controls.Add(this.lbxQuizz);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.sbAcceuil);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(1071, 529);
            this.Name = "AcceuilView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Acceuil";
            this.gbxStatistique.ResumeLayout(false);
            this.cxFlatGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImgDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbAcceuil;
        private CxFlatUI.CxFlatRoundButton btnLogOut;
        private CxFlatUI.Controls.CxFlatButton lbInfo;
        private System.Windows.Forms.Timer tRefresh;
        private CxFlatUI.CxFlatRoundButton btnResetPassword;
        private System.Windows.Forms.ListBox lbxQuizz;
        private CxFlatUI.CxFlatSimpleButton btnValide;
        private CxFlatUI.CxFlatGroupBox gbxStatistique;
        public CxFlatUI.CxFlatRoundProgressBar pbRoundErrorUserInQuizz;
        private CxFlatUI.Controls.CxFlatButton lbInfoStatistiques;
        private CxFlatUI.CxFlatProgressBar pbQcm;
        private CxFlatUI.CxFlatSimpleButton lbInfoQcm;
        private CxFlatUI.CxFlatSimpleButton lbInfoDragDrop;
        private CxFlatUI.CxFlatProgressBar pbDragDrop;
        private CxFlatUI.CxFlatSimpleButton lbInfoVraiFaux;
        private CxFlatUI.CxFlatProgressBar pbVraiFaux;
        private CxFlatUI.CxFlatSimpleButton lbInfoGeneral;
        private CxFlatUI.Controls.CxFlatButton cxFlatButton1;
        private CxFlatUI.CxFlatGroupBox cxFlatGroupBox2;
        private CxFlatUI.CxFlatPictureBox pbImgDisplay;
    }
}