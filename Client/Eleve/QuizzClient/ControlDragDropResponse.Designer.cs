﻿namespace QuizzClient {
    partial class ControlDragDropResponse {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.flpQuestionCorrect = new System.Windows.Forms.FlowLayoutPanel();
            this.tbxReponse = new CxFlatUI.Controls.CxFlatButton();
            this.SuspendLayout();
            // 
            // flpQuestionCorrect
            // 
            this.flpQuestionCorrect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpQuestionCorrect.AutoScroll = true;
            this.flpQuestionCorrect.BackColor = System.Drawing.Color.White;
            this.flpQuestionCorrect.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuestionCorrect.Location = new System.Drawing.Point(3, 41);
            this.flpQuestionCorrect.Name = "flpQuestionCorrect";
            this.flpQuestionCorrect.Padding = new System.Windows.Forms.Padding(1);
            this.flpQuestionCorrect.Size = new System.Drawing.Size(428, 116);
            this.flpQuestionCorrect.TabIndex = 2;
            // 
            // tbxReponse
            // 
            this.tbxReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxReponse.ButtonType = CxFlatUI.ButtonType.Primary;
            this.tbxReponse.Enabled = false;
            this.tbxReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxReponse.Location = new System.Drawing.Point(-3, -1);
            this.tbxReponse.Name = "tbxReponse";
            this.tbxReponse.Size = new System.Drawing.Size(438, 36);
            this.tbxReponse.TabIndex = 3;
            this.tbxReponse.Text = "Reponse";
            this.tbxReponse.TextColor = System.Drawing.Color.White;
            // 
            // ControlDragDropResponse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tbxReponse);
            this.Controls.Add(this.flpQuestionCorrect);
            this.Name = "ControlDragDropResponse";
            this.Size = new System.Drawing.Size(434, 160);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flpQuestionCorrect;
        public CxFlatUI.Controls.CxFlatButton tbxReponse;
    }
}
