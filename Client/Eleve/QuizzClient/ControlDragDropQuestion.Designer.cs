﻿namespace QuizzClient {
    partial class ControlDragDropQuestion {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.pbDraggable = new CxFlatUI.CxFlatPictureBox();
            this.tbxQuestion = new CxFlatUI.CxFlatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDraggable)).BeginInit();
            this.SuspendLayout();
            // 
            // pbDraggable
            // 
            this.pbDraggable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pbDraggable.Image = global::QuizzClient.Properties.Resources.grid__1_;
            this.pbDraggable.Location = new System.Drawing.Point(0, 0);
            this.pbDraggable.Name = "pbDraggable";
            this.pbDraggable.Size = new System.Drawing.Size(25, 33);
            this.pbDraggable.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDraggable.TabIndex = 3;
            this.pbDraggable.TabStop = false;
            // 
            // tbxQuestion
            // 
            this.tbxQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxQuestion.Enabled = false;
            this.tbxQuestion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxQuestion.Hint = "Question";
            this.tbxQuestion.Location = new System.Drawing.Point(31, 0);
            this.tbxQuestion.MaxLength = 32767;
            this.tbxQuestion.MinimumSize = new System.Drawing.Size(100, 27);
            this.tbxQuestion.Multiline = true;
            this.tbxQuestion.Name = "tbxQuestion";
            this.tbxQuestion.PasswordChar = '\0';
            this.tbxQuestion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxQuestion.SelectedText = "";
            this.tbxQuestion.SelectionLength = 0;
            this.tbxQuestion.SelectionStart = 0;
            this.tbxQuestion.Size = new System.Drawing.Size(359, 33);
            this.tbxQuestion.TabIndex = 2;
            this.tbxQuestion.TabStop = false;
            this.tbxQuestion.UseSystemPasswordChar = false;
            // 
            // ControlDragDropQuestion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.pbDraggable);
            this.Controls.Add(this.tbxQuestion);
            this.Name = "ControlDragDropQuestion";
            this.Size = new System.Drawing.Size(391, 33);
            ((System.ComponentModel.ISupportInitialize)(this.pbDraggable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public CxFlatUI.CxFlatTextBox tbxQuestion;
        public CxFlatUI.CxFlatPictureBox pbDraggable;
    }
}
