﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzClient {
    public partial class ConsultQuizzview : Form {


        private User _CurrentUser;
        private Quizz _CurrentQuizz;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cUser">Current User</param>
        /// <param name="cQuizz">Current Quizz</param>
        public ConsultQuizzview(User cUser,Quizz cQuizz) {
            _CurrentQuizz = cQuizz;
            _CurrentUser = cUser;
            InitializeComponent();
            load();
        }





        private void load() {
            FillQuizz();
        }


        /// <summary>
        /// Fill the interface with the current quizz
        /// </summary>
        private void FillQuizz() {
            if (_CurrentQuizz != null && _CurrentQuizz.ListQuestion != null) {
                lbInfoQuizz.Text = _CurrentQuizz.name;
                foreach (Question q in _CurrentQuizz.ListQuestion) {
                    switch (q.idType) {
                        case 1:
                            flpQuizzCorrection.Controls.Add(NewQuestionDragDrop(q));
                            Response RDdToAdd = _CurrentQuizz.ListResponse.Find(x => x.idQuestion == q.idQuestion);
                            flpQuizzUser.Controls.Add(NewQuestionDragDrop(RDdToAdd, q.title, q.idType));
                            break;
                        case 2:
                            flpQuizzCorrection.Controls.Add(NewQuestionVraiFaux(q));
                            Response RVfToAdd = _CurrentQuizz.ListResponse.Find(x => x.idQuestion == q.idQuestion);
                            flpQuizzUser.Controls.Add(NewQuestionVraiFaux(RVfToAdd, q.title, q.idType));

                            break;
                        case 3:
                            flpQuizzCorrection.Controls.Add(NewQuestionQCM(q));
                            Response RQcmToAdd = _CurrentQuizz.ListResponse.Find(x => x.idQuestion == q.idQuestion);
                            flpQuizzUser.Controls.Add(NewQuestionQCM(RQcmToAdd, q.title, q.idType));

                            break;
                    }
                }
            }
        }




        #region Create Question

        #region Correction
        // return a control according to the type of the question for the User interface 

        private ControlQcm NewQuestionQCM(Question Q) {
            ControlQcm cQcm = new ControlQcm();
            cQcm.CorrectedQuestion = Q;
            cQcm.SetColorCorrection();
            return cQcm;
        }
        private ControlDragDrop NewQuestionDragDrop(Question Q) {
            ControlDragDrop cDragDrop = new ControlDragDrop();
            cDragDrop.CorrectedQuestion = Q;
            cDragDrop.SetColorCorrection();
            return cDragDrop;
        }
        private ControlVraiFaux NewQuestionVraiFaux(Question Q) {
            ControlVraiFaux cVraiFaux = new ControlVraiFaux();
            cVraiFaux.Width = flpQuizzUser.Width;
            cVraiFaux.CorrectedQuestion = Q;
            cVraiFaux.SetColorCorrection();
            return cVraiFaux;
        }
        #endregion



        //Create new question for the interface "correction"
        #region user responses
        private ControlQcm NewQuestionQCM(Response R,string QuestionTitle,int idType) {
            ControlQcm cQcm = new ControlQcm();
            Question tempQCorrection = new Question(R.idQuestion, QuestionTitle, R.idQuizz, idType, R.respUser);
            cQcm.CorrectedQuestion = tempQCorrection;
            return cQcm;
        }
        private ControlDragDrop NewQuestionDragDrop(Response R,string QuestionTitle, int idType) {
            ControlDragDrop cDragDrop = new ControlDragDrop();
            Question tempQCorrection = new Question(R.idQuestion, QuestionTitle, R.idQuizz, idType, R.respUser);

            cDragDrop.Width = flpQuizzUser.Width;
            cDragDrop.CorrectedQuestion = tempQCorrection;
            return cDragDrop;
        }
        private ControlVraiFaux NewQuestionVraiFaux(Response R,string QuestionTitle, int idType) {
            ControlVraiFaux cVraiFaux = new ControlVraiFaux();
            Question tempQCorrection = new Question(R.idQuestion, QuestionTitle, R.idQuizz, idType, R.respUser);

            cVraiFaux.Width = flpQuizzUser.Width;
            cVraiFaux.CorrectedQuestion = tempQCorrection;
            return cVraiFaux;
        }





        #endregion

        #endregion

        private void btnback_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
