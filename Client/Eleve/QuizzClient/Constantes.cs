﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzClient {
    public static class Constantes {

        public static class Global {
            public const string DateFormatClient = "dd.mm.yyyy";
            public const string DateFormatBDD = "yyyy-MM-dd";
            public const bool DefaultBoolFalue = false;
            public const string EmptyJson = "[]";
            public const string DefaultNameDragDrop = "Legende";
            public const int LevelAdmin = 1;
            public const string SuccessCode = "1";
        }

        public static class Correction {
            public const int PointsTotVraiFaux = 1;
            public const int PointsWrongAnswerVraiFaux = 0;

        }

        public class Refresh {
            public const bool Enabled = true;
            public const int Interval = 4000;
        }

        public static class PasswordPolitics {
            public const int minNumber = 1;
            public const int minChar = 7;
            public const int Uppercase = 1;
        }

        public static class UI {
            public const int cGrip = 16;      //Border resize : Grip size
            public const int cCaption = 40;   //Border resize: Caption height
            public const int iMargeFlp = 25;
            public const int widthFlpQuizz = 250;
        }

        public static class AccueilView {
            public const string lbQcmInfo = "Vous avez obtenu {0} pts sur {1} sur les questions Qcm. ";
            public const string lbDragDropInfo = "Vous avez obtenu {0} pts sur {1} sur les questions drag and drop. ";
            public const string lbVraiFaux = "Vous avez obtenu {0} pts sur {1} sur les questions vrai ou fausse. ";
        }

        public static class ColumnName {
            public static class User {
                public const string username = "username";
                public const string password = "password";
                public const string idRole = "idRole";
            }

        }

        public static class DisplayListbox {
            public const string ListboxUsers = "username";
            public const string ListboxQuizzes = "name";
            public const string ListboxQuestion = "title";
            public const string ComboboxType = "name";
            public const string clxRole = "name";
        }

        public static class URL {
            public const string ServerAddresse = "http://localhost:8000";
            public const string ApiAddresseRoot = "/api";
            public const string ApiVersion = "/v1";
            public const string ServerApiFullPath = ServerAddresse + ApiAddresseRoot + ApiVersion;

            public static class RequestHTTP {

                public static class Types {
                    public const string ApiTypeAddresse = ServerApiFullPath + "/types";
                    public const string GetAllType = ApiTypeAddresse + "/";
                    public const string GetOne = ApiTypeAddresse + "/{0}";
                }

                public static class Users {
                    public const string ApiAddresse = ServerApiFullPath + "/users";
                    public const string GetAll = ApiAddresse + "/";
                    public const string GetOne = ApiAddresse + "/{0}";
                    public const string Create = ApiAddresse + "?username={0}&password={1}&idRole={2}";
                    public const string Update = ApiAddresse + "/{0}?username={1}&password={2}&idRole={3}";
                    public const string Delete = ApiAddresse + "/{0}";
                    public const string Find = ApiAddresse + "/find/{0}";
                    public class UserQuizzes {
                        public const string BasePathQuizzes = ApiAddresse + "/{0}/quizzes";
                        public const string GetAll = BasePathQuizzes + "/";
                        public const string PutAttach = BasePathQuizzes + "/{1}";
                        public const string DeleteDetach = BasePathQuizzes + "/{1}";
                        public static class responses {
                            public const string BasePathResponse = BasePathQuizzes + "/responses";
                            public const string GetAll = "/";
                        }
                    }
                    public class UserQuizzesOwned {
                        public const string BasePathQuizzes = ApiAddresse + "/{0}/ownedquizzes";
                        public const string GetAll = BasePathQuizzes + "/";
                    }
                }


                public static class Quizzes {
                    public const string ApiAddresse = ServerApiFullPath + "/quizzes";
                    public const string GetAll = ApiAddresse + "/";
                    public const string GetOne = ApiAddresse + "/{0}";
                    public const string Create = ApiAddresse + "?name={0}&idUser={1}&date={2}";
                    public const string Update = ApiAddresse + "/{0}?name={1}&idUser={2}&date={3}";
                    public const string Delete = ApiAddresse + "/{0}";

                    public static class UsersQuizz {
                        public const string BasePathUsersQuizz = GetOne + "/users";
                        public const string GetAll = BasePathUsersQuizz + "/";
                    }

                    public static class QuizzesQuestions {
                        public const string BasePathQuestion = GetOne + "/questions";
                        //create question
                        public const string GetAll = BasePathQuestion + "/";
                        public const string PutAttach = BasePathQuestion + "/{1}?title={2}&idQuizz={3}&idType={4}&correction={5}";
                        public const string DeleteDetach = BasePathQuestion + "/{1}";
                    }

                }
                public static class Questions {
                    public const string ApiAddresse = ServerApiFullPath + "/questions";
                    public const string GetAll = ApiAddresse + "/";
                    public const string GetOne = ApiAddresse + "/{0}";
                    public const string Create = ApiAddresse + "?title={0}&correction={1}&idType={2}&idQuizz={3}";
                    public const string Update = ApiAddresse + "/{0}?title={1}&correction={2}&idType={3}&idQuizz={4}";
                    public const string Delete = ApiAddresse + "/{0}";
                }

                public static class responses {
                    public const string ApiAddresse = ServerApiFullPath + "/responses";
                    public const string GetAll = ApiAddresse + "/";
                    public const string GetOne = ApiAddresse + "/{0}";
                    public const string Create = ApiAddresse + "?respuser={0}&idQuizz={1}&idUser={2}&idQuestion={3}";
                    public const string Update = ApiAddresse + "/{0}?respuser={0}&idQuizz={1}&idUser={2}&idQuestion={3}";
                    public const string Delete = ApiAddresse + "/{0}";



                    public const string GetAllFromUserAndQuizz = ServerApiFullPath + "/users/{0}/quizzes/{1}/responses";
                }


                public static class Roles {
                    public const string ApiAddresse = ServerApiFullPath + "/roles";
                    public const string GetAll = ApiAddresse + "/";
                }
            }

        }





        public static class ErrorMessage {
            public const string ErrorMessageException = "Une erreur est survenue :\r\n{0}";
            public static class ViewUser {
                public const string ErrorMessageTypeViewUser = "Attention ! Une erreur est survenue, le paramètre passé à la classe ViewUser est incorrecte ! ";
            }
            public static class Accueil {
                public const string ErrorMessageDelCurrentuser = "Vous ne pouvez pas supprimer le compte sur lequel vous êtes connecté!";
            }
            public class HttpRequest {
                public const string Errorwhileupdate = "Vous ne pouvez pas supprimer le compte sur lequel vous êtes connecté!";
            }

        }

        public static class InfoMessage {
            public const string OperationSuccesss = "L'opération s'est terminée avec succès !";
            public static class ViewLogin {
                public const string WrongCredentials = "Le nom d'utilisateur ou le mot de passe est incorrecte !";
            }
            public static class ViewAccueil {
                public const string ConfirmationDeleteUser = "Voulez - vous vraiment supprimer l'utilisateur : {0} ?";
                public const string ConfirmationDeleteQuizz = "Voulez - vous vraiment supprimer le quizz : {0} ?";
            }

            public static class Quizz {
                public const string ConfirmationAddQuizz = "Le quizz doit d'abord être sauvegardé, voulez vous continuer ? ";
                public const string ConfirmationDelQuestion = "Voulez - vous vraiment supprimer le quizz : {0}? ";
            }
        }

        public static class AlertMessage {
            public const string EmptyFields = "Tous les champs ne sont pas remplit !";
            public static class ViewUser {
                public const string PassworddidntMatch = "Les mot de passe ne correspondent pas !";
                public const string UserAlreadyExist = "L'utilisateur existe déjà !";
            }
            public static class Login {
                public const string LoginFail = "Le mot de passe ou le nom d'utilisatuer est incorrecte !";
            }

            public static class Password {
                public static string PasswordPoliticsIncorrect = string.Format("Le mot de passe n'est pas conforme :\r\n Nombre de caractères minimum :{0}\r\nNombre de chiffre :{1}\r\nNombre de majuscule : {2}", PasswordPolitics.minChar,PasswordPolitics.minNumber,PasswordPolitics.Uppercase);
            }


        }

        public static class TitleForm {
            public const string ViewUserAdd = "Nouvel utilisateur";
            public const string ViewUserEdit = "Modifier un utilisateur";
            public const string ViewMain = "Acceuil";
            public const string ViewLogin = "Login";
            public const string ViewQuizzAdd = "Nouveau quizz";
            public const string ViewQuizzEdit = "Modifier un quizz";
            public const string ViewQuestionAdd = "Nouvelle question";
            public const string ViewQuestionEdit = "Modifier une question";
            public const string ViewDragDropAdd = "Nouveau drag and drop";
            public const string ViewDragDropEdit = "Modifier un drag and drop";
            public const string ViewQCMAdd = "Nouveau QCM";
            public const string ViewQCMEdit = "Modifier un QCM";
            public const string ViewTrueFalseAdd = "ajouter un vrai faux";
            public const string ViewTrueFalseEdit = "Modifier un vrai faux";

        }

        public static class btnTextType {
            public const string Add = "Ajouter";
            public const string Edit = "Editer";
        }

        public static class btnQuizzActionType {
            public const string Consult = "Consulter";
            public const string Fill = "Remplir";
        }

        public static class TypeAction {
            public const string Add = "Ajouter";
            public const string Edit = "Editer";
           
        }

        public static class QuestionsType {
            public const string Vraifaux = "VraiFaux";
            public const string DragDrop = "DragDrop";
            public const string Qcm = "QCM";
        }

        public static class FormatTab {
            public static class TrueFalse {
                public const char TrueFalseSeparator = ';';
                public static string TrueFalseNom = "{0}" + TrueFalseSeparator + "{1}";

            }
        }


    }
}
