﻿namespace QuizzClient {
    partial class ConsultQuizzview {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsultQuizzview));
            this.sbFillView = new CxFlatUI.CxFlatStatusBar();
            this.tlpGlobal = new System.Windows.Forms.TableLayoutPanel();
            this.lbInfoResponse = new CxFlatUI.Controls.CxFlatButton();
            this.gbxMain = new CxFlatUI.CxFlatGroupBox();
            this.flpQuizzUser = new System.Windows.Forms.FlowLayoutPanel();
            this.cxFlatGroupBox1 = new CxFlatUI.CxFlatGroupBox();
            this.flpQuizzCorrection = new System.Windows.Forms.FlowLayoutPanel();
            this.lbInfoCorrection = new CxFlatUI.Controls.CxFlatButton();
            this.lbInfoQuizz = new CxFlatUI.Controls.CxFlatButton();
            this.btnback = new CxFlatUI.CxFlatRoundButton();
            this.tlpGlobal.SuspendLayout();
            this.gbxMain.SuspendLayout();
            this.cxFlatGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbFillView
            // 
            this.sbFillView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbFillView.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbFillView.Location = new System.Drawing.Point(0, 0);
            this.sbFillView.Name = "sbFillView";
            this.sbFillView.Size = new System.Drawing.Size(1261, 40);
            this.sbFillView.TabIndex = 3;
            this.sbFillView.Text = "Consultation du quizz";
            this.sbFillView.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // tlpGlobal
            // 
            this.tlpGlobal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpGlobal.ColumnCount = 2;
            this.tlpGlobal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpGlobal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpGlobal.Controls.Add(this.lbInfoResponse, 0, 0);
            this.tlpGlobal.Controls.Add(this.gbxMain, 0, 1);
            this.tlpGlobal.Controls.Add(this.cxFlatGroupBox1, 1, 1);
            this.tlpGlobal.Controls.Add(this.lbInfoCorrection, 1, 0);
            this.tlpGlobal.Location = new System.Drawing.Point(12, 80);
            this.tlpGlobal.Name = "tlpGlobal";
            this.tlpGlobal.RowCount = 3;
            this.tlpGlobal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.274648F));
            this.tlpGlobal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.72535F));
            this.tlpGlobal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tlpGlobal.Size = new System.Drawing.Size(1237, 548);
            this.tlpGlobal.TabIndex = 4;
            // 
            // lbInfoResponse
            // 
            this.lbInfoResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoResponse.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoResponse.Enabled = false;
            this.lbInfoResponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoResponse.Location = new System.Drawing.Point(3, 3);
            this.lbInfoResponse.Name = "lbInfoResponse";
            this.lbInfoResponse.Size = new System.Drawing.Size(612, 33);
            this.lbInfoResponse.TabIndex = 8;
            this.lbInfoResponse.Text = "Réponse";
            this.lbInfoResponse.TextColor = System.Drawing.Color.White;
            // 
            // gbxMain
            // 
            this.gbxMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxMain.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxMain.Controls.Add(this.flpQuizzUser);
            this.gbxMain.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxMain.Location = new System.Drawing.Point(3, 42);
            this.gbxMain.Name = "gbxMain";
            this.gbxMain.ShowText = false;
            this.gbxMain.Size = new System.Drawing.Size(612, 429);
            this.gbxMain.TabIndex = 5;
            this.gbxMain.TabStop = false;
            this.gbxMain.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // flpQuizzUser
            // 
            this.flpQuizzUser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpQuizzUser.AutoScroll = true;
            this.flpQuizzUser.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuizzUser.Location = new System.Drawing.Point(25, 28);
            this.flpQuizzUser.Name = "flpQuizzUser";
            this.flpQuizzUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flpQuizzUser.Size = new System.Drawing.Size(563, 385);
            this.flpQuizzUser.TabIndex = 1;
            this.flpQuizzUser.WrapContents = false;
            // 
            // cxFlatGroupBox1
            // 
            this.cxFlatGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cxFlatGroupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cxFlatGroupBox1.Controls.Add(this.flpQuizzCorrection);
            this.cxFlatGroupBox1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cxFlatGroupBox1.Location = new System.Drawing.Point(621, 42);
            this.cxFlatGroupBox1.Name = "cxFlatGroupBox1";
            this.cxFlatGroupBox1.ShowText = false;
            this.cxFlatGroupBox1.Size = new System.Drawing.Size(613, 429);
            this.cxFlatGroupBox1.TabIndex = 6;
            this.cxFlatGroupBox1.TabStop = false;
            this.cxFlatGroupBox1.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // flpQuizzCorrection
            // 
            this.flpQuizzCorrection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpQuizzCorrection.AutoScroll = true;
            this.flpQuizzCorrection.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuizzCorrection.Location = new System.Drawing.Point(24, 28);
            this.flpQuizzCorrection.Name = "flpQuizzCorrection";
            this.flpQuizzCorrection.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flpQuizzCorrection.Size = new System.Drawing.Size(563, 385);
            this.flpQuizzCorrection.TabIndex = 0;
            this.flpQuizzCorrection.WrapContents = false;
            // 
            // lbInfoCorrection
            // 
            this.lbInfoCorrection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoCorrection.ButtonType = CxFlatUI.ButtonType.Success;
            this.lbInfoCorrection.Enabled = false;
            this.lbInfoCorrection.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoCorrection.Location = new System.Drawing.Point(621, 3);
            this.lbInfoCorrection.Name = "lbInfoCorrection";
            this.lbInfoCorrection.Size = new System.Drawing.Size(613, 33);
            this.lbInfoCorrection.TabIndex = 7;
            this.lbInfoCorrection.Text = "Correction";
            this.lbInfoCorrection.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoQuizz
            // 
            this.lbInfoQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoQuizz.ButtonType = CxFlatUI.ButtonType.Waring;
            this.lbInfoQuizz.Enabled = false;
            this.lbInfoQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoQuizz.Location = new System.Drawing.Point(371, 51);
            this.lbInfoQuizz.Name = "lbInfoQuizz";
            this.lbInfoQuizz.Size = new System.Drawing.Size(519, 23);
            this.lbInfoQuizz.TabIndex = 5;
            this.lbInfoQuizz.Text = "Quizz";
            this.lbInfoQuizz.TextColor = System.Drawing.Color.White;
            // 
            // btnback
            // 
            this.btnback.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnback.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnback.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnback.Location = new System.Drawing.Point(342, 634);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(583, 36);
            this.btnback.TabIndex = 6;
            this.btnback.Text = "Fermer";
            this.btnback.TextColor = System.Drawing.Color.White;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // ConsultQuizzview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 682);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.lbInfoQuizz);
            this.Controls.Add(this.tlpGlobal);
            this.Controls.Add(this.sbFillView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.Name = "ConsultQuizzview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultQuizzview";
            this.tlpGlobal.ResumeLayout(false);
            this.gbxMain.ResumeLayout(false);
            this.cxFlatGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbFillView;
        private System.Windows.Forms.TableLayoutPanel tlpGlobal;
        private CxFlatUI.CxFlatGroupBox gbxMain;
        private CxFlatUI.CxFlatGroupBox cxFlatGroupBox1;
        private System.Windows.Forms.FlowLayoutPanel flpQuizzCorrection;
        private CxFlatUI.Controls.CxFlatButton lbInfoCorrection;
        private CxFlatUI.Controls.CxFlatButton lbInfoResponse;
        private CxFlatUI.Controls.CxFlatButton lbInfoQuizz;
        private CxFlatUI.CxFlatRoundButton btnback;
        private System.Windows.Forms.FlowLayoutPanel flpQuizzUser;
    }
}