﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzClient {
    public partial class Login : Form {
        public Login() {
            InitializeComponent();
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e) {
            CheckIfuserExist(tbxuserName.Text, Crypt.hashPassword(tbxPassword.Text));
        }

        //database action

        /// <summary>
        /// Check if the user exist and show the next form if hte password match
        /// </summary>
        /// <param name="Username">Username</param>
        /// <param name="Sha256Password">Password hashed in sha 256 in the class Crypt.cs</param>
        private async void CheckIfuserExist(string Username, string Sha256Password) {
            try {
                Task<User> Rexist = HttpRequest.UsersRequest.Find(Username);
                User UResultatExist = await Rexist;

                if (UResultatExist != null && UResultatExist.username != "" && UResultatExist.password == Sha256Password) {
                    Task<List<Quizz>> lUser = HttpRequest.QuizzesRequest.GetQuizzesWhoUserIsOwner(UResultatExist.idUser);
                    User FinalUser = UResultatExist;
                    List<Quizz> ltemp = await lUser;

                    FinalUser.quizzes_owned = ltemp;

                    AcceuilView A = new AcceuilView(FinalUser);
                    this.Visible = false;
                    A.ShowDialog();
                    this.Visible = true;
                    tbxPassword.Text = "";
                } else {
                    MessageBox.Show(Constantes.AlertMessage.Login.LoginFail, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Event when the user press enter when the focus is in tbxPassword
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                btnLogin_Click(null,null);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
    }
}
