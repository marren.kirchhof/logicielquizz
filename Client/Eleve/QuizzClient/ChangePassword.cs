﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QuizzClient.Constantes.UI;

using System.Windows.Forms;

namespace QuizzClient {
    public partial class ChangePassword : Form {

        private User Currentuser;

        public ChangePassword(User SelectedUser) {
            init();
            this.Currentuser = SelectedUser;
        }

        private void btnAction_Click(object sender, EventArgs e) {
            try {
                if (CheckPassword()) {
                    UpdateUser(Currentuser.idUser, Currentuser.username, Crypt.hashPassword(tbxPassword.Text), Currentuser.idRole);
                    this.Close();
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private async void UpdateUser(int id, string name, string CryptedPassword, int idrole) {
            try {
                Task<bool> UpdatedUser = HttpRequest.UsersRequest.Update(id, name, CryptedPassword, idrole);
                bool resp = await UpdatedUser;
                if (resp) {
                    MessageBox.Show(Constantes.InfoMessage.OperationSuccesss, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool CheckPassword() {
            bool bresult = true;
            if (tbxConfirmPassword.Text.Trim() != "" && tbxPassword.Text == tbxConfirmPassword.Text) {
                string s = tbxConfirmPassword.Text;
                
                if ((s.Count(c => char.IsUpper(c)) < Constantes.PasswordPolitics.Uppercase)) {
                    bresult = false;
                }

                string sChar = System.Text.RegularExpressions.Regex.Replace(s, "\\d", "");
                if (sChar.Length > Constantes.PasswordPolitics.minChar) {
                    bresult = false;
                }

                if ((s.Length < Constantes.PasswordPolitics.minNumber + Constantes.PasswordPolitics.minChar)) {
                    bresult = false;
                }

                if (!bresult) {
                    MessageBox.Show(Constantes.AlertMessage.Password.PasswordPoliticsIncorrect, "Alerte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            } else {
                bresult = false;
                MessageBox.Show(Constantes.AlertMessage.ViewUser.PassworddidntMatch, "Alerte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return bresult;
        }



        #region UI
        private void init() {
            InitializeComponent();
            this.DoubleBuffered = true;                          //  Necessary for resize when you don't have border --> Border resize
            this.SetStyle(ControlStyles.ResizeRedraw, true);     //  Border resize
        }
        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }
        #endregion

        private void btnLogOut_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
