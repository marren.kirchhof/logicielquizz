﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Security.Cryptography;

namespace QuizzManager
{
    public static class Crypt
    {
        /*
        public static string hashPassword(string RawPassword)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] hashValue;
            UTF8Encoding objUtf8 = new UTF8Encoding();
            hashValue = sha256.ComputeHash(objUtf8.GetBytes(RawPassword));
            

            
            //return Encoding.UTF8.GetString(hashValue);
            return Encoding.UTF8.GetString(hashValue);
        }

        */
        /// <summary>
        /// https://stackoverflow.com/questions/16999361/obtain-sha-256-string-of-a-string/17001289
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string hashPassword(string value)
        {
            StringBuilder Sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }
    }
}
