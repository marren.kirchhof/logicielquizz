﻿namespace QuizzManager {
    partial class QcmView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QcmView));
            this.sbViewQcm = new CxFlatUI.CxFlatStatusBar();
            this.btnExit = new CxFlatUI.CxFlatRoundButton();
            this.lbQuestionText = new CxFlatUI.CxFlatTextBox();
            this.sbxCurrentQCM = new CxFlatUI.CxFlatSwitch();
            this.tbxCurrentQCM = new CxFlatUI.CxFlatTextBox();
            this.gbxEdition = new CxFlatUI.CxFlatGroupBox();
            this.btnAction = new CxFlatUI.CxFlatSimpleButton();
            this.btnAddQcm = new CxFlatUI.CxFlatSimpleButton();
            this.btnDelQcm = new CxFlatUI.CxFlatSimpleButton();
            this.btnEditQcm = new CxFlatUI.CxFlatSimpleButton();
            this.btnInfoCorrection = new CxFlatUI.Controls.CxFlatButton();
            this.btnInfoReponse = new CxFlatUI.CxFlatRoundButton();
            this.clbxQcms = new System.Windows.Forms.CheckedListBox();
            this.gbxEdition.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbViewQcm
            // 
            this.sbViewQcm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewQcm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewQcm.Location = new System.Drawing.Point(0, 0);
            this.sbViewQcm.Name = "sbViewQcm";
            this.sbViewQcm.Size = new System.Drawing.Size(648, 40);
            this.sbViewQcm.TabIndex = 0;
            this.sbViewQcm.Text = "QCM";
            this.sbViewQcm.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnExit
            // 
            this.btnExit.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnExit.Location = new System.Drawing.Point(12, 46);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Retour";
            this.btnExit.TextColor = System.Drawing.Color.White;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbQuestionText
            // 
            this.lbQuestionText.Enabled = false;
            this.lbQuestionText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbQuestionText.Hint = "";
            this.lbQuestionText.Location = new System.Drawing.Point(18, 75);
            this.lbQuestionText.MaxLength = 32767;
            this.lbQuestionText.Multiline = true;
            this.lbQuestionText.Name = "lbQuestionText";
            this.lbQuestionText.PasswordChar = '\0';
            this.lbQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbQuestionText.SelectedText = "";
            this.lbQuestionText.SelectionLength = 0;
            this.lbQuestionText.SelectionStart = 0;
            this.lbQuestionText.Size = new System.Drawing.Size(421, 125);
            this.lbQuestionText.TabIndex = 12;
            this.lbQuestionText.TabStop = false;
            this.lbQuestionText.Text = "Question";
            this.lbQuestionText.UseSystemPasswordChar = false;
            // 
            // sbxCurrentQCM
            // 
            this.sbxCurrentQCM.AutoSize = true;
            this.sbxCurrentQCM.Location = new System.Drawing.Point(6, 15);
            this.sbxCurrentQCM.Name = "sbxCurrentQCM";
            this.sbxCurrentQCM.Size = new System.Drawing.Size(40, 20);
            this.sbxCurrentQCM.TabIndex = 13;
            this.sbxCurrentQCM.Text = "cxFlatSwitch1";
            this.sbxCurrentQCM.UseVisualStyleBackColor = true;
            // 
            // tbxCurrentQCM
            // 
            this.tbxCurrentQCM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxCurrentQCM.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxCurrentQCM.Hint = "Edition / ajout QCM";
            this.tbxCurrentQCM.Location = new System.Drawing.Point(52, 5);
            this.tbxCurrentQCM.MaxLength = 32767;
            this.tbxCurrentQCM.Multiline = false;
            this.tbxCurrentQCM.Name = "tbxCurrentQCM";
            this.tbxCurrentQCM.PasswordChar = '\0';
            this.tbxCurrentQCM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxCurrentQCM.SelectedText = "";
            this.tbxCurrentQCM.SelectionLength = 0;
            this.tbxCurrentQCM.SelectionStart = 0;
            this.tbxCurrentQCM.Size = new System.Drawing.Size(369, 38);
            this.tbxCurrentQCM.TabIndex = 14;
            this.tbxCurrentQCM.TabStop = false;
            this.tbxCurrentQCM.UseSystemPasswordChar = false;
            this.tbxCurrentQCM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxCurrentQCM_KeyDown);
            // 
            // gbxEdition
            // 
            this.gbxEdition.Controls.Add(this.sbxCurrentQCM);
            this.gbxEdition.Controls.Add(this.tbxCurrentQCM);
            this.gbxEdition.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxEdition.Location = new System.Drawing.Point(12, 264);
            this.gbxEdition.Name = "gbxEdition";
            this.gbxEdition.ShowText = false;
            this.gbxEdition.Size = new System.Drawing.Size(427, 48);
            this.gbxEdition.TabIndex = 15;
            this.gbxEdition.TabStop = false;
            this.gbxEdition.Text = "cxFlatGroupBox1";
            this.gbxEdition.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.ButtonType = CxFlatUI.ButtonType.Default;
            this.btnAction.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAction.Location = new System.Drawing.Point(445, 373);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(191, 45);
            this.btnAction.TabIndex = 16;
            this.btnAction.Text = "terminé";
            this.btnAction.TextColor = System.Drawing.Color.White;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // btnAddQcm
            // 
            this.btnAddQcm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddQcm.ButtonType = CxFlatUI.ButtonType.Success;
            this.btnAddQcm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAddQcm.Location = new System.Drawing.Point(294, 329);
            this.btnAddQcm.MinimumSize = new System.Drawing.Size(128, 38);
            this.btnAddQcm.Name = "btnAddQcm";
            this.btnAddQcm.Size = new System.Drawing.Size(128, 38);
            this.btnAddQcm.TabIndex = 17;
            this.btnAddQcm.Text = "Ajouter";
            this.btnAddQcm.TextColor = System.Drawing.Color.White;
            this.btnAddQcm.Click += new System.EventHandler(this.btnAddQcm_Click);
            // 
            // btnDelQcm
            // 
            this.btnDelQcm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelQcm.ButtonType = CxFlatUI.ButtonType.Danger;
            this.btnDelQcm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelQcm.Location = new System.Drawing.Point(26, 329);
            this.btnDelQcm.MinimumSize = new System.Drawing.Size(128, 38);
            this.btnDelQcm.Name = "btnDelQcm";
            this.btnDelQcm.Size = new System.Drawing.Size(128, 38);
            this.btnDelQcm.TabIndex = 18;
            this.btnDelQcm.Text = "Supprimer";
            this.btnDelQcm.TextColor = System.Drawing.Color.White;
            this.btnDelQcm.Click += new System.EventHandler(this.btnDelQcm_Click);
            // 
            // btnEditQcm
            // 
            this.btnEditQcm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditQcm.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnEditQcm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnEditQcm.Location = new System.Drawing.Point(160, 329);
            this.btnEditQcm.MinimumSize = new System.Drawing.Size(128, 38);
            this.btnEditQcm.Name = "btnEditQcm";
            this.btnEditQcm.Size = new System.Drawing.Size(128, 38);
            this.btnEditQcm.TabIndex = 19;
            this.btnEditQcm.Text = "Editer";
            this.btnEditQcm.TextColor = System.Drawing.Color.White;
            this.btnEditQcm.Click += new System.EventHandler(this.btnEditQcm_Click);
            // 
            // btnInfoCorrection
            // 
            this.btnInfoCorrection.ButtonType = CxFlatUI.ButtonType.Default;
            this.btnInfoCorrection.Enabled = false;
            this.btnInfoCorrection.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnInfoCorrection.Location = new System.Drawing.Point(18, 235);
            this.btnInfoCorrection.Name = "btnInfoCorrection";
            this.btnInfoCorrection.Size = new System.Drawing.Size(415, 23);
            this.btnInfoCorrection.TabIndex = 21;
            this.btnInfoCorrection.Text = "Correction";
            this.btnInfoCorrection.TextColor = System.Drawing.Color.White;
            // 
            // btnInfoReponse
            // 
            this.btnInfoReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfoReponse.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnInfoReponse.Enabled = false;
            this.btnInfoReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnInfoReponse.Location = new System.Drawing.Point(445, 75);
            this.btnInfoReponse.Name = "btnInfoReponse";
            this.btnInfoReponse.Size = new System.Drawing.Size(191, 23);
            this.btnInfoReponse.TabIndex = 22;
            this.btnInfoReponse.Text = "Liste des réponses";
            this.btnInfoReponse.TextColor = System.Drawing.Color.White;
            // 
            // clbxQcms
            // 
            this.clbxQcms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clbxQcms.FormattingEnabled = true;
            this.clbxQcms.Location = new System.Drawing.Point(445, 108);
            this.clbxQcms.Name = "clbxQcms";
            this.clbxQcms.Size = new System.Drawing.Size(191, 259);
            this.clbxQcms.TabIndex = 11;
            this.clbxQcms.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbxQcms_ItemCheck);
            this.clbxQcms.SelectedValueChanged += new System.EventHandler(this.clbxQcms_SelectedValueChanged);
            // 
            // QcmView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 427);
            this.Controls.Add(this.btnInfoReponse);
            this.Controls.Add(this.btnInfoCorrection);
            this.Controls.Add(this.btnEditQcm);
            this.Controls.Add(this.btnDelQcm);
            this.Controls.Add(this.btnAddQcm);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.gbxEdition);
            this.Controls.Add(this.lbQuestionText);
            this.Controls.Add(this.clbxQcms);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.sbViewQcm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(648, 427);
            this.Name = "QcmView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QcmView";
            this.gbxEdition.ResumeLayout(false);
            this.gbxEdition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewQcm;
        private CxFlatUI.CxFlatRoundButton btnExit;
        private CxFlatUI.CxFlatTextBox lbQuestionText;
        private CxFlatUI.CxFlatSwitch sbxCurrentQCM;
        private CxFlatUI.CxFlatTextBox tbxCurrentQCM;
        private CxFlatUI.CxFlatGroupBox gbxEdition;
        private CxFlatUI.CxFlatSimpleButton btnAction;
        private CxFlatUI.CxFlatSimpleButton btnAddQcm;
        private CxFlatUI.CxFlatSimpleButton btnDelQcm;
        private CxFlatUI.CxFlatSimpleButton btnEditQcm;
        private CxFlatUI.Controls.CxFlatButton btnInfoCorrection;
        private CxFlatUI.CxFlatRoundButton btnInfoReponse;
        private System.Windows.Forms.CheckedListBox clbxQcms;
    }
}