﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzManager
{
    public partial class fLogin : Form
    {
        public fLogin()
        {
            InitializeComponent();
            sbLogin.Text = Constantes.TitleForm.ViewLogin;

        }



        private void btnLogin_Click(object sender, EventArgs e)
        {
           CheckIfuserExist(tbxuserName.Text, Crypt.hashPassword(tbxPassword.Text));
        }


        #region action BDD
        /// <summary>
        /// Check if the user exist in the database and check if he can access to the application
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Sha256Password"></param>
        private async void CheckIfuserExist(string Username, string Sha256Password)
        {
            try
            {
                Task<User> Rexist = HttpRequest.UsersRequest.Find(Username);
                User UResultatExist = await Rexist;

                if (UResultatExist != null && UResultatExist.username != "" && UResultatExist.password == Sha256Password && UResultatExist.role.level == Constantes.Global.LevelAdmin) {
                    Task<List<Quizz>> lUser = HttpRequest.QuizzesRequest.GetQuizzesWhoUserIsOwner(UResultatExist.idUser);
                    User FinalUser = UResultatExist;
                    List<Quizz>ltemp = await lUser;

                    FinalUser.quizzes_owned = ltemp;

                    Accueil A = new Accueil(FinalUser);
                    this.Visible = false;
                    A.ShowDialog();
                    this.Visible = true;
                    tbxPassword.Text = "";
                } else {
                    MessageBox.Show(Constantes.AlertMessage.Login.LoginFail, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
        /// <summary>
        /// envent enter in tbxPassword
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                btnLogin_Click(null, null);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
    }
}
