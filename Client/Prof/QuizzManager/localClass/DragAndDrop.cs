﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzManager {
    class DragAndDrop {
        public List<ResponseDragDrop> Response { get; set; }
        public List<string> WrongQuestion { get; set; }
    }

    class ResponseDragDrop {
        public ResponseDragDrop(string title, List<string> listquestion) {
            ListQuestion = listquestion;
            Title = title;
        }
        public string Title { get; set; }
        public List<string> ListQuestion { get; set; }
    }
}

    

