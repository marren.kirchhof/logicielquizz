﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzManager
{
    public class Question
    {

        public Question(int idQuestion, string title,int Quizz_id , int type_id, string correction)
        {
            this.idQuestion = idQuestion;
            this.title = title;
            idQuizz = Quizz_id;
            idType = type_id;
            this.correction = correction;
        }

        public int idQuestion { get; set; }
        public string title { get; set; }
        public int idQuizz { get; set; }
        public int idType { get; set; }
        public string correction { get; set; }
    }
}
