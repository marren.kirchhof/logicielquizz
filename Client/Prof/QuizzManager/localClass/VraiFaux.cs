﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzManager {
    public class VraiFaux {
        public VraiFaux(bool response,string TrueResp,string FalseResp) {
            ResponseCorrection = response;
            ResponseTrue = TrueResp;
            ResponseFalse = FalseResp;
        }
        public bool ResponseCorrection { get; set; }
        public string ResponseTrue { get; set; }
        public string ResponseFalse { get; set; }

    }
}
