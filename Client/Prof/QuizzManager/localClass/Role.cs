﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzManager.localClass {
    public class Role {
        private int _id;
        private string _name;
        private int _level;

        public Role(int id, string name, int level) {
            _id = id;
            _name = name;
            _level = level;
        }

        public int idRole { get {return _id; } set { _id = value; } }
        public string name { get { return _name; } set { _name = value; } }
        public int level { get { return _level; } set { _level = value; } }
    }
}
