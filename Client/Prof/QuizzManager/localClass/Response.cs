﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzManager {
    public class Response {

        public Response(int id,int idUser,string nom,string repuser,string correction,int idquestion) {
            ID_Reponse = id;
            ID_Utilisateur = idUser;
            Nom = nom;
            Rep_user = repuser;
            Correction = correction;
            ID_Question = idquestion;
        }

        public int ID_Reponse { get; set; }
        public int ID_Utilisateur { get; set; }
        public string Nom { get; set; }
        public string Rep_user { get; set; }
        public string Correction { get; set; }
        public int ID_Question { get; set; }

        
    }
}
