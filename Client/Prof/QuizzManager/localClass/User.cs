﻿using QuizzManager.localClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzManager {
    public class User {
        private int _id;
        private string _username;
        private string _password;
        private int _idRole;
        private List<Quizz> LQuizzOwned;
        private Role _role;





        public User(int ID_Utilisateur, string Nom, string Password) {
            _id = ID_Utilisateur;
            _username = Nom;
            _password = Password;
        }

        public int idUser { get { return _id; } set { _id = value; } }
        public string username { get { return _username; } set { if (value.Trim() != "") { _username = value; } } }
        public string password { get { return _password; } set { if (value.Trim() != "") { _password = value; } } }
        public List<Quizz> quizzes_owned { get { return this.LQuizzOwned; } set { LQuizzOwned = value; } }
        public int idRole { get { return _idRole; } set { _idRole = value; } }

        public Role role { get { return _role; } set { _role = value; } }

}



    
}
