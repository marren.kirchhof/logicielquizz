﻿namespace QuizzManager
{
    partial class fLogin
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fLogin));
            this.sbLogin = new CxFlatUI.CxFlatStatusBar();
            this.tbxuserName = new CxFlatUI.CxFlatTextBox();
            this.btnLogin = new CxFlatUI.Controls.CxFlatButton();
            this.tbxPassword = new CxFlatUI.CxFlatTextBox();
            this.cxFlatPictureBox1 = new CxFlatUI.CxFlatPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.cxFlatPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // sbLogin
            // 
            this.sbLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbLogin.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbLogin.Location = new System.Drawing.Point(0, 0);
            this.sbLogin.Name = "sbLogin";
            this.sbLogin.Size = new System.Drawing.Size(414, 40);
            this.sbLogin.TabIndex = 0;
            this.sbLogin.Text = "Login";
            this.sbLogin.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // tbxuserName
            // 
            this.tbxuserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxuserName.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxuserName.Hint = "Nom d\'utilisateur";
            this.tbxuserName.Location = new System.Drawing.Point(12, 116);
            this.tbxuserName.MaxLength = 32767;
            this.tbxuserName.Multiline = false;
            this.tbxuserName.Name = "tbxuserName";
            this.tbxuserName.PasswordChar = '\0';
            this.tbxuserName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxuserName.SelectedText = "";
            this.tbxuserName.SelectionLength = 0;
            this.tbxuserName.SelectionStart = 0;
            this.tbxuserName.Size = new System.Drawing.Size(389, 38);
            this.tbxuserName.TabIndex = 1;
            this.tbxuserName.TabStop = false;
            this.tbxuserName.UseSystemPasswordChar = false;
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogin.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnLogin.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLogin.Location = new System.Drawing.Point(13, 206);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(389, 23);
            this.btnLogin.TabIndex = 3;
            this.btnLogin.TabStop = false;
            this.btnLogin.Text = "Login";
            this.btnLogin.TextColor = System.Drawing.Color.White;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // tbxPassword
            // 
            this.tbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxPassword.Hint = "Password";
            this.tbxPassword.Location = new System.Drawing.Point(12, 160);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.PasswordChar = '*';
            this.tbxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPassword.SelectedText = "";
            this.tbxPassword.SelectionLength = 0;
            this.tbxPassword.SelectionStart = 0;
            this.tbxPassword.Size = new System.Drawing.Size(389, 38);
            this.tbxPassword.TabIndex = 2;
            this.tbxPassword.TabStop = false;
            this.tbxPassword.UseSystemPasswordChar = false;
            this.tbxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxPassword_KeyDown);
            // 
            // cxFlatPictureBox1
            // 
            this.cxFlatPictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cxFlatPictureBox1.Image = global::QuizzManager.Properties.Resources.login;
            this.cxFlatPictureBox1.Location = new System.Drawing.Point(167, 46);
            this.cxFlatPictureBox1.Name = "cxFlatPictureBox1";
            this.cxFlatPictureBox1.Size = new System.Drawing.Size(90, 60);
            this.cxFlatPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cxFlatPictureBox1.TabIndex = 4;
            this.cxFlatPictureBox1.TabStop = false;
            // 
            // fLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 241);
            this.Controls.Add(this.cxFlatPictureBox1);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.tbxuserName);
            this.Controls.Add(this.sbLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.Name = "fLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            ((System.ComponentModel.ISupportInitialize)(this.cxFlatPictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbLogin;
        private CxFlatUI.CxFlatTextBox tbxuserName;
        private CxFlatUI.Controls.CxFlatButton btnLogin;
        private CxFlatUI.CxFlatTextBox tbxPassword;
        private CxFlatUI.CxFlatPictureBox cxFlatPictureBox1;
    }
}

