﻿namespace QuizzManager {
    partial class QuestionView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionView));
            this.sbViewQuestion = new CxFlatUI.CxFlatStatusBar();
            this.btnExit = new CxFlatUI.CxFlatRoundButton();
            this.cbxType = new CxFlatUI.CxFlatComboBox();
            this.btnNext = new CxFlatUI.Controls.CxFlatButton();
            this.tbxTextQuestion = new CxFlatUI.CxFlatTextBox();
            this.lbInfoQuestion = new CxFlatUI.Controls.CxFlatButton();
            this.SuspendLayout();
            // 
            // sbViewQuestion
            // 
            this.sbViewQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewQuestion.Location = new System.Drawing.Point(0, 0);
            this.sbViewQuestion.Name = "sbViewQuestion";
            this.sbViewQuestion.Size = new System.Drawing.Size(403, 40);
            this.sbViewQuestion.TabIndex = 0;
            this.sbViewQuestion.Text = "Ajout / Edition Question";
            this.sbViewQuestion.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnExit
            // 
            this.btnExit.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnExit.Location = new System.Drawing.Point(12, 46);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 23);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Retour";
            this.btnExit.TextColor = System.Drawing.Color.White;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cbxType
            // 
            this.cbxType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbxType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxType.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.cbxType.FormattingEnabled = true;
            this.cbxType.ItemHeight = 30;
            this.cbxType.Location = new System.Drawing.Point(270, 100);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(121, 36);
            this.cbxType.TabIndex = 11;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnNext.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnNext.Location = new System.Drawing.Point(270, 143);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(121, 37);
            this.btnNext.TabIndex = 12;
            this.btnNext.Text = "Suivant";
            this.btnNext.TextColor = System.Drawing.Color.White;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // tbxTextQuestion
            // 
            this.tbxTextQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxTextQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxTextQuestion.Hint = "Question à poser";
            this.tbxTextQuestion.Location = new System.Drawing.Point(13, 127);
            this.tbxTextQuestion.MaxLength = 32767;
            this.tbxTextQuestion.Multiline = true;
            this.tbxTextQuestion.Name = "tbxTextQuestion";
            this.tbxTextQuestion.PasswordChar = '\0';
            this.tbxTextQuestion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxTextQuestion.SelectedText = "";
            this.tbxTextQuestion.SelectionLength = 0;
            this.tbxTextQuestion.SelectionStart = 0;
            this.tbxTextQuestion.Size = new System.Drawing.Size(251, 53);
            this.tbxTextQuestion.TabIndex = 13;
            this.tbxTextQuestion.TabStop = false;
            this.tbxTextQuestion.UseSystemPasswordChar = false;
            // 
            // lbInfoQuestion
            // 
            this.lbInfoQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoQuestion.ButtonType = CxFlatUI.ButtonType.Default;
            this.lbInfoQuestion.Enabled = false;
            this.lbInfoQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoQuestion.Location = new System.Drawing.Point(13, 98);
            this.lbInfoQuestion.Name = "lbInfoQuestion";
            this.lbInfoQuestion.Size = new System.Drawing.Size(251, 23);
            this.lbInfoQuestion.TabIndex = 14;
            this.lbInfoQuestion.Text = "Intitulé de la question";
            this.lbInfoQuestion.TextColor = System.Drawing.Color.White;
            // 
            // QuestionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 192);
            this.Controls.Add(this.lbInfoQuestion);
            this.Controls.Add(this.tbxTextQuestion);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.cbxType);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.sbViewQuestion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(600, 800);
            this.MinimumSize = new System.Drawing.Size(403, 192);
            this.Name = "QuestionView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QuestionView";
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewQuestion;
        private CxFlatUI.CxFlatRoundButton btnExit;
        private CxFlatUI.CxFlatComboBox cbxType;
        private CxFlatUI.Controls.CxFlatButton btnNext;
        private CxFlatUI.CxFlatTextBox tbxTextQuestion;
        private CxFlatUI.Controls.CxFlatButton lbInfoQuestion;
    }
}