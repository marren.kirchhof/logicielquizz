﻿using QuizzManagerControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QuizzManager.Constantes.UI;
using System.Windows.Forms;


namespace QuizzManager {
    public partial class DragDropView : Form {

        ControlQuestion CurrentQcontrol;
        ControlResponse CurrentRcontrol;
        QuestionType CurrentType;
        Question CurrentQuestion;
        User CurrentUser;
        Quizz CurrentQuizz;

        string ActionT;


        /// <summary>
        /// Constructor when the user ant to edit a question
        /// </summary>
        /// <param name="ConnectedUser">The current User</param>
        /// <param name="CurrentQuizz">The current Quizz</param>
        /// <param name="QuestionToEdit">the question to edit</param>
        /// <param name="ActionType">Constantes.TypeAction</param>
        /// <param name="QT">The current question type</param>
        public DragDropView(User ConnectedUser, Quizz CurrentQuizz, Question QuestionToEdit, string ActionType, QuestionType QT) {
            InitializeComponent();
            CurrentType = QT;
            this.CurrentQuizz = CurrentQuizz;
            ActionT = ActionType;
            CurrentUser = ConnectedUser;
            CurrentQuestion = QuestionToEdit;
            SetUI(ActionT);
            setParamsResizeForm();
            lbQuestionText.Text = CurrentQuestion.title;
            LoadDataDragDrop();
        }
        /// <summary>
        /// Constructor when the user want to add a new question
        /// </summary>
        /// <param name="ConnectedUser">The current User</param>
        /// <param name="CurrentQuizz">The current Quizz</param>
        /// <param name="title">the question string</param>
        /// <param name="ActionType">Constantes.TypeAction</param>
        /// <param name="QT">The current question type</param>
        public DragDropView(User ConnectedUser, Quizz CurrentQuizz, string title, string ActionType, QuestionType QT) {
            InitializeComponent();
            CurrentType = QT;
            this.CurrentQuizz = CurrentQuizz;
            ActionT = ActionType;
            CurrentUser = ConnectedUser;
            SetUI(ActionT);
            setParamsResizeForm();
            lbQuestionText.Text = title;
        }


        #region Load 
        /// <summary>
        /// If necessary load the data of the current drag and dorp
        /// </summary>
        private async void LoadDataDragDrop() {
            try {
                if (ActionT == Constantes.TypeAction.Edit) {

                    Task<Question> Question = HttpRequest.QuestionRequest.GetOne(CurrentQuestion.idQuestion);

                    Question q = await Question;
                    DragAndDrop R = DeserializeJSON(q.correction);

                    if (R != null) {

                        foreach (string s in R.WrongQuestion) {
                            CreateQuestion(s);
                        }
                        int i = 0;
                        foreach (ResponseDragDrop r in R.Response) {
                            CreateResponse(r.Title);
                            foreach (string sQuestion in r.ListQuestion) {
                                ControlQuestion Q = new ControlQuestion(sQuestion);
                                ((ControlResponse)flpReponse.Controls[i]).flpResponse.Controls.Add(Q);
                                Q.Click += Q_Click;
                                Q.Width = Q.Parent.Width - iMargeFlp;

                            }
                            i++;
                        }
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Convert a json string to a drag drop
        /// </summary>
        /// <param name="JsonString">The drag drop json string</param>
        /// <returns>The object drag and drop</returns>
        private DragAndDrop DeserializeJSON(string JsonString) {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<DragAndDrop>(JsonString);
        }

        /// <summary>
        /// Set the style interface
        /// </summary>
        /// <param name="ActionType">Constantes.TypeAction</param>
        private void SetUI(string ActionType) {
            switch (ActionType) {
                case Constantes.TypeAction.Add:
                    UiAdd();
                    break;
                case Constantes.TypeAction.Edit:
                    UiEdit();
                    break;
            }

        }
        /// <summary>
        /// Interface style add
        /// </summary>
        private void UiAdd() {
            sbViewDragDrop.Text = Constantes.TitleForm.ViewDragDropAdd;
            ActionT = Constantes.TypeAction.Add;
            btnValide.ButtonType = CxFlatUI.ButtonType.Success;
        }
        /// <summary>
        /// Interface style edit
        /// </summary>
        private void UiEdit() {
            sbViewDragDrop.Text = Constantes.TitleForm.ViewDragDropEdit;
            ActionT = Constantes.TypeAction.Edit;
            btnValide.ButtonType = CxFlatUI.ButtonType.Primary;
        }

        #endregion
        #region UI
        #region resize form
        private void setParamsResizeForm() {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }






        #endregion

        #endregion
        #region Func dragdrop
        /// <summary>
        /// Create a new question in flpquesiton
        /// </summary>
        private void btnCreateQuestion_Click(object sender, EventArgs e) {
            CreateQuestion(tbxQuestion.Text);
            tbxQuestion.Clear();
        }
        /// <summary>
        /// add a question in flpquestion
        /// </summary>
        /// <param name="Name">name of the question</param>
        private void CreateQuestion(string Name) {
            ControlQuestion Q = new ControlQuestion(Name);
            Q.Width = flpQuestion.Width - Constantes.UI.iMargeFlp;
            flpQuestion.Controls.Add(Q);
            Q.Click += Q_Click; ;
            Q.Width = Q.Parent.Width - Constantes.UI.iMargeFlp;
        }

        /// <summary>
        /// when the user clic on a question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Q_Click(object sender, EventArgs e) {

            if (CurrentQcontrol != null) {
                CurrentQcontrol.Visible = true;
                CurrentQcontrol.Width = CurrentQcontrol.Parent.Width - Constantes.UI.iMargeFlp;
                CurrentQcontrol = (ControlQuestion)(sender);
                CurrentQcontrol.Visible = false;

            } else if (CurrentQcontrol == (ControlQuestion)(sender)) {
                CurrentQcontrol.Width = CurrentQcontrol.Parent.Width - Constantes.UI.iMargeFlp;
                CurrentQcontrol.Visible = true;
            } else {
                CurrentQcontrol = (ControlQuestion)(sender);
                CurrentQcontrol.Visible = false;
            }
        }
        /// <summary>
        /// when the user clic on flpQuestion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flpQuestion_Click(object sender, EventArgs e) {
            if (CurrentQcontrol != null) {
                CurrentQcontrol.Width = flpQuestion.Width - Constantes.UI.iMargeFlp;
                flpQuestion.Controls.Add(CurrentQcontrol);
                CurrentQcontrol.Visible = true;
                CurrentQcontrol = null;
            }
        }

        //Response
        /// <summary>
        /// create response in flpresponse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateResponse_Click(object sender, EventArgs e) {
            CreateResponse(tbxResponse.Text);
            tbxResponse.Clear();
        }

        /// <summary>
        /// Add a control response in flpresponse
        /// </summary>
        /// <param name="Name"></param>
        private void CreateResponse(string Name) {
            ControlResponse R = new ControlResponse(Name);
            R.flpResponse.Click += FlpReponse_Click;
            R.Width = flpReponse.Width - iMargeFlp;
            R.Anchor = (AnchorStyles.Left | AnchorStyles.Right);
            flpReponse.Controls.Add(R);
        }


        //UserControl.flpuser_Click
        /// <summary>
        /// If the user clic on a response in a dragdrop
        /// </summary>
        /// <param name="sender">FlowLayoutPanel --> flpQuestion of the dragdrop</param>
        /// <param name="e"></param>
        private void FlpReponse_Click(object sender, EventArgs e) {
            if (CurrentRcontrol == null && CurrentQcontrol == null) {
                CurrentRcontrol = (ControlResponse)((FlowLayoutPanel)(sender)).Parent;
                CurrentRcontrol.Visible = false;
            } else if (CurrentRcontrol != (ControlResponse)((FlowLayoutPanel)(sender)).Parent && CurrentQcontrol == null) {
                CurrentRcontrol.Visible = true;
                CurrentRcontrol = (ControlResponse)((FlowLayoutPanel)(sender)).Parent;
                CurrentRcontrol.Visible = false;
            } else {
                CurrentRcontrol = (ControlResponse)((FlowLayoutPanel)(sender)).Parent;
                if (CurrentQcontrol != null) {
                    CurrentQcontrol.Anchor = (AnchorStyles.Left | AnchorStyles.Right);
                    CurrentQcontrol.Width = CurrentRcontrol.Width + Constantes.UI.iMargeFlp;
                    CurrentRcontrol.flpResponse.Controls.Add(CurrentQcontrol);
                    CurrentQcontrol.Visible = true;
                    CurrentQcontrol = null;
                } else { CurrentRcontrol.Visible = false; }
            }
        }

        //this.flpResponse
        /// <summary>
        /// the flp response who contains all the response
        /// </summary>
        private void flpReponse_Click_1(object sender, EventArgs e) {
            if (CurrentRcontrol != null) {
                CurrentRcontrol.Visible = true;
                CurrentRcontrol = null;

                if (CurrentQcontrol != null) {
                    CurrentQcontrol.Visible = true;
                    CurrentQcontrol = null;
                }
            }
        }

        /// <summary>
        /// Delete the current question and the current response
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e) {

            flpReponse.Controls.Remove(CurrentRcontrol);
            flpQuestion.Controls.Remove(CurrentQcontrol);

            CurrentQcontrol = null;
            CurrentRcontrol = null;
        }



        /// <summary>
        /// Get all answer in flpQuestion
        /// </summary>
        /// <returns></returns>
        private List<string> GetAllWrongAnswer() {
            List<string> lr = new List<string>();
            foreach (ControlQuestion q in flpQuestion.Controls) {
                lr.Add(q.ValueText);
            }
            return lr;
        }

        /// <summary>
        /// get all response in the interface
        /// </summary>
        /// <returns></returns>
        private List<ResponseDragDrop> GetAllResp() {
            List<ResponseDragDrop> Lr = new List<ResponseDragDrop>();
            foreach (ControlResponse cR in flpReponse.Controls) {
                List<string> LQuestion = new List<string>();
                string titleRep = cR.Title;
                foreach (ControlQuestion cQ in cR.flpResponse.Controls) {
                    LQuestion.Add(cQ.ValueText);
                }
                Lr.Add(new ResponseDragDrop(titleRep, LQuestion));
            }
            return Lr;
        }


        #endregion
        #region json
        /// <summary>
        /// Check and return a json of the current dragdrop
        /// </summary>
        /// <returns></returns>
        private string GetJsonStringResponse() {
            DragAndDrop R = new DragAndDrop();

            R.WrongQuestion = GetAllWrongAnswer();
            R.Response = GetAllResp();

            string jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(R);

            DragAndDrop Rtest = Newtonsoft.Json.JsonConvert.DeserializeObject<DragAndDrop>(jsonstring);

            return jsonstring;
        }
        #endregion
        #region Navigation button

        /// <summary>
        /// Quit the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }
        
        /// <summary>
        /// Create or edit the question dragdrop
        /// </summary>
        private async void btnValide_Click_1(object sender, EventArgs e) {
            string jsonString = GetJsonStringResponse();
            switch (ActionT) {
                case Constantes.TypeAction.Add:
                    Task<Question> DDadd = HttpRequest.QuestionRequest.Create(lbQuestionText.Text, CurrentQuizz.idQuizz, CurrentType.idType, jsonString);
                    Question ddResultatAdd = await DDadd;
                    this.Close();
                    break;
                case Constantes.TypeAction.Edit:
                    Task<bool> DDedit = HttpRequest.QuestionRequest.Update(CurrentQuestion.idQuestion,lbQuestionText.Text, CurrentQuizz.idQuizz, CurrentType.idType, jsonString);
                    bool ddResultatEdit = await DDedit;
                    this.Close();
                    break;
            }
        }
        #endregion

        /// <summary>
        /// Enter in tbxQuestion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxQuestion_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                CreateQuestion(tbxQuestion.Text);
                e.Handled = true;
                e.SuppressKeyPress = true;
                tbxQuestion.Clear();
            }
        }

        /// <summary>
        /// Enter in tbxResponse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxResponse_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                CreateResponse(tbxResponse.Text);
                e.Handled = true;
                e.SuppressKeyPress = true;
                tbxResponse.Clear();
            }
        }
    }
}
