﻿namespace QuizzManager {
    partial class VraiFauxView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VraiFauxView));
            this.sbViewVraiFaux = new CxFlatUI.CxFlatStatusBar();
            this.tbxQuestion = new CxFlatUI.CxFlatTextBox();
            this.btnExit = new CxFlatUI.CxFlatRoundButton();
            this.btnAction = new CxFlatUI.CxFlatSimpleButton();
            this.gbxReponse = new CxFlatUI.CxFlatGroupBox();
            this.tbxFalse = new CxFlatUI.CxFlatTextBox();
            this.tbxTrue = new CxFlatUI.CxFlatTextBox();
            this.rbFalse = new CxFlatUI.CxFlatRadioButton();
            this.rbTrue = new CxFlatUI.CxFlatRadioButton();
            this.gbxReponse.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbViewVraiFaux
            // 
            this.sbViewVraiFaux.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewVraiFaux.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewVraiFaux.Location = new System.Drawing.Point(0, 0);
            this.sbViewVraiFaux.Name = "sbViewVraiFaux";
            this.sbViewVraiFaux.Size = new System.Drawing.Size(529, 40);
            this.sbViewVraiFaux.TabIndex = 0;
            this.sbViewVraiFaux.Text = "Ajout / Edition d\'une question Vrai faux";
            this.sbViewVraiFaux.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // tbxQuestion
            // 
            this.tbxQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxQuestion.Enabled = false;
            this.tbxQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxQuestion.Hint = "";
            this.tbxQuestion.Location = new System.Drawing.Point(13, 75);
            this.tbxQuestion.MaxLength = 32767;
            this.tbxQuestion.MinimumSize = new System.Drawing.Size(206, 78);
            this.tbxQuestion.Multiline = true;
            this.tbxQuestion.Name = "tbxQuestion";
            this.tbxQuestion.PasswordChar = '\0';
            this.tbxQuestion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxQuestion.SelectedText = "";
            this.tbxQuestion.SelectionLength = 0;
            this.tbxQuestion.SelectionStart = 0;
            this.tbxQuestion.Size = new System.Drawing.Size(207, 134);
            this.tbxQuestion.TabIndex = 1;
            this.tbxQuestion.TabStop = false;
            this.tbxQuestion.UseSystemPasswordChar = false;
            // 
            // btnExit
            // 
            this.btnExit.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnExit.Location = new System.Drawing.Point(13, 46);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Retour";
            this.btnExit.TextColor = System.Drawing.Color.White;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.ButtonType = CxFlatUI.ButtonType.Default;
            this.btnAction.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAction.Location = new System.Drawing.Point(226, 176);
            this.btnAction.MinimumSize = new System.Drawing.Size(203, 33);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(291, 33);
            this.btnAction.TabIndex = 11;
            this.btnAction.Text = "valider";
            this.btnAction.TextColor = System.Drawing.Color.White;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // gbxReponse
            // 
            this.gbxReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxReponse.Controls.Add(this.tbxFalse);
            this.gbxReponse.Controls.Add(this.tbxTrue);
            this.gbxReponse.Controls.Add(this.rbFalse);
            this.gbxReponse.Controls.Add(this.rbTrue);
            this.gbxReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxReponse.Location = new System.Drawing.Point(226, 75);
            this.gbxReponse.MinimumSize = new System.Drawing.Size(203, 39);
            this.gbxReponse.Name = "gbxReponse";
            this.gbxReponse.ShowText = false;
            this.gbxReponse.Size = new System.Drawing.Size(291, 95);
            this.gbxReponse.TabIndex = 12;
            this.gbxReponse.TabStop = false;
            this.gbxReponse.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // tbxFalse
            // 
            this.tbxFalse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxFalse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxFalse.Hint = "Faux";
            this.tbxFalse.Location = new System.Drawing.Point(37, 51);
            this.tbxFalse.MaxLength = 32767;
            this.tbxFalse.Multiline = false;
            this.tbxFalse.Name = "tbxFalse";
            this.tbxFalse.PasswordChar = '\0';
            this.tbxFalse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxFalse.SelectedText = "";
            this.tbxFalse.SelectionLength = 0;
            this.tbxFalse.SelectionStart = 0;
            this.tbxFalse.Size = new System.Drawing.Size(248, 38);
            this.tbxFalse.TabIndex = 3;
            this.tbxFalse.TabStop = false;
            this.tbxFalse.UseSystemPasswordChar = false;
            // 
            // tbxTrue
            // 
            this.tbxTrue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxTrue.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxTrue.Hint = "Vrai";
            this.tbxTrue.Location = new System.Drawing.Point(37, 7);
            this.tbxTrue.MaxLength = 32767;
            this.tbxTrue.Multiline = false;
            this.tbxTrue.Name = "tbxTrue";
            this.tbxTrue.PasswordChar = '\0';
            this.tbxTrue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxTrue.SelectedText = "";
            this.tbxTrue.SelectionLength = 0;
            this.tbxTrue.SelectionStart = 0;
            this.tbxTrue.Size = new System.Drawing.Size(248, 38);
            this.tbxTrue.TabIndex = 2;
            this.tbxTrue.TabStop = false;
            this.tbxTrue.UseSystemPasswordChar = false;
            // 
            // rbFalse
            // 
            this.rbFalse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbFalse.AutoSize = true;
            this.rbFalse.CheckedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            this.rbFalse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.rbFalse.Location = new System.Drawing.Point(8, 60);
            this.rbFalse.Name = "rbFalse";
            this.rbFalse.Size = new System.Drawing.Size(25, 20);
            this.rbFalse.TabIndex = 1;
            this.rbFalse.TabStop = true;
            this.rbFalse.UseVisualStyleBackColor = true;
            // 
            // rbTrue
            // 
            this.rbTrue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbTrue.AutoSize = true;
            this.rbTrue.CheckedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            this.rbTrue.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.rbTrue.Location = new System.Drawing.Point(8, 16);
            this.rbTrue.Name = "rbTrue";
            this.rbTrue.Size = new System.Drawing.Size(25, 20);
            this.rbTrue.TabIndex = 0;
            this.rbTrue.TabStop = true;
            this.rbTrue.UseVisualStyleBackColor = true;
            // 
            // VraiFauxView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 221);
            this.Controls.Add(this.gbxReponse);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tbxQuestion);
            this.Controls.Add(this.sbViewVraiFaux);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(529, 221);
            this.Name = "VraiFauxView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VraiFauxView";
            this.gbxReponse.ResumeLayout(false);
            this.gbxReponse.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewVraiFaux;
        private CxFlatUI.CxFlatTextBox tbxQuestion;
        private CxFlatUI.CxFlatRoundButton btnExit;
        private CxFlatUI.CxFlatSimpleButton btnAction;
        private CxFlatUI.CxFlatGroupBox gbxReponse;
        private CxFlatUI.CxFlatRadioButton rbFalse;
        private CxFlatUI.CxFlatRadioButton rbTrue;
        private CxFlatUI.CxFlatTextBox tbxFalse;
        private CxFlatUI.CxFlatTextBox tbxTrue;
    }
}