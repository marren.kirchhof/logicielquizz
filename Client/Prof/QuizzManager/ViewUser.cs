﻿using QuizzManager.localClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace QuizzManager
{
    public partial class ViewUser : Form
    {

        private string ActionType;
        private User SelectedUser;

        private delegate void ActionAfterLoadRole();

        #region Constructor
        /// <summary>
        /// Constructor edit
        /// </summary>
        /// <param name="SelectedUser"></param>
        public ViewUser(User SelectedUser) {
            init();
            
            UiEdit();            
            this.SelectedUser = SelectedUser;
            cbxRole.DisplayMember = Constantes.DisplayListbox.clxRole;
            RefreshRole(FillInput);
        }
        /// <summary>
        /// Constructor edit
        /// </summary>
        public ViewUser() { 
            init();
            UiAdd();
            RefreshRole(null);
            cbxRole.DisplayMember = Constantes.DisplayListbox.clxRole;
        }


        /// <summary>
        /// Set the style interface 
        /// </summary>
        #region UI

        private void init() {
            InitializeComponent();
            this.DoubleBuffered = true;                          //  Necessary for resize when you don't have border --> Border resize
            this.SetStyle(ControlStyles.ResizeRedraw, true);     //  Border resize
        }
        private void UiAdd() {
            sbViewUser.Text = Constantes.TitleForm.ViewUserAdd;
            btnAction.ButtonType = CxFlatUI.ButtonType.Success;
            btnAction.Text = Constantes.btnTextType.Add;
            ActionType = Constantes.TypeAction.Add;
        }
        private void UiEdit() {
            sbViewUser.Text = Constantes.TitleForm.ViewUserEdit;
            btnAction.ButtonType = CxFlatUI.ButtonType.Primary;
            btnAction.Text = Constantes.btnTextType.Edit;
            ActionType = Constantes.TypeAction.Edit;
        }


        /// <summary>
        /// Fill the input interface with the current data
        /// </summary>
        private void FillInput() {
            tbxNom.Text = SelectedUser.username;
            cbxRole.SelectedIndex = cbxRole.FindStringExact(SelectedUser.role.name);
        }

        /// <summary>
        /// Load all the type of user role in the combobox cbxRole
        /// </summary>
        /// <param name="F"></param>
        private async void RefreshRole(ActionAfterLoadRole F) {
            try {
                Task<List<Role>> ListRole = HttpRequest.RolesRequest.GetAll();
                List<Role> AllRole = await ListRole;
                cbxRole.Items.Clear();
                foreach (Role r in AllRole) {
                    cbxRole.Items.Add(r);
                }
                F?.Invoke();
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #region Resize form

        private const int cGrip = 16;      //Border resize : Grip size
        private const int cCaption = 40;   //Border resize: Caption height


        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x84)
            {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption)
                {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip)
                {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }
        #endregion
        #endregion
        #endregion


        #region Button Action 

        /// <summary>
        /// Create or edit a new user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAction_Click(object sender, EventArgs e) {
            try {
                if (CheckField()) {
                    switch (ActionType) {
                        case Constantes.TypeAction.Add:
                            CreatUser(tbxNom.Text, Crypt.hashPassword(tbxPassword.Text), ((Role)cbxRole.SelectedItem).idRole);
                            this.Close();
                            break;
                        case Constantes.TypeAction.Edit:
                            UpdateUser(SelectedUser.idUser, tbxNom.Text, Crypt.hashPassword(tbxPassword.Text), ((Role)cbxRole.SelectedItem).idRole);
                            this.Close();
                            break;
                        default:
                            MessageBox.Show(Constantes.ErrorMessage.ViewUser.ErrorMessageTypeViewUser, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        #endregion


        #region fucntion database 

        /// <summary>
        /// Check if the user already exist or not
        /// Create a new user in the database 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="CryptedPassword"></param>
        /// <param name="idrole"></param>
        private async void CreatUser(string name, string CryptedPassword,int idrole ) {
            try {

                Task<User> Uexist = HttpRequest.UsersRequest.Find(name);
                User UResultatExist = await Uexist;

                if (UResultatExist == null) {
                    Task<User> NewUser = HttpRequest.UsersRequest.Create(name, CryptedPassword, idrole);
                    User resp = await NewUser;
                    MessageBox.Show(Constantes.InfoMessage.OperationSuccesss, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else {
                    MessageBox.Show(Constantes.AlertMessage.ViewUser.UserAlreadyExist, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Edit a user in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="CryptedPassword"></param>
        /// <param name="idrole"></param>
        private async void UpdateUser(int id,string name, string CryptedPassword, int idrole) {
            try {
                Task<bool> UpdatedUser = HttpRequest.UsersRequest.Update(id,name, CryptedPassword, idrole);
                bool resp = await UpdatedUser;
                if (resp) {
                    MessageBox.Show(Constantes.InfoMessage.OperationSuccesss, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion


        #region btnBack
        private void btnLogOut_Click(object sender, EventArgs e) {
            this.Close();
        }




        /// <summary>
        /// Check if the input are valid
        /// </summary>
        /// <returns>is valid</returns>
        private bool CheckField() {
            bool bValide = true;
            if (tbxNom.Text.Trim() == "" || tbxPassword.Text.Trim() == "" || tbxConfirmPassword.Text.Trim() == "" || cbxRole.SelectedIndex < 0) {
                MessageBox.Show(Constantes.AlertMessage.EmptyFields, "Alerte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                bValide = false;
            } else if (tbxConfirmPassword.Text != tbxPassword.Text) {
                MessageBox.Show(Constantes.AlertMessage.ViewUser.PassworddidntMatch, "Alerte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                bValide = false;
            }
            return bValide;
        }

        #endregion
    }
}
