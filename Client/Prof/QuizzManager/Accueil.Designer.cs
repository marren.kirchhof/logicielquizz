﻿namespace QuizzManager
{
    partial class Accueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Accueil));
            this.sbAccueil = new CxFlatUI.CxFlatStatusBar();
            this.btnLogOut = new CxFlatUI.CxFlatRoundButton();
            this.gbQuizz = new CxFlatUI.CxFlatGroupBox();
            this.lbQizzCaption = new CxFlatUI.CxFlatTextBox();
            this.lbxQuizz = new System.Windows.Forms.ListBox();
            this.btnAddQuizz = new CxFlatUI.CxFlatSimpleButton();
            this.btnEditQuizz = new CxFlatUI.CxFlatSimpleButton();
            this.btnDelQuizz = new CxFlatUI.CxFlatSimpleButton();
            this.gbActionsQuizz = new CxFlatUI.CxFlatGroupBox();
            this.gbUser = new CxFlatUI.CxFlatGroupBox();
            this.btnRefreshUser = new CxFlatUI.CxFlatRoundButton();
            this.lb = new CxFlatUI.CxFlatTextBox();
            this.lbxUser = new System.Windows.Forms.ListBox();
            this.lbxUserQuizz = new System.Windows.Forms.ListBox();
            this.lbUsersCaption = new CxFlatUI.CxFlatTextBox();
            this.gbActionsUser = new CxFlatUI.CxFlatGroupBox();
            this.btnDelUser = new CxFlatUI.CxFlatSimpleButton();
            this.btnAddUser = new CxFlatUI.CxFlatSimpleButton();
            this.btnEditUser = new CxFlatUI.CxFlatSimpleButton();
            this.lbInfoConnectedUser = new CxFlatUI.Controls.CxFlatButton();
            this.lbInfoQuizzButton = new CxFlatUI.Controls.CxFlatButton();
            this.lbInfoUserButton = new CxFlatUI.Controls.CxFlatButton();
            this.ibxArrowAddUser = new CxFlatUI.CxFlatPictureBox();
            this.ibxArrowDelUser = new CxFlatUI.CxFlatPictureBox();
            this.gbQuizz.SuspendLayout();
            this.gbActionsQuizz.SuspendLayout();
            this.gbUser.SuspendLayout();
            this.gbActionsUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibxArrowAddUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibxArrowDelUser)).BeginInit();
            this.SuspendLayout();
            // 
            // sbAccueil
            // 
            this.sbAccueil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbAccueil.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbAccueil.Location = new System.Drawing.Point(0, 0);
            this.sbAccueil.Name = "sbAccueil";
            this.sbAccueil.Size = new System.Drawing.Size(875, 40);
            this.sbAccueil.TabIndex = 0;
            this.sbAccueil.Text = "Accueil";
            this.sbAccueil.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnLogOut
            // 
            this.btnLogOut.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnLogOut.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLogOut.Location = new System.Drawing.Point(13, 47);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(127, 23);
            this.btnLogOut.TabIndex = 1;
            this.btnLogOut.Text = "Déconnexion";
            this.btnLogOut.TextColor = System.Drawing.Color.White;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // gbQuizz
            // 
            this.gbQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbQuizz.BackColor = System.Drawing.Color.White;
            this.gbQuizz.Controls.Add(this.lbQizzCaption);
            this.gbQuizz.Controls.Add(this.lbxQuizz);
            this.gbQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbQuizz.Location = new System.Drawing.Point(13, 76);
            this.gbQuizz.MinimumSize = new System.Drawing.Size(325, 0);
            this.gbQuizz.Name = "gbQuizz";
            this.gbQuizz.ShowText = false;
            this.gbQuizz.Size = new System.Drawing.Size(326, 253);
            this.gbQuizz.TabIndex = 2;
            this.gbQuizz.TabStop = false;
            this.gbQuizz.Text = "Quizz";
            this.gbQuizz.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // lbQizzCaption
            // 
            this.lbQizzCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbQizzCaption.Enabled = false;
            this.lbQizzCaption.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbQizzCaption.Hint = "";
            this.lbQizzCaption.Location = new System.Drawing.Point(6, 10);
            this.lbQizzCaption.MaxLength = 32767;
            this.lbQizzCaption.Multiline = false;
            this.lbQizzCaption.Name = "lbQizzCaption";
            this.lbQizzCaption.PasswordChar = '\0';
            this.lbQizzCaption.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbQizzCaption.SelectedText = "";
            this.lbQizzCaption.SelectionLength = 0;
            this.lbQizzCaption.SelectionStart = 0;
            this.lbQizzCaption.Size = new System.Drawing.Size(314, 38);
            this.lbQizzCaption.TabIndex = 1;
            this.lbQizzCaption.TabStop = false;
            this.lbQizzCaption.Text = "Quizzes";
            this.lbQizzCaption.UseSystemPasswordChar = false;
            // 
            // lbxQuizz
            // 
            this.lbxQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxQuizz.BackColor = System.Drawing.Color.White;
            this.lbxQuizz.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbxQuizz.FormattingEnabled = true;
            this.lbxQuizz.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbxQuizz.ItemHeight = 21;
            this.lbxQuizz.Location = new System.Drawing.Point(6, 52);
            this.lbxQuizz.Name = "lbxQuizz";
            this.lbxQuizz.Size = new System.Drawing.Size(314, 193);
            this.lbxQuizz.TabIndex = 0;
            this.lbxQuizz.SelectedValueChanged += new System.EventHandler(this.lbxQuizz_SelectedValueChanged);
            // 
            // btnAddQuizz
            // 
            this.btnAddQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddQuizz.ButtonType = CxFlatUI.ButtonType.Success;
            this.btnAddQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAddQuizz.Location = new System.Drawing.Point(224, 11);
            this.btnAddQuizz.Name = "btnAddQuizz";
            this.btnAddQuizz.Size = new System.Drawing.Size(93, 33);
            this.btnAddQuizz.TabIndex = 3;
            this.btnAddQuizz.Text = "Nouveau";
            this.btnAddQuizz.TextColor = System.Drawing.Color.White;
            this.btnAddQuizz.Click += new System.EventHandler(this.btnAddQuizz_Click);
            // 
            // btnEditQuizz
            // 
            this.btnEditQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnEditQuizz.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnEditQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnEditQuizz.Location = new System.Drawing.Point(117, 11);
            this.btnEditQuizz.Name = "btnEditQuizz";
            this.btnEditQuizz.Size = new System.Drawing.Size(93, 33);
            this.btnEditQuizz.TabIndex = 4;
            this.btnEditQuizz.Text = "Editer";
            this.btnEditQuizz.TextColor = System.Drawing.Color.White;
            this.btnEditQuizz.Click += new System.EventHandler(this.btnEditQuizz_Click);
            // 
            // btnDelQuizz
            // 
            this.btnDelQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelQuizz.ButtonType = CxFlatUI.ButtonType.Danger;
            this.btnDelQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelQuizz.Location = new System.Drawing.Point(6, 11);
            this.btnDelQuizz.Name = "btnDelQuizz";
            this.btnDelQuizz.Size = new System.Drawing.Size(93, 33);
            this.btnDelQuizz.TabIndex = 5;
            this.btnDelQuizz.Text = "Supprimer";
            this.btnDelQuizz.TextColor = System.Drawing.Color.White;
            this.btnDelQuizz.Click += new System.EventHandler(this.btnDelQuizz_Click);
            // 
            // gbActionsQuizz
            // 
            this.gbActionsQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbActionsQuizz.Controls.Add(this.btnDelQuizz);
            this.gbActionsQuizz.Controls.Add(this.btnAddQuizz);
            this.gbActionsQuizz.Controls.Add(this.btnEditQuizz);
            this.gbActionsQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbActionsQuizz.Location = new System.Drawing.Point(14, 364);
            this.gbActionsQuizz.MinimumSize = new System.Drawing.Size(325, 53);
            this.gbActionsQuizz.Name = "gbActionsQuizz";
            this.gbActionsQuizz.ShowText = false;
            this.gbActionsQuizz.Size = new System.Drawing.Size(325, 53);
            this.gbActionsQuizz.TabIndex = 6;
            this.gbActionsQuizz.TabStop = false;
            this.gbActionsQuizz.Text = "Action";
            this.gbActionsQuizz.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // gbUser
            // 
            this.gbUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbUser.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbUser.Controls.Add(this.btnRefreshUser);
            this.gbUser.Controls.Add(this.ibxArrowAddUser);
            this.gbUser.Controls.Add(this.ibxArrowDelUser);
            this.gbUser.Controls.Add(this.lb);
            this.gbUser.Controls.Add(this.lbxUser);
            this.gbUser.Controls.Add(this.lbxUserQuizz);
            this.gbUser.Controls.Add(this.lbUsersCaption);
            this.gbUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbUser.Location = new System.Drawing.Point(345, 76);
            this.gbUser.Name = "gbUser";
            this.gbUser.ShowText = false;
            this.gbUser.Size = new System.Drawing.Size(518, 253);
            this.gbUser.TabIndex = 7;
            this.gbUser.TabStop = false;
            this.gbUser.Text = "gbUsers";
            this.gbUser.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // btnRefreshUser
            // 
            this.btnRefreshUser.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnRefreshUser.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnRefreshUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnRefreshUser.Location = new System.Drawing.Point(9, 224);
            this.btnRefreshUser.Name = "btnRefreshUser";
            this.btnRefreshUser.Size = new System.Drawing.Size(165, 23);
            this.btnRefreshUser.TabIndex = 9;
            this.btnRefreshUser.Text = "Raffraichir";
            this.btnRefreshUser.TextColor = System.Drawing.Color.White;
            this.btnRefreshUser.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lb
            // 
            this.lb.Enabled = false;
            this.lb.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lb.Hint = "";
            this.lb.Location = new System.Drawing.Point(207, 10);
            this.lb.MaxLength = 32767;
            this.lb.Multiline = false;
            this.lb.Name = "lb";
            this.lb.PasswordChar = '\0';
            this.lb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lb.SelectedText = "";
            this.lb.SelectionLength = 0;
            this.lb.SelectionStart = 0;
            this.lb.Size = new System.Drawing.Size(305, 38);
            this.lb.TabIndex = 4;
            this.lb.TabStop = false;
            this.lb.Text = "Utilisateurs du quizz";
            this.lb.UseSystemPasswordChar = false;
            // 
            // lbxUser
            // 
            this.lbxUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbxUser.FormattingEnabled = true;
            this.lbxUser.ItemHeight = 21;
            this.lbxUser.Location = new System.Drawing.Point(9, 52);
            this.lbxUser.Name = "lbxUser";
            this.lbxUser.Size = new System.Drawing.Size(165, 151);
            this.lbxUser.TabIndex = 0;
            // 
            // lbxUserQuizz
            // 
            this.lbxUserQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxUserQuizz.FormattingEnabled = true;
            this.lbxUserQuizz.ItemHeight = 21;
            this.lbxUserQuizz.Location = new System.Drawing.Point(207, 52);
            this.lbxUserQuizz.Name = "lbxUserQuizz";
            this.lbxUserQuizz.Size = new System.Drawing.Size(305, 151);
            this.lbxUserQuizz.TabIndex = 1;
            // 
            // lbUsersCaption
            // 
            this.lbUsersCaption.Enabled = false;
            this.lbUsersCaption.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbUsersCaption.Hint = "";
            this.lbUsersCaption.Location = new System.Drawing.Point(9, 10);
            this.lbUsersCaption.MaxLength = 32767;
            this.lbUsersCaption.Multiline = false;
            this.lbUsersCaption.Name = "lbUsersCaption";
            this.lbUsersCaption.PasswordChar = '\0';
            this.lbUsersCaption.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbUsersCaption.SelectedText = "";
            this.lbUsersCaption.SelectionLength = 0;
            this.lbUsersCaption.SelectionStart = 0;
            this.lbUsersCaption.Size = new System.Drawing.Size(165, 38);
            this.lbUsersCaption.TabIndex = 3;
            this.lbUsersCaption.TabStop = false;
            this.lbUsersCaption.Text = "Tous les utilisateurs";
            this.lbUsersCaption.UseSystemPasswordChar = false;
            // 
            // gbActionsUser
            // 
            this.gbActionsUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gbActionsUser.Controls.Add(this.btnDelUser);
            this.gbActionsUser.Controls.Add(this.btnAddUser);
            this.gbActionsUser.Controls.Add(this.btnEditUser);
            this.gbActionsUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbActionsUser.Location = new System.Drawing.Point(538, 364);
            this.gbActionsUser.MinimumSize = new System.Drawing.Size(325, 53);
            this.gbActionsUser.Name = "gbActionsUser";
            this.gbActionsUser.ShowText = false;
            this.gbActionsUser.Size = new System.Drawing.Size(325, 53);
            this.gbActionsUser.TabIndex = 7;
            this.gbActionsUser.TabStop = false;
            this.gbActionsUser.Text = "Action";
            this.gbActionsUser.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // btnDelUser
            // 
            this.btnDelUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelUser.ButtonType = CxFlatUI.ButtonType.Danger;
            this.btnDelUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelUser.Location = new System.Drawing.Point(6, 11);
            this.btnDelUser.Name = "btnDelUser";
            this.btnDelUser.Size = new System.Drawing.Size(93, 33);
            this.btnDelUser.TabIndex = 5;
            this.btnDelUser.Text = "Supprimer";
            this.btnDelUser.TextColor = System.Drawing.Color.White;
            this.btnDelUser.Click += new System.EventHandler(this.btnDelUser_Click);
            // 
            // btnAddUser
            // 
            this.btnAddUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddUser.ButtonType = CxFlatUI.ButtonType.Success;
            this.btnAddUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAddUser.Location = new System.Drawing.Point(224, 11);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(93, 33);
            this.btnAddUser.TabIndex = 3;
            this.btnAddUser.Text = "Nouveau";
            this.btnAddUser.TextColor = System.Drawing.Color.White;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // btnEditUser
            // 
            this.btnEditUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnEditUser.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnEditUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnEditUser.Location = new System.Drawing.Point(113, 11);
            this.btnEditUser.Name = "btnEditUser";
            this.btnEditUser.Size = new System.Drawing.Size(93, 33);
            this.btnEditUser.TabIndex = 4;
            this.btnEditUser.Text = "Editer";
            this.btnEditUser.TextColor = System.Drawing.Color.White;
            this.btnEditUser.Click += new System.EventHandler(this.btnEditUser_Click);
            // 
            // lbInfoConnectedUser
            // 
            this.lbInfoConnectedUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoConnectedUser.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoConnectedUser.Enabled = false;
            this.lbInfoConnectedUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoConnectedUser.Location = new System.Drawing.Point(552, 47);
            this.lbInfoConnectedUser.Name = "lbInfoConnectedUser";
            this.lbInfoConnectedUser.Size = new System.Drawing.Size(311, 23);
            this.lbInfoConnectedUser.TabIndex = 8;
            this.lbInfoConnectedUser.Text = "Info utilisateur";
            this.lbInfoConnectedUser.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoQuizzButton
            // 
            this.lbInfoQuizzButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbInfoQuizzButton.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoQuizzButton.Enabled = false;
            this.lbInfoQuizzButton.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoQuizzButton.Location = new System.Drawing.Point(14, 335);
            this.lbInfoQuizzButton.Name = "lbInfoQuizzButton";
            this.lbInfoQuizzButton.Size = new System.Drawing.Size(325, 23);
            this.lbInfoQuizzButton.TabIndex = 9;
            this.lbInfoQuizzButton.Text = "Gestion des quizz";
            this.lbInfoQuizzButton.TextColor = System.Drawing.Color.White;
            // 
            // lbInfoUserButton
            // 
            this.lbInfoUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfoUserButton.ButtonType = CxFlatUI.ButtonType.Primary;
            this.lbInfoUserButton.Enabled = false;
            this.lbInfoUserButton.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbInfoUserButton.Location = new System.Drawing.Point(538, 335);
            this.lbInfoUserButton.Name = "lbInfoUserButton";
            this.lbInfoUserButton.Size = new System.Drawing.Size(325, 23);
            this.lbInfoUserButton.TabIndex = 10;
            this.lbInfoUserButton.Text = "Gestion des utilisateurs";
            this.lbInfoUserButton.TextColor = System.Drawing.Color.White;
            // 
            // ibxArrowAddUser
            // 
            this.ibxArrowAddUser.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ibxArrowAddUser.Image = global::QuizzManager.Properties.Resources.right;
            this.ibxArrowAddUser.Location = new System.Drawing.Point(180, 80);
            this.ibxArrowAddUser.MaximumSize = new System.Drawing.Size(21, 51);
            this.ibxArrowAddUser.MinimumSize = new System.Drawing.Size(21, 51);
            this.ibxArrowAddUser.Name = "ibxArrowAddUser";
            this.ibxArrowAddUser.Size = new System.Drawing.Size(21, 51);
            this.ibxArrowAddUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ibxArrowAddUser.TabIndex = 6;
            this.ibxArrowAddUser.TabStop = false;
            this.ibxArrowAddUser.Click += new System.EventHandler(this.ibxArrowAddUser_Click);
            // 
            // ibxArrowDelUser
            // 
            this.ibxArrowDelUser.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ibxArrowDelUser.Image = global::QuizzManager.Properties.Resources.left;
            this.ibxArrowDelUser.Location = new System.Drawing.Point(180, 137);
            this.ibxArrowDelUser.MaximumSize = new System.Drawing.Size(21, 51);
            this.ibxArrowDelUser.MinimumSize = new System.Drawing.Size(21, 51);
            this.ibxArrowDelUser.Name = "ibxArrowDelUser";
            this.ibxArrowDelUser.Size = new System.Drawing.Size(21, 51);
            this.ibxArrowDelUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ibxArrowDelUser.TabIndex = 5;
            this.ibxArrowDelUser.TabStop = false;
            this.ibxArrowDelUser.Click += new System.EventHandler(this.ibxArrowDelUser_Click);
            // 
            // Accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 429);
            this.Controls.Add(this.lbInfoUserButton);
            this.Controls.Add(this.lbInfoQuizzButton);
            this.Controls.Add(this.lbInfoConnectedUser);
            this.Controls.Add(this.gbActionsUser);
            this.Controls.Add(this.gbUser);
            this.Controls.Add(this.gbActionsQuizz);
            this.Controls.Add(this.gbQuizz);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.sbAccueil);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(875, 429);
            this.Name = "Accueil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Acceuil";
            this.gbQuizz.ResumeLayout(false);
            this.gbActionsQuizz.ResumeLayout(false);
            this.gbUser.ResumeLayout(false);
            this.gbActionsUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ibxArrowAddUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibxArrowDelUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbAccueil;
        private CxFlatUI.CxFlatRoundButton btnLogOut;
        private CxFlatUI.CxFlatGroupBox gbQuizz;
        private System.Windows.Forms.ListBox lbxQuizz;
        private CxFlatUI.CxFlatSimpleButton btnAddQuizz;
        private CxFlatUI.CxFlatSimpleButton btnEditQuizz;
        private CxFlatUI.CxFlatSimpleButton btnDelQuizz;
        private CxFlatUI.CxFlatGroupBox gbActionsQuizz;
        private CxFlatUI.CxFlatGroupBox gbUser;
        private CxFlatUI.CxFlatGroupBox gbActionsUser;
        private CxFlatUI.CxFlatSimpleButton btnDelUser;
        private CxFlatUI.CxFlatSimpleButton btnAddUser;
        private CxFlatUI.CxFlatSimpleButton btnEditUser;
        private CxFlatUI.CxFlatTextBox lbQizzCaption;
        private System.Windows.Forms.ListBox lbxUser;
        private CxFlatUI.CxFlatTextBox lb;
        private CxFlatUI.CxFlatTextBox lbUsersCaption;
        private System.Windows.Forms.ListBox lbxUserQuizz;
        private CxFlatUI.CxFlatPictureBox ibxArrowAddUser;
        private CxFlatUI.CxFlatPictureBox ibxArrowDelUser;
        private CxFlatUI.Controls.CxFlatButton lbInfoConnectedUser;
        private CxFlatUI.CxFlatRoundButton btnRefreshUser;
        private CxFlatUI.Controls.CxFlatButton lbInfoQuizzButton;
        private CxFlatUI.Controls.CxFlatButton lbInfoUserButton;
    }
}