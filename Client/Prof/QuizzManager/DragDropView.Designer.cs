﻿namespace QuizzManager {
    partial class DragDropView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DragDropView));
            this.sbViewDragDrop = new CxFlatUI.CxFlatStatusBar();
            this.btnExit = new CxFlatUI.CxFlatRoundButton();
            this.flpQuestion = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCreateQuestion = new CxFlatUI.Controls.CxFlatButton();
            this.tbxQuestion = new CxFlatUI.CxFlatTextBox();
            this.tbxResponse = new CxFlatUI.CxFlatTextBox();
            this.btnCreateResponse = new CxFlatUI.Controls.CxFlatButton();
            this.flpReponse = new System.Windows.Forms.FlowLayoutPanel();
            this.btnDelete = new CxFlatUI.Controls.CxFlatButton();
            this.btnValide = new CxFlatUI.CxFlatSimpleButton();
            this.lbQuestionText = new CxFlatUI.CxFlatTextBox();
            this.abxInfoQuestion = new CxFlatUI.CxFlatAlertBox();
            this.albxInfoColumnReponse = new CxFlatUI.CxFlatAlertBox();
            this.SuspendLayout();
            // 
            // sbViewDragDrop
            // 
            this.sbViewDragDrop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewDragDrop.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewDragDrop.Location = new System.Drawing.Point(0, 0);
            this.sbViewDragDrop.Name = "sbViewDragDrop";
            this.sbViewDragDrop.Size = new System.Drawing.Size(1055, 40);
            this.sbViewDragDrop.TabIndex = 0;
            this.sbViewDragDrop.Text = "Drag and drop";
            this.sbViewDragDrop.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnExit
            // 
            this.btnExit.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnExit.Location = new System.Drawing.Point(12, 46);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 23);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "Retour";
            this.btnExit.TextColor = System.Drawing.Color.White;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // flpQuestion
            // 
            this.flpQuestion.AllowDrop = true;
            this.flpQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flpQuestion.AutoScroll = true;
            this.flpQuestion.BackColor = System.Drawing.Color.White;
            this.flpQuestion.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuestion.Location = new System.Drawing.Point(12, 235);
            this.flpQuestion.Name = "flpQuestion";
            this.flpQuestion.Size = new System.Drawing.Size(435, 438);
            this.flpQuestion.TabIndex = 12;
            this.flpQuestion.WrapContents = false;
            this.flpQuestion.Click += new System.EventHandler(this.flpQuestion_Click);
            // 
            // btnCreateQuestion
            // 
            this.btnCreateQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreateQuestion.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnCreateQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnCreateQuestion.Location = new System.Drawing.Point(372, 679);
            this.btnCreateQuestion.Name = "btnCreateQuestion";
            this.btnCreateQuestion.Size = new System.Drawing.Size(75, 38);
            this.btnCreateQuestion.TabIndex = 14;
            this.btnCreateQuestion.Text = "Créer";
            this.btnCreateQuestion.TextColor = System.Drawing.Color.White;
            this.btnCreateQuestion.Click += new System.EventHandler(this.btnCreateQuestion_Click);
            // 
            // tbxQuestion
            // 
            this.tbxQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxQuestion.Hint = "Choix";
            this.tbxQuestion.Location = new System.Drawing.Point(12, 679);
            this.tbxQuestion.MaxLength = 32767;
            this.tbxQuestion.Multiline = false;
            this.tbxQuestion.Name = "tbxQuestion";
            this.tbxQuestion.PasswordChar = '\0';
            this.tbxQuestion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxQuestion.SelectedText = "";
            this.tbxQuestion.SelectionLength = 0;
            this.tbxQuestion.SelectionStart = 0;
            this.tbxQuestion.Size = new System.Drawing.Size(354, 38);
            this.tbxQuestion.TabIndex = 15;
            this.tbxQuestion.TabStop = false;
            this.tbxQuestion.UseSystemPasswordChar = false;
            this.tbxQuestion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxQuestion_KeyDown);
            // 
            // tbxResponse
            // 
            this.tbxResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxResponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxResponse.Hint = "Réponse";
            this.tbxResponse.Location = new System.Drawing.Point(453, 679);
            this.tbxResponse.MaxLength = 32767;
            this.tbxResponse.Multiline = false;
            this.tbxResponse.Name = "tbxResponse";
            this.tbxResponse.PasswordChar = '\0';
            this.tbxResponse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxResponse.SelectedText = "";
            this.tbxResponse.SelectionLength = 0;
            this.tbxResponse.SelectionStart = 0;
            this.tbxResponse.Size = new System.Drawing.Size(364, 38);
            this.tbxResponse.TabIndex = 17;
            this.tbxResponse.TabStop = false;
            this.tbxResponse.UseSystemPasswordChar = false;
            this.tbxResponse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxResponse_KeyDown);
            // 
            // btnCreateResponse
            // 
            this.btnCreateResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreateResponse.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnCreateResponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnCreateResponse.Location = new System.Drawing.Point(823, 679);
            this.btnCreateResponse.Name = "btnCreateResponse";
            this.btnCreateResponse.Size = new System.Drawing.Size(75, 38);
            this.btnCreateResponse.TabIndex = 16;
            this.btnCreateResponse.Text = "Créer";
            this.btnCreateResponse.TextColor = System.Drawing.Color.White;
            this.btnCreateResponse.Click += new System.EventHandler(this.btnCreateResponse_Click);
            // 
            // flpReponse
            // 
            this.flpReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flpReponse.AutoScroll = true;
            this.flpReponse.BackColor = System.Drawing.Color.White;
            this.flpReponse.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpReponse.Location = new System.Drawing.Point(453, 235);
            this.flpReponse.Name = "flpReponse";
            this.flpReponse.Size = new System.Drawing.Size(445, 438);
            this.flpReponse.TabIndex = 18;
            this.flpReponse.WrapContents = false;
            this.flpReponse.Click += new System.EventHandler(this.flpReponse_Click_1);
            // 
            // btnDelete
            // 
            this.btnDelete.ButtonType = CxFlatUI.ButtonType.Danger;
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(921, 276);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(122, 120);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "Supprimer les éléments sélectionnés";
            this.btnDelete.TextColor = System.Drawing.Color.White;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnValide
            // 
            this.btnValide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValide.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnValide.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnValide.Location = new System.Drawing.Point(937, 679);
            this.btnValide.Name = "btnValide";
            this.btnValide.Size = new System.Drawing.Size(106, 38);
            this.btnValide.TabIndex = 20;
            this.btnValide.Text = "valider";
            this.btnValide.TextColor = System.Drawing.Color.White;
            this.btnValide.Click += new System.EventHandler(this.btnValide_Click_1);
            // 
            // lbQuestionText
            // 
            this.lbQuestionText.Enabled = false;
            this.lbQuestionText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbQuestionText.Hint = "";
            this.lbQuestionText.Location = new System.Drawing.Point(12, 98);
            this.lbQuestionText.MaxLength = 32767;
            this.lbQuestionText.Multiline = true;
            this.lbQuestionText.Name = "lbQuestionText";
            this.lbQuestionText.PasswordChar = '\0';
            this.lbQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbQuestionText.SelectedText = "";
            this.lbQuestionText.SelectionLength = 0;
            this.lbQuestionText.SelectionStart = 0;
            this.lbQuestionText.Size = new System.Drawing.Size(886, 91);
            this.lbQuestionText.TabIndex = 21;
            this.lbQuestionText.TabStop = false;
            this.lbQuestionText.Text = "Question";
            this.lbQuestionText.UseSystemPasswordChar = false;
            // 
            // abxInfoQuestion
            // 
            this.abxInfoQuestion.Enabled = false;
            this.abxInfoQuestion.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.abxInfoQuestion.Location = new System.Drawing.Point(12, 195);
            this.abxInfoQuestion.Name = "abxInfoQuestion";
            this.abxInfoQuestion.Size = new System.Drawing.Size(435, 34);
            this.abxInfoQuestion.TabIndex = 22;
            this.abxInfoQuestion.Text = "Ajouter les questions dans cette listes";
            this.abxInfoQuestion.Type = CxFlatUI.CxFlatAlertBox.AlertType.Warning;
            // 
            // albxInfoColumnReponse
            // 
            this.albxInfoColumnReponse.Enabled = false;
            this.albxInfoColumnReponse.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.albxInfoColumnReponse.Location = new System.Drawing.Point(453, 195);
            this.albxInfoColumnReponse.Name = "albxInfoColumnReponse";
            this.albxInfoColumnReponse.Size = new System.Drawing.Size(445, 34);
            this.albxInfoColumnReponse.TabIndex = 23;
            this.albxInfoColumnReponse.Text = "Puis placer les dans les réponses correspondantes";
            this.albxInfoColumnReponse.Type = CxFlatUI.CxFlatAlertBox.AlertType.Warning;
            // 
            // DragDropView
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1055, 738);
            this.Controls.Add(this.albxInfoColumnReponse);
            this.Controls.Add(this.abxInfoQuestion);
            this.Controls.Add(this.lbQuestionText);
            this.Controls.Add(this.btnValide);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.flpReponse);
            this.Controls.Add(this.tbxResponse);
            this.Controls.Add(this.btnCreateResponse);
            this.Controls.Add(this.tbxQuestion);
            this.Controls.Add(this.btnCreateQuestion);
            this.Controls.Add(this.flpQuestion);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.sbViewDragDrop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(1055, 500);
            this.Name = "DragDropView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DragDrop";
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewDragDrop;
        private CxFlatUI.CxFlatRoundButton btnExit;
        private System.Windows.Forms.FlowLayoutPanel flpQuestion;
        private CxFlatUI.Controls.CxFlatButton btnCreateQuestion;
        private CxFlatUI.CxFlatTextBox tbxQuestion;
        private CxFlatUI.CxFlatTextBox tbxResponse;
        private CxFlatUI.Controls.CxFlatButton btnCreateResponse;
        private System.Windows.Forms.FlowLayoutPanel flpReponse;
        private CxFlatUI.Controls.CxFlatButton btnDelete;
        private CxFlatUI.CxFlatSimpleButton btnValide;
        private CxFlatUI.CxFlatTextBox lbQuestionText;
        private CxFlatUI.CxFlatAlertBox abxInfoQuestion;
        private CxFlatUI.CxFlatAlertBox albxInfoColumnReponse;
    }
}