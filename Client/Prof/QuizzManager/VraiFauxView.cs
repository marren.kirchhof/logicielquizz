﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QuizzManager.Constantes.UI;

namespace QuizzManager {
    public partial class VraiFauxView : Form {
        private User CurrentUser = null;
        private Question CurrentQuestion = null;
        private Quizz CurrentQuizz = null;
        private QuestionType CurrentType = null;
        private string ActionT;

        private delegate void ActionAfterGetVraiFaux();

        #region Constructeur
        /// <summary>
        /// Edit construcotr
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="CurrentQuizz"></param>
        /// <param name="QuestionToEdit"></param>
        /// <param name="ActionType"></param>
        /// <param name="QT"></param>
        public VraiFauxView(User ConnectedUser, Quizz CurrentQuizz, Question QuestionToEdit,string ActionType,QuestionType QT) {
            InitializeComponent();
            CurrentType = QT;
            this.CurrentQuizz = CurrentQuizz;
            ActionT = ActionType;
            CurrentUser = ConnectedUser;
            CurrentQuestion = QuestionToEdit;
            SetUI(ActionT);
            setParamsResizeForm();
            SetDataVraiFaux();
            tbxQuestion.Text = CurrentQuestion.title;
        }
        /// <summary>
        /// Add constructor
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="CurrentQuizz"></param>
        /// <param name="title"></param>
        /// <param name="ActionType"></param>
        /// <param name="QT"></param>
        public VraiFauxView(User ConnectedUser, Quizz CurrentQuizz, string title, string ActionType, QuestionType QT) {
            InitializeComponent();
            CurrentType = QT;
            this.CurrentQuizz = CurrentQuizz;
            ActionT = ActionType;
            CurrentUser = ConnectedUser;
            SetUI(ActionT);
            setParamsResizeForm();
            tbxQuestion.Text = title;
        }

        #endregion
        #region UI
        #region SetUiAdd/Edit
        /// <summary>
        /// set the style of the interfaec
        /// </summary>
        /// <param name="ActionType"></param>
        private void SetUI(string ActionType ) {
            switch (ActionType) {
                case Constantes.TypeAction.Add:
                    UiAdd();
                    break;
                case Constantes.TypeAction.Edit:
                    UiEdit();
                    break;
            }
           
        }
        /// <summary>
        /// Interface style add
        /// </summary>
        private void UiAdd() {
            sbViewVraiFaux.Text = Constantes.TitleForm.ViewQuestionAdd;
            ActionT = Constantes.TypeAction.Add;
            btnAction.ButtonType = CxFlatUI.ButtonType.Success;
        }
        /// <summary>
        /// Interface style edit
        /// </summary>
        private void UiEdit() {
            sbViewVraiFaux.Text = Constantes.TitleForm.ViewQuestionEdit;
            ActionT = Constantes.TypeAction.Edit;
            btnAction.ButtonType = CxFlatUI.ButtonType.Primary;
        }
        #endregion
        #region resize form
        private void setParamsResizeForm() {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }




        #endregion

        /// <summary>
        /// Set the data and the interface of the current true false
        /// </summary>
        private async void SetDataVraiFaux() {
            try {
                Task<Question> QuestionAdded = HttpRequest.QuestionRequest.GetOne(CurrentQuestion.idQuestion);
                Question q = await QuestionAdded;
                VraiFaux CurrentVraiFaux = DeserializeJSON(q.correction);
                tbxFalse.Text = CurrentVraiFaux.ResponseFalse;
                tbxTrue.Text = CurrentVraiFaux.ResponseTrue;
                rbTrue.Checked = CurrentVraiFaux.ResponseCorrection;
                rbFalse.Checked = !CurrentVraiFaux.ResponseCorrection;
            } catch {
                MessageBox.Show(Constantes.AlertMessage.EmptyFields, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Create or edit the question true false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #endregion
        private void btnAction_Click(object sender, EventArgs e) {

            if (InputQuizzValide()) {
                switch (ActionT) {
                    case Constantes.TypeAction.Add:
                        CreateQuestion();
                        this.Close();
                        break;
                    case Constantes.TypeAction.Edit:
                        EditQuestion();
                        this.Close();
                        break;
                }
            } else {
                MessageBox.Show(Constantes.AlertMessage.EmptyFields, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Create a new question Qcm in the database
        /// </summary>
        private async void CreateQuestion() {
            try {
                Task<Question> QuestionAdded = HttpRequest.QuestionRequest.Create(tbxQuestion.Text, CurrentQuizz.idQuizz, CurrentType.idType, GetJsonStringResponse());
                Question q = await QuestionAdded;
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Edit the question in the database
        /// </summary>
        private async void EditQuestion() {
            try {
                Task<bool> QuestionUpdated = HttpRequest.QuestionRequest.Update(CurrentQuestion.idQuestion,tbxQuestion.Text, CurrentQuizz.idQuizz, CurrentType.idType, GetJsonStringResponse());
                bool resp = await QuestionUpdated;
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        #region check 
        /// <summary>
        /// Check input
        /// </summary>
        /// <returns></returns>
        private bool InputQuizzValide() {
            return ((rbFalse.Checked || rbTrue.Checked) && (tbxFalse.Text.Trim() != "" && tbxTrue.Text.Trim() != ""));
        }

        /// <summary>
        /// get the json string of a true false object
        /// </summary>
        /// <returns>json string of the current true false</returns>
        private string GetJsonStringResponse() {
            VraiFaux R = new VraiFaux(rbTrue.Checked, tbxTrue.Text, tbxFalse.Text);
            return Newtonsoft.Json.JsonConvert.SerializeObject(R);
        }
        /// <summary>
        /// Convert a json string to a Truefalse
        /// </summary>
        /// <param name="JsonString">The truefalse json string</param>
        /// <returns>The object truefalse</returns>
        private VraiFaux DeserializeJSON(string JsonString) {
           return Newtonsoft.Json.JsonConvert.DeserializeObject<VraiFaux>(JsonString);
        }

        #endregion

        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
