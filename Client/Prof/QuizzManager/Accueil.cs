﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QuizzManager.Constantes.UI;

namespace QuizzManager {
    public partial class Accueil : Form {
        private User ConnectedUser = null;
        public Accueil(User U) {
            ConnectedUser = U;
            InitializeComponent();
            lbxUser.DisplayMember = Constantes.DisplayListbox.ListboxUsers;
            lbxUserQuizz.DisplayMember = Constantes.DisplayListbox.ListboxUsers;
            lbxQuizz.DisplayMember = Constantes.DisplayListbox.ListboxQuizzes;
            this.DoubleBuffered = true;                          //  Necessary for resize when you don't have border --> Border resize
            this.SetStyle(ControlStyles.ResizeRedraw, true);     //  Border resize
            sbAccueil.Text = Constantes.TitleForm.ViewMain;
            lbInfoConnectedUser.Text = ConnectedUser.username;
            RefreshListQuizz();
            RefreshListUser();
        }
        #region Resize form

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);

        }
        #endregion

        /// <summary>
        /// Refresh all the component in the interface
        /// </summary>
        #region refresh
        private void RefreshAll() {
            RefreshListQuizz();
            RefreshListUser();
        }
        /// <summary>
        /// Refresh the list of all quizz
        /// </summary>
        private async void RefreshListQuizz() {
            try {
                Task<List<Quizz>> lUser = HttpRequest.QuizzesRequest.GetQuizzesWhoUserIsOwner(ConnectedUser.idUser);
                List<Quizz> ltemp = await lUser;
                ConnectedUser.quizzes_owned = ltemp;

                if (ConnectedUser.quizzes_owned != null) {
                    lbxQuizz.Items.Clear();
                    foreach (Quizz q in ConnectedUser.quizzes_owned) {
                        lbxQuizz.Items.Add(q);

                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// refresh the list of all quizz linked to the user
        /// </summary>
        private async void RefreshListUser() {
            try {
                Task<List<User>> ListUser = HttpRequest.UsersRequest.GetAll();
                List<User> AllUser = await ListUser;

                lbxUser.Items.Clear();
                foreach (User u in AllUser) {
                    lbxUser.Items.Add(u);
                }

            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #endregion

        #region Button
        /// <summary>
        /// Show the form ViewUser with the contructor add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddUser_Click(object sender, EventArgs e) {
            ViewUser AddUser = new ViewUser();
            this.Visible = false;
            AddUser.ShowDialog();
            RefreshListUser();
            this.Visible = true;
        }
        /// <summary>
        /// Show the form ViewUser with the contructor edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditUser_Click(object sender, EventArgs e) {
            if (lbxUser.SelectedItem != null) {
                ViewUser Edituser = new ViewUser(((User)lbxUser.SelectedItem));
                this.Visible = false;
                Edituser.ShowDialog();
                RefreshListUser();
                this.Visible = true;
            }
        }

        /// <summary>
        /// Delete a user to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnDelUser_Click(object sender, EventArgs e) {
            try {
                if (lbxUser.SelectedItem != null && ConnectedUser.idUser != ((User)lbxUser.SelectedItem).idUser) {
                    if (MessageBox.Show(string.Format(Constantes.InfoMessage.ViewAccueil.ConfirmationDeleteUser, ((User)lbxUser.SelectedItem).username), "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                        Task<bool> DelUser = HttpRequest.UsersRequest.Delete(((User)lbxUser.SelectedItem).idUser);
                        bool AllUser = await DelUser;
                        lbxUser.Items.Remove(lbxUser.SelectedItem);
                    }
                }

            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// when the selected value in lbx quizz, refresh the list of user link to the quizz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void lbxQuizz_SelectedValueChanged(object sender, EventArgs e) {
            if (lbxQuizz.SelectedItem != null) {
                try {
                    Task<List<User>> ListUser = HttpRequest.UsersRequest.GetAllUserLinkedToQuizz(((Quizz)lbxQuizz.SelectedItem).idQuizz);
                    List<User> AllUser = await ListUser;
                    lbxUserQuizz.Items.Clear();
                    if (AllUser != null) {
                        foreach (User u in AllUser) {
                            lbxUserQuizz.Items.Add(u);
                        }
                    }
                } catch (Exception ex) { MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            }
        }

        /// <summary>
        /// Link a user to a quizz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ibxArrowAddUser_Click(object sender, EventArgs e) {

            if (lbxUser.SelectedItem != null && lbxQuizz.SelectedItem != null && !UserQuizzAlreadyExist((User)lbxUser.SelectedItem, lbxUserQuizz)) {
                try {

                    User U = (User)lbxUser.SelectedItem;
                    Quizz Q = (Quizz)lbxQuizz.SelectedItem;
                    Task<bool> R = HttpRequest.UsersRequest.LinkToQuizz(U.idUser, Q.idQuizz);
                    bool OperationSuccess = await R;
                    if (OperationSuccess) {
                        lbxUserQuizz.Items.Add(U);
                    }
                } catch (Exception ex) {
                    MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Detach a user linked to a quizz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ibxArrowDelUser_Click(object sender, EventArgs e) {
            if (lbxUserQuizz.SelectedItem != null && lbxQuizz.SelectedItem != null) {
                try {
                    User U = (User)lbxUserQuizz.SelectedItem;
                    Quizz Q = (Quizz)lbxQuizz.SelectedItem;
                    Task<bool> R = HttpRequest.UsersRequest.DetachToQuizz(U.idUser, Q.idQuizz);
                    bool OperationSuccess = await R;
                    if (OperationSuccess) {
                        lbxUserQuizz.Items.Remove(U);
                    }
                } catch (Exception ex) {
                    MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Delete a quizz in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnDelQuizz_Click(object sender, EventArgs e) {
            if (lbxQuizz.SelectedItem != null) {
                try {
                    Quizz Q = (Quizz)lbxQuizz.SelectedItem;
                    if (MessageBox.Show(string.Format(Constantes.InfoMessage.ViewAccueil.ConfirmationDeleteQuizz, Q.name), "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                        Task<bool> R = HttpRequest.QuizzesRequest.Delete(Q.idQuizz);
                        bool LResultat = await R;
                        if (LResultat) {
                            Task<bool> bDeleteSuccess = HttpRequest.ResponseRequest.DeleteAllLinkedToQuizz(((Quizz)lbxQuizz.SelectedItem).idQuizz);
                            bool bResultat = await bDeleteSuccess;
                            lbxQuizz.Items.Remove(Q);
                        }
                    }
                } catch (Exception ex) {
                    MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Edit a quizz in the database , show the interface QuizzView with the constructor edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditQuizz_Click(object sender, EventArgs e) {
            if ((Quizz)lbxQuizz.SelectedItem != null) {
                QuizzView Q = new QuizzView(ConnectedUser, (Quizz)lbxQuizz.SelectedItem);
                Visible = false;
                Q.ShowDialog();
                Visible = true;
                RefreshAll();
            }
        }

        /// <summary>
        /// Add a quizz in the database , show the interface QuizzView with the constructor add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddQuizz_Click(object sender, EventArgs e) {
            QuizzView Q = new QuizzView(ConnectedUser);
            this.Visible = false;
            Q.ShowDialog();
            this.Visible = true;
            RefreshAll();
        }

        #endregion

        /// <summary>
        /// Check if the user is already link to the quizz
        /// </summary>
        /// <param name="U"></param>
        /// <param name="lU"></param>
        /// <returns></returns>
        private bool UserQuizzAlreadyExist(User U, ListBox lU) {
            bool bR = false;
            foreach (User iU in lU.Items) {
                if (iU.idUser == U.idUser) { bR = true; }
            }
            return bR;
        }

        private void btnLogOut_Click(object sender, EventArgs e) {
            this.Close();
        }
        /// <summary>
        /// refresh the list of all user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e) {
            RefreshListUser();
        }
    }
}
        


    

