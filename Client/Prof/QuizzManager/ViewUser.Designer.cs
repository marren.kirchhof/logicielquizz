﻿namespace QuizzManager
{
    partial class ViewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewUser));
            this.sbViewUser = new CxFlatUI.CxFlatStatusBar();
            this.btnLogOut = new CxFlatUI.CxFlatRoundButton();
            this.gbxInfoUser = new CxFlatUI.CxFlatGroupBox();
            this.cbxRole = new CxFlatUI.CxFlatComboBox();
            this.tbxConfirmPassword = new CxFlatUI.CxFlatTextBox();
            this.tbxPassword = new CxFlatUI.CxFlatTextBox();
            this.tbxNom = new CxFlatUI.CxFlatTextBox();
            this.btnAction = new CxFlatUI.CxFlatSimpleButton();
            this.gbxInfoUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbViewUser
            // 
            this.sbViewUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewUser.Location = new System.Drawing.Point(0, 0);
            this.sbViewUser.Name = "sbViewUser";
            this.sbViewUser.Size = new System.Drawing.Size(800, 40);
            this.sbViewUser.TabIndex = 0;
            this.sbViewUser.Text = "sbViewUser";
            this.sbViewUser.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // btnLogOut
            // 
            this.btnLogOut.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnLogOut.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnLogOut.Location = new System.Drawing.Point(12, 46);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(127, 23);
            this.btnLogOut.TabIndex = 2;
            this.btnLogOut.Text = "retour";
            this.btnLogOut.TextColor = System.Drawing.Color.White;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // gbxInfoUser
            // 
            this.gbxInfoUser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxInfoUser.Controls.Add(this.cbxRole);
            this.gbxInfoUser.Controls.Add(this.tbxConfirmPassword);
            this.gbxInfoUser.Controls.Add(this.tbxPassword);
            this.gbxInfoUser.Controls.Add(this.tbxNom);
            this.gbxInfoUser.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gbxInfoUser.Location = new System.Drawing.Point(12, 75);
            this.gbxInfoUser.Name = "gbxInfoUser";
            this.gbxInfoUser.ShowText = false;
            this.gbxInfoUser.Size = new System.Drawing.Size(579, 180);
            this.gbxInfoUser.TabIndex = 3;
            this.gbxInfoUser.TabStop = false;
            this.gbxInfoUser.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // cbxRole
            // 
            this.cbxRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxRole.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbxRole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxRole.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.cbxRole.FormattingEnabled = true;
            this.cbxRole.ItemHeight = 30;
            this.cbxRole.Location = new System.Drawing.Point(442, 28);
            this.cbxRole.Name = "cbxRole";
            this.cbxRole.Size = new System.Drawing.Size(131, 36);
            this.cbxRole.TabIndex = 3;
            // 
            // tbxConfirmPassword
            // 
            this.tbxConfirmPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxConfirmPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxConfirmPassword.Hint = "Confirmation du mot de passe";
            this.tbxConfirmPassword.Location = new System.Drawing.Point(6, 136);
            this.tbxConfirmPassword.MaxLength = 32767;
            this.tbxConfirmPassword.Multiline = false;
            this.tbxConfirmPassword.Name = "tbxConfirmPassword";
            this.tbxConfirmPassword.PasswordChar = '*';
            this.tbxConfirmPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxConfirmPassword.SelectedText = "";
            this.tbxConfirmPassword.SelectionLength = 0;
            this.tbxConfirmPassword.SelectionStart = 0;
            this.tbxConfirmPassword.Size = new System.Drawing.Size(430, 38);
            this.tbxConfirmPassword.TabIndex = 2;
            this.tbxConfirmPassword.TabStop = false;
            this.tbxConfirmPassword.UseSystemPasswordChar = false;
            // 
            // tbxPassword
            // 
            this.tbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPassword.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxPassword.Hint = "Mot de passe";
            this.tbxPassword.Location = new System.Drawing.Point(6, 92);
            this.tbxPassword.MaxLength = 32767;
            this.tbxPassword.Multiline = false;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.PasswordChar = '*';
            this.tbxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxPassword.SelectedText = "";
            this.tbxPassword.SelectionLength = 0;
            this.tbxPassword.SelectionStart = 0;
            this.tbxPassword.Size = new System.Drawing.Size(430, 38);
            this.tbxPassword.TabIndex = 1;
            this.tbxPassword.TabStop = false;
            this.tbxPassword.UseSystemPasswordChar = false;
            // 
            // tbxNom
            // 
            this.tbxNom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxNom.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxNom.Hint = "Nom d\'utilisateur";
            this.tbxNom.Location = new System.Drawing.Point(6, 28);
            this.tbxNom.MaxLength = 32767;
            this.tbxNom.Multiline = false;
            this.tbxNom.Name = "tbxNom";
            this.tbxNom.PasswordChar = '\0';
            this.tbxNom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxNom.SelectedText = "";
            this.tbxNom.SelectionLength = 0;
            this.tbxNom.SelectionStart = 0;
            this.tbxNom.Size = new System.Drawing.Size(430, 38);
            this.tbxNom.TabIndex = 0;
            this.tbxNom.TabStop = false;
            this.tbxNom.UseSystemPasswordChar = false;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.ButtonType = CxFlatUI.ButtonType.Default;
            this.btnAction.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAction.Location = new System.Drawing.Point(597, 212);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(191, 38);
            this.btnAction.TabIndex = 4;
            this.btnAction.Text = "Ajouter/Editer";
            this.btnAction.TextColor = System.Drawing.Color.White;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // ViewUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 267);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.gbxInfoUser);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.sbViewUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(800, 270);
            this.MinimumSize = new System.Drawing.Size(348, 246);
            this.Name = "ViewUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ViewUser";
            this.gbxInfoUser.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewUser;
        private CxFlatUI.CxFlatRoundButton btnLogOut;
        private CxFlatUI.CxFlatGroupBox gbxInfoUser;
        private CxFlatUI.CxFlatTextBox tbxConfirmPassword;
        private CxFlatUI.CxFlatTextBox tbxPassword;
        private CxFlatUI.CxFlatTextBox tbxNom;
        private CxFlatUI.CxFlatSimpleButton btnAction;
        private CxFlatUI.CxFlatComboBox cbxRole;
    }
}