﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QuizzManager.Constantes.UI;

namespace QuizzManager {
    public partial class QuizzView : Form {
        private Quizz CurrentQuizz = null;
        private User CurrentUser = null;
        private string ActionType;

        private bool AlreadySaveQuizz = false;

        private delegate void FunctionToCallAfterCreate();

        #region Loading
        private void QuizzView_Load(object sender, EventArgs e) {
            setParamsResizeForm();
            lbxQuestionsQuizz.DisplayMember = Constantes.DisplayListbox.ListboxQuestion;
        }
        #endregion

        /// <summary>
        /// constructor for add 
        /// </summary>
        /// <param name="ConnectedUser"></param>
        #region constructors
        public QuizzView(User ConnectedUser) {
            InitializeComponent();
            CurrentUser = ConnectedUser;
            ActionType = Constantes.TypeAction.Add;
            UiAdd();
            PrepareInputAdd();
        }
        /// <summary>
        /// Constructor for edit
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="QuizzToEdit"></param>
        public QuizzView(User ConnectedUser, Quizz QuizzToEdit) {
            InitializeComponent();
            ActionType = Constantes.TypeAction.Edit;
            CurrentQuizz = QuizzToEdit;
            CurrentUser = ConnectedUser;
            UiEdit();
            FillInputWithCurrentQuizz();

        }
        #endregion

        #region UI
        /// <summary>
        /// Set the style interface 
        /// </summary>
        #region SetUiAdd/Edit
        private void UiAdd() {
            sbViewQuizz.Text = Constantes.TitleForm.ViewQuizzAdd;
            btnAction.ButtonType = CxFlatUI.ButtonType.Success;
            btnAction.Text = Constantes.btnTextType.Add;
            ActionType = Constantes.TypeAction.Add;
            SetCommon();
        }
        private void UiEdit() {
            sbViewQuizz.Text = Constantes.TitleForm.ViewQuizzEdit;
            btnAction.ButtonType = CxFlatUI.ButtonType.Primary;
            btnAction.Text = Constantes.btnTextType.Edit;
            ActionType = Constantes.TypeAction.Edit;
            SetCommon();
        }

        /// <summary>
        /// Set the the commun item in UIedit and UIadd
        /// </summary>
        private void SetCommon() {
            lbxQuestionsQuizz.DisplayMember = Constantes.DisplayListbox.ListboxQuestion;
        }
        #endregion
        #region resize form
        private void setParamsResizeForm() {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }
        #endregion
        #region PrepareInputForAdd
        private void PrepareInputAdd() {
            tbxTitleQuizz.Text = "";
            dpDateQuizz.Value = DateTime.Today;
        }
        #endregion
        #endregion


        private void CloseForm() {
            try {
                this.Close();
            } catch { }

        }


        /// <summary>
        /// check if the user didn't save and if the input are correct
        /// </summary>
        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }
        private async void btnAction_Click(object sender, EventArgs e) {
            try {
                switch (ActionType) {
                    case Constantes.TypeAction.Add:
                        if (tbxTitleQuizz.Text.Trim() != "") {
                            if (!AlreadySaveQuizz) {
                                CreateQuizz(tbxTitleQuizz.Text, CurrentUser, dpDateQuizz.Value);
                            }
                            CloseForm();
                        }
                        break;
                    case Constantes.TypeAction.Edit:
                        if (CurrentQuizz != null) {
                            if (tbxTitleQuizz.Text.Trim() != "") {
                                Task<bool> UpdatedQuizz = HttpRequest.QuizzesRequest.Update(CurrentQuizz.idQuizz, tbxTitleQuizz.Text, CurrentUser.idUser, dpDateQuizz.Value);
                                bool resp = await UpdatedQuizz;
                                CloseForm();
                            }
                        } else { throw new Exception(); }
                        break;

                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Load data from the current quizz to the interface
        /// </summary>
        private async void FillInputWithCurrentQuizz() {
            try {
                tbxTitleQuizz.Text = CurrentQuizz.name;
                Task<List<Question>> lQuestions = HttpRequest.QuestionRequest.GetAll();
                List<Question> ltemp = await lQuestions;
                RefreshQuizzQuestion();
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Refresh the question list
        /// </summary>
        private async void RefreshQuizzQuestion() {
            try {
                if (CurrentQuizz != null) {
                    Task<List<Question>> lQuestions = HttpRequest.QuestionRequest.GetAllQuestionsLinkedToQuizz(CurrentQuizz.idQuizz);
                    List<Question> ltemp = await lQuestions;
                    lbxQuestionsQuizz.Items.Clear();
                    if (ltemp != null) {
                        foreach (Question question in ltemp) {
                            lbxQuestionsQuizz.Items.Add(question);
                        }
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Delete a question in the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnDelQuestion_Click(object sender, EventArgs e) {
            try {
                Task<bool> bUserresponse = ResponseAlreadyExist();
                bool Uresp = await bUserresponse;

                if (Uresp) {
                    Question qToDel = (Question)lbxQuestionsQuizz.SelectedItem;
                    if (qToDel != null) {
                        Task<bool> DelQuestion = HttpRequest.QuestionRequest.Delete(qToDel.idQuestion);
                        bool OperationSuccess = await DelQuestion;
                        if (OperationSuccess) {
                            lbxQuestionsQuizz.Items.Remove(qToDel);
                        }
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// edit a question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditQuestion_Click(object sender, EventArgs e) {
            if ((Question)lbxQuestionsQuizz.SelectedItem != null) {
                QuestionView Q = new QuestionView(CurrentUser, CurrentQuizz, (Question)lbxQuestionsQuizz.SelectedItem);
                ShowQuestionView(Q);
            }
        }

        /// <summary>
        /// Add a question, if other users already have answered this, propose to clear them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAddQuestion_Click(object sender, EventArgs e) {
            try {
                QuestionView Q = new QuestionView(CurrentUser, CurrentQuizz);
                    switch (ActionType) {
                        case Constantes.TypeAction.Add:
                            if (!AlreadySaveQuizz) {
                                if (MessageBox.Show(string.Format(Constantes.InfoMessage.ViewQuizz.ConfirmationAddQuizz), "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                                    if (tbxTitleQuizz.Text.Trim() != "") {
                                        AlreadySaveQuizz = true;
                                        Task<Quizz> NewQuizz = HttpRequest.QuizzesRequest.Create(tbxTitleQuizz.Text, CurrentUser.idUser, dpDateQuizz.Value);
                                        Quizz resp = await NewQuizz;
                                        CurrentQuizz = resp;
                                        Q = new QuestionView(CurrentUser, CurrentQuizz);
                                        ShowQuestionView(Q);
                                    } else {
                                        MessageBox.Show(Constantes.AlertMessage.EmptyFields, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            } else {
                                ShowQuestionView(Q);
                            }

                            break;
                        case Constantes.TypeAction.Edit:
                        Task<bool> bUserresponse = ResponseAlreadyExist();
                        bool Uresp = await bUserresponse;
                        if (Uresp) {
                            ShowQuestionView(Q);
                        }
                        break;
                        
                    }
                
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// check if response is link to the current quizz or not, and ask to the current user if he want to clear or not 
        /// </summary>
        /// <returns></returns>
        private async Task<bool>  ResponseAlreadyExist() {
            bool bContinue = false;
            if (CurrentQuizz != null) {
                Task<List<Response>> R = HttpRequest.ResponseRequest.GetAlllinkedToUserAndQuizz(CurrentQuizz.idQuizz);
                List<Response> LResultat = await R;
                if (LResultat.Count == 0) {
                    bContinue = true;
                } else {
                    if (MessageBox.Show(string.Format(Constantes.InfoMessage.ViewQuizz.ConfirmationResponseAlreadyExist), "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) == DialogResult.Yes) {
                        Task<bool> bDeleteSuccess = HttpRequest.ResponseRequest.DeleteAllLinkedToQuizz(CurrentQuizz.idQuizz);
                        bool bResultat = await bDeleteSuccess;
                        bContinue = true;
                    }
                }
            }
            return bContinue;
        }

        /// <summary>
        /// Show a questionview
        /// </summary>
        /// <param name="QV"></param>
        private void ShowQuestionView(QuestionView QV) {
            this.Visible = false;
            QV.ShowDialog();
            this.Visible = true;
            RefreshQuizzQuestion();
        }
        /// <summary>
        /// Upload a new quizz in the database
        /// </summary>
        /// <param name="title"></param>
        /// <param name="User"></param>
        /// <param name="Date"></param>
        private async void CreateQuizz(string title, User User, DateTime Date) {
            try {
                Task<Quizz> NewQuizz = HttpRequest.QuizzesRequest.Create(title, User.idUser, Date);
                Quizz resp = await NewQuizz;
                CurrentQuizz = resp;
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e) {
            RefreshQuizzQuestion();
        }
    }

    
}
