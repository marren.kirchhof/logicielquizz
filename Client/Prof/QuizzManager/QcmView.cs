﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QuizzManager.Constantes.UI;

namespace QuizzManager {
    public partial class QcmView : Form {

        private Quizz CurrentQuizz = null;
        private User CurrentUser = null;
        private Question CurrentQuestion = null;
        QuestionType CurrentType = null;
        string ActionT;

        private delegate void ActionAfterProcess();

        public QcmView() {
            InitializeComponent();
        }

        /// <summary>
        /// set the style interface
        /// </summary>
        #region UI
        #region resize form
        private void setParamsResizeForm() {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }






        #endregion

        #endregion

        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }

        /// <summary>
        /// Constructor edit
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="CurrentQuizz"></param>
        /// <param name="QuestionToEdit"></param>
        /// <param name="ActionType"></param>
        /// <param name="QT"></param>
        public QcmView(User ConnectedUser, Quizz CurrentQuizz, Question QuestionToEdit, string ActionType, QuestionType QT) {
            InitializeComponent();
            CurrentType = QT;
            this.CurrentQuizz = CurrentQuizz;
            ActionT = ActionType;
            CurrentUser = ConnectedUser;
            CurrentQuestion = QuestionToEdit;
            SetUI(ActionT);
            setParamsResizeForm();
            SetDataQcm();
            lbQuestionText.Text = CurrentQuestion.title;
        }
        /// <summary>
        /// Constructor add
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="CurrentQuizz"></param>
        /// <param name="title"></param>
        /// <param name="ActionType"></param>
        /// <param name="QT"></param>
        public QcmView(User ConnectedUser, Quizz CurrentQuizz, string title, string ActionType, QuestionType QT) {
            InitializeComponent();
            CurrentType = QT;
            this.CurrentQuizz = CurrentQuizz;
            ActionT = ActionType;
            CurrentUser = ConnectedUser;
            SetUI(ActionT);
            setParamsResizeForm();
            lbQuestionText.Text = title;
        }

        /// <summary>
        /// set the style interface
        /// </summary>
        /// <param name="ActionType"></param>
        private void SetUI(string ActionType) {
            switch (ActionType) {
                case Constantes.TypeAction.Add:
                    UiAdd();
                    break;
                case Constantes.TypeAction.Edit:
                    UiEdit();
                    break;
            }

        }

        /// <summary>
        /// style interface add
        /// </summary>
        private void UiAdd() {
            sbViewQcm.Text = Constantes.TitleForm.ViewQCMAdd;
            ActionT = Constantes.TypeAction.Add;
            btnAction.ButtonType = CxFlatUI.ButtonType.Success;
        }
        /// <summary>
        /// style interface edit
        /// </summary>
        private void UiEdit() {
            sbViewQcm.Text = Constantes.TitleForm.ViewQCMEdit;
            ActionT = Constantes.TypeAction.Edit;
            btnAction.ButtonType = CxFlatUI.ButtonType.Primary;
        }
        /// <summary>
        /// Load the data form the current Qcm
        /// </summary>
        private async void SetDataQcm() {
            try {
                Task<Question> Question = HttpRequest.QuestionRequest.GetOne(CurrentQuestion.idQuestion);
                Question q = await Question;
                Qcm CurrentQcm = DeserializeJSON(q.correction);
                clbxQcms.Items.Clear();
                foreach (KeyValuePair<string, bool> entry in CurrentQcm.ListQcm) {
                    clbxQcms.Items.Add(entry.Key, entry.Value);
                }
            } catch {
                MessageBox.Show(Constantes.AlertMessage.EmptyFields, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }





        /// <summary>
        /// Clear all input after adding a new qcm
        /// </summary>
        private void ClearInput() {
            tbxCurrentQCM.Clear(); sbxCurrentQCM.Checked = Constantes.Global.DefaultBoolFalue;
        }

        /// <summary>
        /// when the selected value change in the list 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clbxQcms_SelectedValueChanged(object sender, EventArgs e) {
            if (clbxQcms.SelectedItem != null) {
                sbxCurrentQCM.Checked = clbxQcms.GetItemChecked(clbxQcms.SelectedIndex);
                tbxCurrentQCM.Text = ((CheckedListBox)sender).Text;
            } else { ClearInput(); }
        }

        /// <summary>
        /// when the current checkbox.check change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clbxQcms_ItemCheck(object sender, ItemCheckEventArgs e) {
            sbxCurrentQCM.Checked = (e.NewValue == CheckState.Checked);
            tbxCurrentQCM.Text = ((CheckedListBox)sender).Text;
        }

        /// <summary>
        /// when the user want to edit a checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditQcm_Click(object sender, EventArgs e) {
            if (clbxQcms.SelectedItem != null && tbxCurrentQCM.Text.Trim()!="") {
                clbxQcms.SetItemCheckState(clbxQcms.SelectedIndex, sbxCurrentQCM.CheckState);
                clbxQcms.Items[clbxQcms.SelectedIndex] = tbxCurrentQCM.Text;
            }
        }

        /// <summary>
        /// when the user wnat to delete a checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelQcm_Click(object sender, EventArgs e) {
            if (clbxQcms.SelectedItem != null) {
                clbxQcms.Items.Remove(clbxQcms.SelectedItem);
            } else { ClearInput(); }
        }

        /// <summary>
        /// When the user wnat to add a new checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddQcm_Click(object sender, EventArgs e) {
            if (InputValide(tbxCurrentQCM.Text)) {
                clbxQcms.Items.Add(tbxCurrentQCM.Text,sbxCurrentQCM.Checked);
                tbxCurrentQCM.Text = "";
            }
        }

        /// <summary>
        /// Add or edit a question qcm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAction_Click(object sender, EventArgs e) {

            switch (ActionT) {
                case Constantes.TypeAction.Add:
                    CreateQuestion();
                    break;
                case Constantes.TypeAction.Edit:
                    EditQuestion();
                    break;
                default:
                    break;
            }

            this.Close();
        }

        /// <summary>
        /// Add a Qcm question in the database
        /// </summary>
        private async void CreateQuestion() {
            try {
                Task<Question> QuestionAdded = HttpRequest.QuestionRequest.Create(lbQuestionText.Text, CurrentQuizz.idQuizz, CurrentType.idType, GetJsonStringResponse());
                Question q = await QuestionAdded;
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Edit a Qcm question in the database
        /// </summary>
        private async void EditQuestion() {
            try {
                Task<bool> QuestionUpdated = HttpRequest.QuestionRequest.Update(CurrentQuestion.idQuestion, lbQuestionText.Text, CurrentQuizz.idQuizz, CurrentType.idType, GetJsonStringResponse());
                bool resp = await QuestionUpdated;
            } catch (Exception ex) {
                MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #region json

        /// <summary>
        /// Get the json string of the current qcm
        /// </summary>
        /// <returns>json string </returns>
        private string GetJsonStringResponse() {
            Dictionary<string, bool> ListQuestion = new Dictionary<string, bool>();
            for (int i = 0; i < clbxQcms.Items.Count; i++) {
                bool chkd = clbxQcms.GetItemChecked(i);
                ListQuestion.Add(clbxQcms.Items[i].ToString(), clbxQcms.GetItemChecked(i));
            }
           
            Qcm QcmJson = new Qcm(ListQuestion);
            return Newtonsoft.Json.JsonConvert.SerializeObject(QcmJson);
        }
        /// <summary>
        /// Get the qcm form the json string 
        /// </summary>
        /// <param name="JsonString">Qcm json string</param>
        /// <returns>Qcm object</returns>
        private Qcm DeserializeJSON(string JsonString) {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Qcm>(JsonString);
        }
        #endregion
        /// <summary>
        /// Check if inputs are valid
        /// </summary>
        /// <param name="NewText"></param>
        /// <returns></returns>
        private bool InputValide(string NewText) {
            bool br = true;

            br = (NewText.Trim() != "");
            for (int i = 0; i < clbxQcms.Items.Count; i++) {
                if (clbxQcms.Items[i].ToString() == NewText) {
                    br = false;
                }
            }
            return br;
        }
        /// <summary>
        /// Kex enter in tbxCurrentQcm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxCurrentQCM_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                btnAddQcm_Click(null, null);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
    }
}
