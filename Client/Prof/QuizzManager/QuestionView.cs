﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static QuizzManager.Constantes.UI;

namespace QuizzManager {
    public partial class QuestionView : Form {

        private Quizz CurrentQuizz = null;
        private User CurrentUser = null;
        private Question CurrentQuestion = null;
        private string ActionType;

        private delegate void ActionAfterCreateQuestion();

        #region Constructeur
        /// <summary>
        /// Constructor add
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="CurrentQuizz"></param>
        public QuestionView(User ConnectedUser, Quizz CurrentQuizz) {
            InitializeComponent();
            CurrentUser = ConnectedUser;
            this.CurrentQuizz = CurrentQuizz;
            ActionType = Constantes.TypeAction.Add;
            UiAdd();
            PrepareInputAdd();
            setParamsResizeForm();
            RefreshcbxType();
        }
        /// <summary>
        /// Constructor edit
        /// </summary>
        /// <param name="ConnectedUser"></param>
        /// <param name="CurrentQuizz"></param>
        /// <param name="QuestionToEdit"></param>
        public QuestionView(User ConnectedUser, Quizz CurrentQuizz, Question QuestionToEdit) {
            InitializeComponent();
            ActionType = Constantes.TypeAction.Edit;
            CurrentUser = ConnectedUser;
            this.CurrentQuizz = CurrentQuizz;
            UiEdit();
            CurrentQuestion = QuestionToEdit;
            setParamsResizeForm();
            RefreshcbxType();
            tbxTextQuestion.Text = QuestionToEdit.title;
        }



        #endregion
        #region UI
        /// <summary>
        /// Set the style interface
        /// </summary>
        #region SetUiAdd/Edit
        private void UiAdd() {
            sbViewQuestion.Text = Constantes.TitleForm.ViewQuestionAdd;
            ActionType = Constantes.TypeAction.Add;
        }
        private void UiEdit() {
            sbViewQuestion.Text = Constantes.TitleForm.ViewQuestionEdit;
            ActionType = Constantes.TypeAction.Edit;
        }
        #endregion
        #region resize form
        private void setParamsResizeForm() {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            Rectangle rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
            ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
            rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
            e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
        }

        /// <summary>
        /// Border resize
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (m.Msg == 0x84) {  // Trap WM_NCHITTEST
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption) {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return;
                }
                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip) {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return;
                }
            }
            base.WndProc(ref m);
        }
        #endregion

        /// <summary>
        /// Prepare input for add a new question
        /// </summary>
        #region PrepareInputForAdd
        private void PrepareInputAdd() {
            tbxTextQuestion.Text = "";
        }
        #endregion
        #endregion
        #region RefreshType
        /// <summary>
        /// Refresh the combobox cbxType with the according to the action of the user too
        /// </summary>
        private async void RefreshcbxType() {
            try {
                cbxType.DisplayMember = Constantes.DisplayListbox.ComboboxType;
                Task<List<QuestionType>> R = HttpRequest.TypeQuestionRequest.GetAll();
                List<QuestionType> LResultat = await R;
                cbxType.Items.Clear();
                foreach (QuestionType qT in LResultat) {
                    cbxType.Items.Add(qT);
                }

                switch (ActionType) {
                    case Constantes.TypeAction.Add:
                        cbxType.SelectedItem = 0;
                        break;
                    case Constantes.TypeAction.Edit:

                        Task<QuestionType> RqtypeSelected = HttpRequest.TypeQuestionRequest.GetOne(CurrentQuestion.idType);
                        QuestionType currentType = await RqtypeSelected;
                        cbxType.SelectedIndex = cbxType.FindStringExact(currentType.name);
                        cbxType.Enabled = false;
                        break;
                }
               
            } catch (Exception ex) { MessageBox.Show(Constantes.ErrorMessage.ErrorMessageException + ex, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        }
        #endregion
        #region Button exit
        private void btnExit_Click(object sender, EventArgs e) {
            Close();
        }
        #endregion




        /// <summary>
        /// Edit or add Qcm or DragDrop or TrueFalse question 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, EventArgs e) {
            if (cbxType.SelectedItem != null) {
                if (InputValide()) {

                    Form fShow = null;
                    switch (ActionType) {
                        case Constantes.TypeAction.Add:
                            switch (((QuestionType)cbxType.SelectedItem).name) {
                                case Constantes.QuestionsType.Vraifaux:
                                    fShow = new VraiFauxView(CurrentUser, CurrentQuizz,tbxTextQuestion.Text, ActionType,(QuestionType)cbxType.SelectedItem);
                                    break;
                                case Constantes.QuestionsType.Qcm:
                                    fShow = new QcmView(CurrentUser, CurrentQuizz, tbxTextQuestion.Text, ActionType, (QuestionType)cbxType.SelectedItem);
                                    break;
                                case Constantes.QuestionsType.DragDrop:
                                    fShow = new DragDropView(CurrentUser, CurrentQuizz, tbxTextQuestion.Text, ActionType, (QuestionType)cbxType.SelectedItem);
                                    break;
                            }
                            break;
                        case Constantes.TypeAction.Edit:
                            CurrentQuestion.title = tbxTextQuestion.Text;
                            switch (((QuestionType)cbxType.SelectedItem).name) {
                                case Constantes.QuestionsType.Vraifaux:
                                    fShow = new VraiFauxView(CurrentUser, CurrentQuizz, CurrentQuestion, ActionType, (QuestionType)cbxType.SelectedItem);
                                    break;
                                case Constantes.QuestionsType.Qcm:
                                    fShow = new QcmView(CurrentUser, CurrentQuizz, CurrentQuestion, ActionType, (QuestionType)cbxType.SelectedItem);
                                    break;
                                case Constantes.QuestionsType.DragDrop:
                                    fShow = new DragDropView(CurrentUser, CurrentQuizz, CurrentQuestion, ActionType, (QuestionType)cbxType.SelectedItem);
                                    break;
                            }
                            break;

                    }
                    this.Visible = false;
                    fShow.ShowDialog();
                    Close();
                    
                } else {
                    MessageBox.Show(Constantes.AlertMessage.EmptyFields, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }



        #region check
        /// <summary>
        /// Check if input are valid
        /// </summary>
        /// <returns></returns>
        private bool InputValide() {
            return (tbxTextQuestion.Text.Trim() != "" && cbxType.SelectedItem != null);
        }
        #endregion
    }
}
