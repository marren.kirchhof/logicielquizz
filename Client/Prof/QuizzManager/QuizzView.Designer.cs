﻿namespace QuizzManager
{
    partial class QuizzView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuizzView));
            this.sbViewQuizz = new CxFlatUI.CxFlatStatusBar();
            this.lbAllQuestionQuizz = new CxFlatUI.CxFlatTextBox();
            this.lbxQuestionsQuizz = new System.Windows.Forms.ListBox();
            this.tbxTitleQuizz = new CxFlatUI.CxFlatTextBox();
            this.btnDelQuestion = new CxFlatUI.CxFlatSimpleButton();
            this.btnAddQuestion = new CxFlatUI.CxFlatSimpleButton();
            this.btnEditQuestion = new CxFlatUI.CxFlatSimpleButton();
            this.btnExit = new CxFlatUI.CxFlatRoundButton();
            this.btnAction = new CxFlatUI.CxFlatSimpleButton();
            this.dpDateQuizz = new System.Windows.Forms.DateTimePicker();
            this.cxFlatGroupBox1 = new CxFlatUI.CxFlatGroupBox();
            this.btnRefresh = new CxFlatUI.Controls.CxFlatButton();
            this.cxFlatGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbViewQuizz
            // 
            this.sbViewQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sbViewQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.sbViewQuizz.Location = new System.Drawing.Point(0, 0);
            this.sbViewQuizz.Name = "sbViewQuizz";
            this.sbViewQuizz.Size = new System.Drawing.Size(515, 40);
            this.sbViewQuizz.TabIndex = 0;
            this.sbViewQuizz.Text = " Quizz Ajout / edition";
            this.sbViewQuizz.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(173)))), ((int)(((byte)(255)))));
            // 
            // lbAllQuestionQuizz
            // 
            this.lbAllQuestionQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAllQuestionQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbAllQuestionQuizz.Hint = "";
            this.lbAllQuestionQuizz.Location = new System.Drawing.Point(6, 138);
            this.lbAllQuestionQuizz.MaxLength = 32767;
            this.lbAllQuestionQuizz.Multiline = false;
            this.lbAllQuestionQuizz.Name = "lbAllQuestionQuizz";
            this.lbAllQuestionQuizz.PasswordChar = '\0';
            this.lbAllQuestionQuizz.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lbAllQuestionQuizz.SelectedText = "";
            this.lbAllQuestionQuizz.SelectionLength = 0;
            this.lbAllQuestionQuizz.SelectionStart = 0;
            this.lbAllQuestionQuizz.Size = new System.Drawing.Size(497, 38);
            this.lbAllQuestionQuizz.TabIndex = 3;
            this.lbAllQuestionQuizz.TabStop = false;
            this.lbAllQuestionQuizz.Text = "Questions du quizz";
            this.lbAllQuestionQuizz.UseSystemPasswordChar = false;
            // 
            // lbxQuestionsQuizz
            // 
            this.lbxQuestionsQuizz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxQuestionsQuizz.FormattingEnabled = true;
            this.lbxQuestionsQuizz.Location = new System.Drawing.Point(6, 182);
            this.lbxQuestionsQuizz.Name = "lbxQuestionsQuizz";
            this.lbxQuestionsQuizz.Size = new System.Drawing.Size(497, 186);
            this.lbxQuestionsQuizz.TabIndex = 1;
            // 
            // tbxTitleQuizz
            // 
            this.tbxTitleQuizz.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxTitleQuizz.Hint = "Titre";
            this.tbxTitleQuizz.Location = new System.Drawing.Point(6, 72);
            this.tbxTitleQuizz.MaxLength = 32767;
            this.tbxTitleQuizz.Multiline = false;
            this.tbxTitleQuizz.Name = "tbxTitleQuizz";
            this.tbxTitleQuizz.PasswordChar = '\0';
            this.tbxTitleQuizz.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxTitleQuizz.SelectedText = "";
            this.tbxTitleQuizz.SelectionLength = 0;
            this.tbxTitleQuizz.SelectionStart = 0;
            this.tbxTitleQuizz.Size = new System.Drawing.Size(229, 38);
            this.tbxTitleQuizz.TabIndex = 0;
            this.tbxTitleQuizz.TabStop = false;
            this.tbxTitleQuizz.Text = " - - - Titre Quizz --";
            this.tbxTitleQuizz.UseSystemPasswordChar = false;
            // 
            // btnDelQuestion
            // 
            this.btnDelQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelQuestion.ButtonType = CxFlatUI.ButtonType.Danger;
            this.btnDelQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelQuestion.Location = new System.Drawing.Point(6, 374);
            this.btnDelQuestion.Name = "btnDelQuestion";
            this.btnDelQuestion.Size = new System.Drawing.Size(93, 54);
            this.btnDelQuestion.TabIndex = 5;
            this.btnDelQuestion.Text = "Supprimer";
            this.btnDelQuestion.TextColor = System.Drawing.Color.White;
            this.btnDelQuestion.Click += new System.EventHandler(this.btnDelQuestion_Click);
            // 
            // btnAddQuestion
            // 
            this.btnAddQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddQuestion.ButtonType = CxFlatUI.ButtonType.Success;
            this.btnAddQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAddQuestion.Location = new System.Drawing.Point(204, 374);
            this.btnAddQuestion.Name = "btnAddQuestion";
            this.btnAddQuestion.Size = new System.Drawing.Size(93, 54);
            this.btnAddQuestion.TabIndex = 3;
            this.btnAddQuestion.Text = "Ajouter";
            this.btnAddQuestion.TextColor = System.Drawing.Color.White;
            this.btnAddQuestion.Click += new System.EventHandler(this.btnAddQuestion_Click);
            // 
            // btnEditQuestion
            // 
            this.btnEditQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditQuestion.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnEditQuestion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnEditQuestion.Location = new System.Drawing.Point(105, 374);
            this.btnEditQuestion.Name = "btnEditQuestion";
            this.btnEditQuestion.Size = new System.Drawing.Size(93, 54);
            this.btnEditQuestion.TabIndex = 4;
            this.btnEditQuestion.Text = "Editer";
            this.btnEditQuestion.TextColor = System.Drawing.Color.White;
            this.btnEditQuestion.Click += new System.EventHandler(this.btnEditQuestion_Click);
            // 
            // btnExit
            // 
            this.btnExit.ButtonType = CxFlatUI.ButtonType.Info;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnExit.Location = new System.Drawing.Point(6, 46);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 23);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "Retour";
            this.btnExit.TextColor = System.Drawing.Color.White;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.ButtonType = CxFlatUI.ButtonType.Default;
            this.btnAction.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAction.Location = new System.Drawing.Point(13, 8);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(176, 38);
            this.btnAction.TabIndex = 9;
            this.btnAction.Text = "valider";
            this.btnAction.TextColor = System.Drawing.Color.White;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // dpDateQuizz
            // 
            this.dpDateQuizz.Location = new System.Drawing.Point(6, 112);
            this.dpDateQuizz.Name = "dpDateQuizz";
            this.dpDateQuizz.Size = new System.Drawing.Size(229, 20);
            this.dpDateQuizz.TabIndex = 10;
            // 
            // cxFlatGroupBox1
            // 
            this.cxFlatGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cxFlatGroupBox1.Controls.Add(this.btnAction);
            this.cxFlatGroupBox1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cxFlatGroupBox1.Location = new System.Drawing.Point(303, 374);
            this.cxFlatGroupBox1.Name = "cxFlatGroupBox1";
            this.cxFlatGroupBox1.ShowText = false;
            this.cxFlatGroupBox1.Size = new System.Drawing.Size(200, 54);
            this.cxFlatGroupBox1.TabIndex = 11;
            this.cxFlatGroupBox1.TabStop = false;
            this.cxFlatGroupBox1.Text = "cxFlatGroupBox1";
            this.cxFlatGroupBox1.ThemeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.ButtonType = CxFlatUI.ButtonType.Primary;
            this.btnRefresh.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(412, 109);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(91, 23);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.Text = "Actualiser";
            this.btnRefresh.TextColor = System.Drawing.Color.White;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // QuizzView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 438);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.cxFlatGroupBox1);
            this.Controls.Add(this.lbAllQuestionQuizz);
            this.Controls.Add(this.btnAddQuestion);
            this.Controls.Add(this.lbxQuestionsQuizz);
            this.Controls.Add(this.dpDateQuizz);
            this.Controls.Add(this.tbxTitleQuizz);
            this.Controls.Add(this.btnEditQuestion);
            this.Controls.Add(this.btnDelQuestion);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.sbViewQuizz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1160);
            this.MinimumSize = new System.Drawing.Size(515, 438);
            this.Name = "QuizzView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.QuizzView_Load);
            this.cxFlatGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CxFlatUI.CxFlatStatusBar sbViewQuizz;
        private System.Windows.Forms.ListBox lbxQuestionsQuizz;
        private CxFlatUI.CxFlatTextBox tbxTitleQuizz;
        private CxFlatUI.CxFlatSimpleButton btnDelQuestion;
        private CxFlatUI.CxFlatSimpleButton btnAddQuestion;
        private CxFlatUI.CxFlatSimpleButton btnEditQuestion;
        private CxFlatUI.CxFlatRoundButton btnExit;
        private CxFlatUI.CxFlatSimpleButton btnAction;
        private System.Windows.Forms.DateTimePicker dpDateQuizz;
        private CxFlatUI.CxFlatTextBox lbAllQuestionQuizz;
        private CxFlatUI.CxFlatGroupBox cxFlatGroupBox1;
        private CxFlatUI.Controls.CxFlatButton btnRefresh;
    }
}