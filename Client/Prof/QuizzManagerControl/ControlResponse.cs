﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizzManagerControl {
    public partial class ControlResponse : UserControl {
        public ControlResponse() {
        }
        public ControlResponse(string Nom) {
            InitializeComponent();
            tbxReponse.Text = Nom;

        }

        public FlowLayoutPanel flpResponse { get { return this.flpQuestionCorrect; } set { this.flpQuestionCorrect = value; } }

        public string Title { get { return this.tbxReponse.Text; } }

        private void ControlResponse_Paint(object sender, PaintEventArgs e) {
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle,
                      Color.FromArgb(125,33,150,243), 1, ButtonBorderStyle.Solid,
                      Color.FromArgb(125, 33, 150, 243), 1, ButtonBorderStyle.Solid,
                      Color.FromArgb(125, 33, 150, 243), 1, ButtonBorderStyle.Solid,
                      Color.FromArgb(125, 33, 150, 243), 1, ButtonBorderStyle.Solid);
        }
    }
}
