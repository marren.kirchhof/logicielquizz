﻿namespace QuizzManagerControl {
    partial class ControlResponse {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.flpQuestionCorrect = new System.Windows.Forms.FlowLayoutPanel();
            this.tbxReponse = new CxFlatUI.CxFlatTextBox();
            this.SuspendLayout();
            // 
            // flpQuestionCorrect
            // 
            this.flpQuestionCorrect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpQuestionCorrect.AutoScroll = true;
            this.flpQuestionCorrect.BackColor = System.Drawing.Color.White;
            this.flpQuestionCorrect.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpQuestionCorrect.Location = new System.Drawing.Point(3, 47);
            this.flpQuestionCorrect.Name = "flpQuestionCorrect";
            this.flpQuestionCorrect.Padding = new System.Windows.Forms.Padding(1);
            this.flpQuestionCorrect.Size = new System.Drawing.Size(404, 120);
            this.flpQuestionCorrect.TabIndex = 0;
            // 
            // tbxReponse
            // 
            this.tbxReponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxReponse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            this.tbxReponse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tbxReponse.Enabled = false;
            this.tbxReponse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tbxReponse.Hint = "Réponse";
            this.tbxReponse.Location = new System.Drawing.Point(3, 3);
            this.tbxReponse.MaxLength = 32767;
            this.tbxReponse.Multiline = false;
            this.tbxReponse.Name = "tbxReponse";
            this.tbxReponse.PasswordChar = '\0';
            this.tbxReponse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxReponse.SelectedText = "";
            this.tbxReponse.SelectionLength = 0;
            this.tbxReponse.SelectionStart = 0;
            this.tbxReponse.Size = new System.Drawing.Size(404, 38);
            this.tbxReponse.TabIndex = 1;
            this.tbxReponse.TabStop = false;
            this.tbxReponse.UseSystemPasswordChar = false;
            // 
            // ControlResponse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.tbxReponse);
            this.Controls.Add(this.flpQuestionCorrect);
            this.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.MinimumSize = new System.Drawing.Size(410, 150);
            this.Name = "ControlResponse";
            this.Size = new System.Drawing.Size(410, 170);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ControlResponse_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpQuestionCorrect;
        private CxFlatUI.CxFlatTextBox tbxReponse;
    }
}
