<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



//crud question
//récupérer la liste des ussers liés a un quizz

$router->get('/', function () use ($router) {
    return $router->app->version();
    //return view('index');
});



$router->group(['prefix' => '/api/v1'], function () use ($router){

    $router->get('/', function () {
        return redirect('docs/index.html');
    });

    $router->group(['prefix' => 'users'], function () use ($router){
        $router->get('/', 'UserController@index');
        $router->post('/','UserController@store');
        $router->put('/{id}','UserController@update');
        $router->get('/{id}','UserController@show');
        $router->get('/find/{username}','UserController@showByUsername');
        $router->delete('/{id}','UserController@destroy');
    
        $router->group(['prefix' => '/{iduser}/quizzes'], function () use ($router){
            $router->get('/','UserController@quizzIndex');
            $router->put('/{idquizz}','UserController@quizzAttach');
            $router->delete('/{idquizz}', 'UserController@quizzDetach');
            $router->get('/{idquizz}/responses','UserController@quizzResponseIndex');
        });

        $router->group(['prefix' => '/{iduser}/ownedquizzes'], function () use ($router){
            $router->get('/','UserController@quizzOwnedByUser');
        });
    });
    
    $router->group(['prefix' => 'quizzes'], function () use ($router){
        $router->get('/', 'QuizzController@index');
        $router->post('/', 'QuizzController@store');
        $router->put('/{id}', 'QuizzController@update');
        $router->get('/{id}', 'QuizzController@show');
        $router->delete('/{id}', 'QuizzController@destroy');
    
        $router->group(['prefix' => '/{idquizz}/users'], function () use ($router) {
            $router->get('/','QuizzController@usersIndex');
        });

        $router->group(['prefix' => '/{idquizz}/responses'], function () use ($router) {
            $router->get('/','ResponseController@getLinkToQuizz');
            $router->delete('/','ResponseController@destroyLinkToQuizz');
        });

        $router->group(['prefix' => '/{idquizz}/questions'], function () use ($router) {
            $router->get('/','QuizzController@questionIndex');
            $router->put('/{idquestion}', 'QuizzController@questionAttach');
            $router->delete('/{idquestion}', 'QuizzController@questionDetach');
        });
    });
    
    $router->group(['prefix' => 'types'], function () use ($router) {
        $router->get('/', 'TypeController@index');
        $router->post('/', 'TypeController@store');
        $router->put('/{id}', 'TypeController@update');
        $router->get('/{id}', 'TypeController@show');
        $router->delete('/{id}', 'TypeController@destroy');
    });
    
    $router->group(['prefix' => 'roles'], function () use ($router) {
        $router->get('/', 'RoleController@index');
        $router->post('/', 'RoleController@store');
        $router->put('/{id}', 'RoleController@update');
        $router->get('/{id}', 'RoleController@show');
        $router->delete('/{id}', 'RoleController@destroy');
    });
    
    $router->group(['prefix' => 'responses'], function () use ($router) {
        $router->get('/', 'ResponseController@index');
        $router->post('/', 'ResponseController@store');
        $router->put('/{id}', 'ResponseController@update');
        $router->get('/{id}', 'ResponseController@show');
        $router->delete('/{id}', 'ResponseController@destroy');
    });
    
    $router->group(['prefix' => 'questions'], function () use ($router) {
        $router->get('/', 'QuestionController@index');
        $router->post('/', 'QuestionController@store');
        $router->put('/{id}', 'QuestionController@update');
        $router->get('/{id}', 'QuestionController@show');
        $router->delete('/{id}', 'QuestionController@destroy');
    });
    
});