---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_1aff981da377ba9a1bbc56ff8efaec0d -->
## api/v1/users
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idUser": 1,
        "username": "Admin",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "idRole": 2,
        "role": {
            "idRole": 2,
            "name": "Admin",
            "level": 1
        }
    },
    {
        "idUser": 2,
        "username": "User",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "idRole": 1,
        "role": {
            "idRole": 1,
            "name": "User",
            "level": 0
        }
    }
]
```

### HTTP Request
`GET api/v1/users`


<!-- END_1aff981da377ba9a1bbc56ff8efaec0d -->

<!-- START_4194ceb9a20b7f80b61d14d44df366b4 -->
## api/v1/users
> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/users`


<!-- END_4194ceb9a20b7f80b61d14d44df366b4 -->

<!-- START_4a5d19d4bd94b15e8aaf05f0cf462ef3 -->
## api/v1/users/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/users/{id}`


<!-- END_4a5d19d4bd94b15e8aaf05f0cf462ef3 -->

<!-- START_8e370f8df2793730b7d1497cb3d3a38c -->
## api/v1/users/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "idUser": 1,
    "username": "Admin",
    "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
    "idRole": 2,
    "role": {
        "idRole": 2,
        "name": "Admin",
        "level": 1
    }
}
```

### HTTP Request
`GET api/v1/users/{id}`


<!-- END_8e370f8df2793730b7d1497cb3d3a38c -->

<!-- START_08498cce48aa11679ffc48a788c23321 -->
## api/v1/users/find/{username}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/find/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/find/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[]
```

### HTTP Request
`GET api/v1/users/find/{username}`


<!-- END_08498cce48aa11679ffc48a788c23321 -->

<!-- START_8b97688fa48f9a3858d3b640a906b76b -->
## api/v1/users/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/users/{id}`


<!-- END_8b97688fa48f9a3858d3b640a906b76b -->

<!-- START_0201d1e65afcb91f9ca1e6c9909b51c1 -->
## api/v1/users/{iduser}/quizzes
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/1/quizzes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/quizzes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[]
```

### HTTP Request
`GET api/v1/users/{iduser}/quizzes`


<!-- END_0201d1e65afcb91f9ca1e6c9909b51c1 -->

<!-- START_a0e2889cbdccb3811483d5845fbc1ba0 -->
## api/v1/users/{iduser}/quizzes/{idquizz}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/users/1/quizzes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/quizzes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/users/{iduser}/quizzes/{idquizz}`


<!-- END_a0e2889cbdccb3811483d5845fbc1ba0 -->

<!-- START_eba8c12652483afe8f8dfcc137d447b5 -->
## api/v1/users/{iduser}/quizzes/{idquizz}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/users/1/quizzes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/quizzes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/users/{iduser}/quizzes/{idquizz}`


<!-- END_eba8c12652483afe8f8dfcc137d447b5 -->

<!-- START_f120f1f8c6ba6de33ef99e92c5436af4 -->
## api/v1/users/{iduser}/quizzes/{idquizz}/responses
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/1/quizzes/1/responses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/quizzes/1/responses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET api/v1/users/{iduser}/quizzes/{idquizz}/responses`


<!-- END_f120f1f8c6ba6de33ef99e92c5436af4 -->

<!-- START_125d2e3ff742251208a2216064c4839c -->
## api/v1/users/{iduser}/ownedquizzes
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/users/1/ownedquizzes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/users/1/ownedquizzes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idQuizz": 1,
        "name": "Quizz Numero 1",
        "idUser": 1,
        "Date": "2021-09-20"
    }
]
```

### HTTP Request
`GET api/v1/users/{iduser}/ownedquizzes`


<!-- END_125d2e3ff742251208a2216064c4839c -->

<!-- START_2814814aaa1f8bbab20f2e3b12b94e5c -->
## api/v1/quizzes
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/quizzes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idQuizz": 1,
        "name": "Quizz Numero 1",
        "idUser": 1,
        "Date": "2021-09-20",
        "owner": {
            "idUser": 1,
            "username": "Admin",
            "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
            "idRole": 2
        }
    }
]
```

### HTTP Request
`GET api/v1/quizzes`


<!-- END_2814814aaa1f8bbab20f2e3b12b94e5c -->

<!-- START_2ddfa9e9ab268cf8cf2fe44d13ba1c08 -->
## api/v1/quizzes
> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/quizzes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/quizzes`


<!-- END_2ddfa9e9ab268cf8cf2fe44d13ba1c08 -->

<!-- START_9cc067c9c32ee2e7f8042938209c7b9e -->
## api/v1/quizzes/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/quizzes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/quizzes/{id}`


<!-- END_9cc067c9c32ee2e7f8042938209c7b9e -->

<!-- START_a3aeb67e38a87b2355893d4be3357f19 -->
## api/v1/quizzes/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/quizzes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "idQuizz": 1,
    "name": "Quizz Numero 1",
    "idUser": 1,
    "Date": "2021-09-20",
    "owner": {
        "idUser": 1,
        "username": "Admin",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "idRole": 2
    }
}
```

### HTTP Request
`GET api/v1/quizzes/{id}`


<!-- END_a3aeb67e38a87b2355893d4be3357f19 -->

<!-- START_e0db394e83bb5acf5ed41d5336210427 -->
## api/v1/quizzes/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/quizzes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/quizzes/{id}`


<!-- END_e0db394e83bb5acf5ed41d5336210427 -->

<!-- START_873eb7b93dce9ca2d0a611aa9ff0b079 -->
## api/v1/quizzes/{idquizz}/users
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/quizzes/1/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idUser": 2,
        "username": "User",
        "password": "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
        "idRole": 1,
        "pivot": {
            "idQuizz": 1,
            "idUser": 2
        }
    }
]
```

### HTTP Request
`GET api/v1/quizzes/{idquizz}/users`


<!-- END_873eb7b93dce9ca2d0a611aa9ff0b079 -->

<!-- START_d7f0c565c232746d6c8339c90e703df2 -->
## api/v1/quizzes/{idquizz}/responses
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/quizzes/1/responses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1/responses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idResponse": 106,
        "respUser": "{\"ResponseTrue\": \"Oui\", \"ResponseFalse\": \"Non\", \"ResponseCorrection\": true}",
        "idQuizz": 1,
        "idUser": 2,
        "idQuestion": 1
    },
    {
        "idResponse": 107,
        "respUser": "{\"Response\": [{\"Title\": \"Reponse\", \"ListQuestion\": [\"Question\"]}, {\"Title\": \"reponse deux\", \"ListQuestion\": []}], \"WrongQuestion\": [\"Question \", \"Question\"]}",
        "idQuizz": 1,
        "idUser": 2,
        "idQuestion": 2
    },
    {
        "idResponse": 108,
        "respUser": "{\"ListQcm\": {\"un\": false, \"deux\": true, \"trois\": true, \"quatre\": false}}",
        "idQuizz": 1,
        "idUser": 2,
        "idQuestion": 3
    }
]
```

### HTTP Request
`GET api/v1/quizzes/{idquizz}/responses`


<!-- END_d7f0c565c232746d6c8339c90e703df2 -->

<!-- START_4f0796a10a7ceecfee66a6939e203739 -->
## api/v1/quizzes/{idquizz}/responses
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/quizzes/1/responses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1/responses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/quizzes/{idquizz}/responses`


<!-- END_4f0796a10a7ceecfee66a6939e203739 -->

<!-- START_04f038c9a1729c1208c098ce64c13e9c -->
## api/v1/quizzes/{idquizz}/questions
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/quizzes/1/questions" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/quizzes/1/questions"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idQuestion": 1,
        "title": "Question vrai faux 1",
        "idQuizz": 1,
        "idType": 2,
        "correction": "{\"ResponseTrue\": \"Oui\", \"ResponseFalse\": \"Non\", \"ResponseCorrection\": true}"
    },
    {
        "idQuestion": 2,
        "title": "Question Drag and drop 1",
        "idQuizz": 1,
        "idType": 1,
        "correction": "{\"Response\": [{\"Title\": \"Reponse\", \"ListQuestion\": [\"Question \"]}, {\"Title\": \"reponse deux\", \"ListQuestion\": [\"Question\"]}], \"WrongQuestion\": [\"Question\"]}"
    },
    {
        "idQuestion": 3,
        "title": "Question QCM 1",
        "idQuizz": 1,
        "idType": 3,
        "correction": "{\"ListQcm\": {\"un\": false, \"deux\": true, \"trois\": false, \"quatre\": true}}"
    }
]
```

### HTTP Request
`GET api/v1/quizzes/{idquizz}/questions`


<!-- END_04f038c9a1729c1208c098ce64c13e9c -->

<!-- START_c8c46f79ea22771862059c1f7bcb0e64 -->
## api/v1/types
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/types" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/types"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idType": 1,
        "name": "DragDrop"
    },
    {
        "idType": 2,
        "name": "VraiFaux"
    },
    {
        "idType": 3,
        "name": "QCM"
    }
]
```

### HTTP Request
`GET api/v1/types`


<!-- END_c8c46f79ea22771862059c1f7bcb0e64 -->

<!-- START_4c3debe116eeaffe26c8e69d80a59122 -->
## api/v1/types
> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/types" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/types"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/types`


<!-- END_4c3debe116eeaffe26c8e69d80a59122 -->

<!-- START_0f668e1bad1fc18accbe59e5c6291463 -->
## api/v1/types/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/types/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/types/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/types/{id}`


<!-- END_0f668e1bad1fc18accbe59e5c6291463 -->

<!-- START_833ed78896caf9acfb783aa76731706c -->
## api/v1/types/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/types/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/types/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "idType": 1,
    "name": "DragDrop"
}
```

### HTTP Request
`GET api/v1/types/{id}`


<!-- END_833ed78896caf9acfb783aa76731706c -->

<!-- START_dfd81219fd63f587e42ba1320da3e057 -->
## api/v1/types/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/types/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/types/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/types/{id}`


<!-- END_dfd81219fd63f587e42ba1320da3e057 -->

<!-- START_d2f16357cb4ed36dbb0e9529ea4a460c -->
## api/v1/roles
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/roles" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/roles"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idRole": 1,
        "name": "User",
        "level": 0
    },
    {
        "idRole": 2,
        "name": "Admin",
        "level": 1
    }
]
```

### HTTP Request
`GET api/v1/roles`


<!-- END_d2f16357cb4ed36dbb0e9529ea4a460c -->

<!-- START_5f753b2bffb6b34b6136ddfe1be7bcce -->
## api/v1/roles
> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/roles" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/roles"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/roles`


<!-- END_5f753b2bffb6b34b6136ddfe1be7bcce -->

<!-- START_f4e79933e504b4eb9f9e7c247fc20d28 -->
## api/v1/roles/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/roles/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/roles/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/roles/{id}`


<!-- END_f4e79933e504b4eb9f9e7c247fc20d28 -->

<!-- START_c57949fbf404e2e15ad7f5093f8ac5ad -->
## api/v1/roles/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/roles/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/roles/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "idRole": 1,
    "name": "User",
    "level": 0
}
```

### HTTP Request
`GET api/v1/roles/{id}`


<!-- END_c57949fbf404e2e15ad7f5093f8ac5ad -->

<!-- START_6611a6d3290580c932f0482a0234c02b -->
## api/v1/roles/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/roles/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/roles/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/roles/{id}`


<!-- END_6611a6d3290580c932f0482a0234c02b -->

<!-- START_7811821dc25ba880c64a0fa5e9dffdfd -->
## api/v1/responses
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/responses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/responses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idResponse": 106,
        "respUser": "{\"ResponseTrue\": \"Oui\", \"ResponseFalse\": \"Non\", \"ResponseCorrection\": true}",
        "idQuizz": 1,
        "idUser": 2,
        "idQuestion": 1
    },
    {
        "idResponse": 107,
        "respUser": "{\"Response\": [{\"Title\": \"Reponse\", \"ListQuestion\": [\"Question\"]}, {\"Title\": \"reponse deux\", \"ListQuestion\": []}], \"WrongQuestion\": [\"Question \", \"Question\"]}",
        "idQuizz": 1,
        "idUser": 2,
        "idQuestion": 2
    },
    {
        "idResponse": 108,
        "respUser": "{\"ListQcm\": {\"un\": false, \"deux\": true, \"trois\": true, \"quatre\": false}}",
        "idQuizz": 1,
        "idUser": 2,
        "idQuestion": 3
    }
]
```

### HTTP Request
`GET api/v1/responses`


<!-- END_7811821dc25ba880c64a0fa5e9dffdfd -->

<!-- START_a42d1d6807c0fc46613ca0dbcb89d4fe -->
## api/v1/responses
> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/responses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/responses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/responses`


<!-- END_a42d1d6807c0fc46613ca0dbcb89d4fe -->

<!-- START_71f9eba51d3fc925d6f02d4bd604b08f -->
## api/v1/responses/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/responses/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/responses/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/responses/{id}`


<!-- END_71f9eba51d3fc925d6f02d4bd604b08f -->

<!-- START_8aaaefc4ec1010cdb715d9867cb33bbe -->
## api/v1/responses/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/responses/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/responses/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET api/v1/responses/{id}`


<!-- END_8aaaefc4ec1010cdb715d9867cb33bbe -->

<!-- START_1c21f58aebda38e0c265b1ca161cbaf8 -->
## api/v1/responses/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/responses/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/responses/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/responses/{id}`


<!-- END_1c21f58aebda38e0c265b1ca161cbaf8 -->

<!-- START_0aa6e14208df1945877224c20ca26f46 -->
## api/v1/questions
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/questions" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/questions"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "idQuestion": 1,
        "title": "Question vrai faux 1",
        "idQuizz": 1,
        "idType": 2,
        "correction": "{\"ResponseTrue\": \"Oui\", \"ResponseFalse\": \"Non\", \"ResponseCorrection\": true}",
        "type": {
            "idType": 2,
            "name": "VraiFaux"
        }
    },
    {
        "idQuestion": 2,
        "title": "Question Drag and drop 1",
        "idQuizz": 1,
        "idType": 1,
        "correction": "{\"Response\": [{\"Title\": \"Reponse\", \"ListQuestion\": [\"Question \"]}, {\"Title\": \"reponse deux\", \"ListQuestion\": [\"Question\"]}], \"WrongQuestion\": [\"Question\"]}",
        "type": {
            "idType": 1,
            "name": "DragDrop"
        }
    },
    {
        "idQuestion": 3,
        "title": "Question QCM 1",
        "idQuizz": 1,
        "idType": 3,
        "correction": "{\"ListQcm\": {\"un\": false, \"deux\": true, \"trois\": false, \"quatre\": true}}",
        "type": {
            "idType": 3,
            "name": "QCM"
        }
    }
]
```

### HTTP Request
`GET api/v1/questions`


<!-- END_0aa6e14208df1945877224c20ca26f46 -->

<!-- START_fbd033d4038578cf755c7758d540c071 -->
## api/v1/questions
> Example request:

```bash
curl -X POST \
    "http://localhost/api/v1/questions" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/questions"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/questions`


<!-- END_fbd033d4038578cf755c7758d540c071 -->

<!-- START_b2511fd79e433adceab202878439c588 -->
## api/v1/questions/{id}
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/v1/questions/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/questions/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/v1/questions/{id}`


<!-- END_b2511fd79e433adceab202878439c588 -->

<!-- START_a83563229665ff6c1c321ed7b6cc1c56 -->
## api/v1/questions/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/questions/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/questions/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "idQuestion": 1,
    "title": "Question vrai faux 1",
    "idQuizz": 1,
    "idType": 2,
    "correction": "{\"ResponseTrue\": \"Oui\", \"ResponseFalse\": \"Non\", \"ResponseCorrection\": true}",
    "type": {
        "idType": 2,
        "name": "VraiFaux"
    }
}
```

### HTTP Request
`GET api/v1/questions/{id}`


<!-- END_a83563229665ff6c1c321ed7b6cc1c56 -->

<!-- START_45c7f51e301e3153fc1acab3bafae711 -->
## api/v1/questions/{id}
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/v1/questions/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/questions/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v1/questions/{id}`


<!-- END_45c7f51e301e3153fc1acab3bafae711 -->


