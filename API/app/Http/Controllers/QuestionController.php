<?php

namespace App\Http\Controllers;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(){
        return Question::with('type')->get();
    }

    public function store(Request $request){
        return Question::create($request->all());
    }

    public function update(Request $request, int $id){
        return Question::find($id)->update($request->all());
    }

    public function show(int $id){
        return Question::with('type')->find($id);
    }

    public function destroy(int $id){
        return Question::find($id)->delete();
    }
}
