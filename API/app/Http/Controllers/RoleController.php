<?php

namespace App\Http\Controllers;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(){
        return Role::all();
    }

    public function store(Request $request){
        return Role::create($request->all());
    }

    public function update(Request $request, int $id){
        return  Role::find($id)->update($request->all());
    }

    public function show(int $id){
        return Role::find($id);
    }

    public function destroy(int $id){
        return  Role::find($id)->delete();
    }
}
