<?php

namespace App\Http\Controllers;
use App\Response;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(){
        return Response::all(); //with('question')->
    }

    public function store(Request $request){
        return Response::create($request->all());
    }

    public function update(Request $request, int $id){
        return Response::find($id)->update($request->all());
    }

    public function show(int $id){
        return Response::find($id); //with('question')->
    }

    public function destroy(int $id){
        return Response::find($id)->delete();
    }



    public function getLinkToQuizz($idquizz){
        return Response::where('idQuizz',$idquizz)->get();
    }
    public function destroyLinkToQuizz($idquizz){
        return Response::where('idQuizz',$idquizz)->delete();
    }
    

}
