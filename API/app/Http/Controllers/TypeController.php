<?php

namespace App\Http\Controllers;
use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        return Type::all();
    }

    public function store(Request $request){
        return Type::create($request->all());
    }

    public function update(Request $request, int $id){
        return  Type::find($id)->update($request->all());
    }

    public function show(int $id){
        return Type::find($id);
    }

    public function destroy(int $id){
        return  Type::find($id)->delete();
    }
}
