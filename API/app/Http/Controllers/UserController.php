<?php

namespace App\Http\Controllers;
use App\{User, Quizz,Response};
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        return User::with('role')->get();
    }

    public function store(Request $request){
        return User::create($request->all());
    }

    public function update(Request $request, int $id){
        return  User::find($id)->update($request->all());
    }

    public function show(int $id){
        return User::with('role')->find($id);
    }

    public function showByUsername(String $username){
        return User::with('role')->where('username',$username)->get();
    }

    public function destroy(int $id){
        return User::find($id)->delete();
    }

    public function quizzIndex(int $iduser){
        return User::with('quizzes')->find($iduser)->quizzes;
    }

    public function quizzAttach(int $iduser, int $idquizz){
        $user = User::find($iduser);
        $user->quizzes()->attach(Quizz::find($idquizz));
        return response()->json(['status' => 'success'],200);
    }

    public function quizzDetach(int $iduser, int $idquizz)
    {
        $user = User::find($iduser);
        $user->quizzes()->detach(Quizz::find($idquizz));
        return response()->json(['status' => 'success'],200);
    }

    public function quizzResponseIndex(int $iduser, int $idquizz){

        $test = Response::where('idQuizz',$idquizz)->where('idUser',$iduser)->get();
        $responses = null;
        if(!$test->isEmpty()){
            $responses = Response::where('idQuizz',$idquizz)->where('idUser',$iduser)->get();
        }
        
        return $responses;
    }

    public function quizzOwnedByUser(int $iduser){
        return Quizz::where('idUser',$iduser)->get();
    }
}
