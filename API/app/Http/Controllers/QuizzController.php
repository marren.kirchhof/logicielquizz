<?php

namespace App\Http\Controllers;
use App\{Quizz, Question};
use Illuminate\Http\Request;

class QuizzController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(){
        return Quizz::with('owner')->get();
    }

    public function store(Request $request){
        return Quizz::create($request->all());
    }

    public function update(Request $request, int $id){
        Quizz::find($id)->update($request->all());
        return response()->json(['status' => 'success'],200);
    }

    public function show(int $id){
        return Quizz::with('owner')->find($id);
    }

    public function destroy(int $id){
        return  Quizz::find($id)->delete();
    }

    public function questionIndex(int $idquizz){
        return Quizz::with('questions')->find($idquizz)->questions;
    }

    // public function questionAttach(int $idquizz, int $idquestion)
    // {
    //     $quizz = Quizz::find($idquizz);

    //     return $quizz->questions()->associate(Question::find($idquestion));
    // }

    // public function questionDetach(int $idquizz, int $idquestion)
    // {
    //     $quizz = Quizz::find($idquizz);

    //     return $quizz->questions()->dissociate(Question::find($idquestion));
    // }

    public function usersIndex(int $idquizz){
        return Quizz::with('users')->find($idquizz)->users;
    }

}
