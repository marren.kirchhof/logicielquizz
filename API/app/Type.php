<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $primaryKey = 'idType';
  public $timestamps = false;

  protected $fillable = [
    'name',
  ];

  public function questions()
  {
    return $this->HasMany('App\Question','idType','idType');
  }
}
