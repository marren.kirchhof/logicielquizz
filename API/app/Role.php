<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $primaryKey = 'idRole';
  public $timestamps = false;

  protected $fillable = [
    'name','level'
  ];

  public function users()
  {
    return $this->HasMany('App\User','idUser', 'idUser');
  }
}
