<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'idUser';
    public $timestamps = false;

    protected $fillable = [
        'username','password','idRole',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function quizzes(){
        return $this->belongsToMany('App\Quizz','quizzes_users','idUser','idQuizz','idUser','idQuizz');
    }

    public function responses(){
        return $this->hasMany('App\Response','idUser','idUser');
    }

    public function quizzOwner(){
        return $this->hasOne('App\Quizz','idUser','idUser');
    }

    public function role(){
        return $this->hasOne('App\Role','idRole','idRole');
    }
}
