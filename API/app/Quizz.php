<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizz extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $primaryKey = 'idQuizz';
  protected $table = 'quizzes';
  public $timestamps = false;

  protected $fillable = [
    'name','idUser','date',
  ];

  public function users()
  {
    return $this->belongsToMany('App\User','quizzes_users', 'idQuizz', 'idUser', 'idQuizz', 'idUser');
  }

  public function owner(){
    return $this->hasOne('App\User','idUser','idUser');
  }

  public function questions(){
    return $this->hasMany('App\Question','idQuizz','idQuizz');
  }
}
