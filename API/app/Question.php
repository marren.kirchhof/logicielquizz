<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $primaryKey = 'idQuestion';
  public $timestamps = false;

  protected $fillable = [
    'title','correction','idType','idQuizz',
  ];

  public function type()
  {
    return $this->belongsTo('App\Type', 'idType', 'idType');
  }

  public function question(){
    return $this->belongsTo('App\Question','idQuizz','idQuizz');
  }
}
