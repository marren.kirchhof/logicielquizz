<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $primaryKey = 'idResponse';
  public $timestamps = false;

  protected $fillable = [
    'respuser','idQuizz','idUser','idQuestion',
  ];

  public function quizz()
  {
    return $this->belongsTo('App\Quizz','idQuizz', 'idQuizz');
  }

  public function user()
  {
    return $this->belongsTo('App\User', 'idUser', 'idUser');
  }

  public function question()
  {
    return $this->belongsTo('App\Question', 'idQuestion', 'idQuestion');
  }

}
